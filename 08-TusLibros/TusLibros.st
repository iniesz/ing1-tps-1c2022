!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 15:56:33'!
createBookCatalogue
	| aCatalogue aBookStockDictionary |
	aBookStockDictionary := OrderedCollection with: 'book1'
									with: 'book2'
									with: 'book3'.
	aCatalogue := Catalogue with: aBookStockDictionary.
	
	^ aCatalogue.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'FM 6/2/2022 22:41:48'!
test01CartIsEmptyWhenIsCreated

	| aCart |
	
	aCart := Cart new.
	
	self assert: aCart isEmpty.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 14:39:05'!
test02CartIsNotEmptyWhenABookIsAdded

	| aCart aCatalogue aBookCollection |
	
	aCart := Cart with: (self createBookCatalogue ).
	aCart add: 'book1'.
	
	self deny: aCart isEmpty.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 14:39:41'!
test03CartFailsWhenAddingBooksNotInCatalogue

	| aCart aCatalogue |
	aCart := Cart with: (self createBookCatalogue).
	
	self should: [aCart add: 'bookX']
		raise: Error
		description: aCart class errorDescriptionBookIsNotInCatalogueError.
	
	
	self assert: aCart isEmpty.
	
	! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 15:58:33'!
test04CartAddingPerQuantity

	| aCart |
	aCart := Cart with: (self createBookCatalogue).
	
	aCart add: 'book1' amount: 3.
	aCart add: 'book2' amount: 1.
	aCart add: 'book3' amount: 4.
	
	aCart add: 'book2'.
	aCart add: 'book3'.
	
	self assert: 10 equals: aCart size.
	self assert: 3 equals: (aCart quantity: 'book1').
	self assert: 2 equals: (aCart quantity: 'book2').
	self assert: 5 equals: (aCart quantity: 'book3').! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'books catalogue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 15:02:16'!
add: aBook
	self add: aBook amount: 1.! !

!Cart methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 15:54:35'!
add: aBook amount: anAmount

	| currentAmount |
	(catalogue hasRegistered: aBook) 
		ifFalse:[
			self error: self class errorDescriptionBookIsNotInCatalogueError.
		].
		
	currentAmount := books at: aBook ifAbsent:[0].
	currentAmount := currentAmount + anAmount.
	books at:aBook put: currentAmount.
	
	! !

!Cart methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 14:56:57'!
initialize

	books := Dictionary new.! !

!Cart methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 14:56:52'!
initializeWith: aCatalogue

	books := Dictionary new.
	catalogue := aCatalogue.! !

!Cart methodsFor: 'as yet unclassified' stamp: 'FM 6/2/2022 22:45:36'!
isEmpty

	^ books isEmpty .! !

!Cart methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 15:57:47'!
quantity: aBook
	^ books at: aBook ifAbsent:[0].! !

!Cart methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 15:00:52'!
size
	^ books inject: 0 into: [:total :bookAmount | total + bookAmount].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 14:38:38'!
errorDescriptionBookIsNotInCatalogueError
	^'Book is not in catalogue'.! !

!Cart class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 14:51:20'!
errorDescriptionBookNotInStockError
	^ 'The book is out of stock.'.! !

!Cart class methodsFor: 'as yet unclassified' stamp: 'FM 6/2/2022 22:53:53'!
with: aCatalogue

	^ self new initializeWith: aCatalogue.! !


!classDefinition: #Catalogue category: 'TusLibros'!
Object subclass: #Catalogue
	instanceVariableNames: 'books'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Catalogue methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 15:54:03'!
hasRegistered: aBook

	^ books includes: aBook.! !

!Catalogue methodsFor: 'as yet unclassified' stamp: 'FM 6/2/2022 22:51:10'!
initializeWith: aBookCollection

	books := aBookCollection ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Catalogue class' category: 'TusLibros'!
Catalogue class
	instanceVariableNames: ''!

!Catalogue class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 15:20:02'!
errorDescriptionBookIsNotInCatalogue
	^ 'The book is not in the catalogue.'.! !

!Catalogue class methodsFor: 'as yet unclassified' stamp: 'FM 6/2/2022 22:53:59'!
with: aBookCollection

	^ self new initializeWith: aBookCollection! !
