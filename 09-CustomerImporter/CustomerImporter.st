!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/22/2022 21:22:39'!
assertAddressAt: aStreetName streetNumber: aStreetNumber town: aTown zipCode: aZipCode province: aProvince
ofCustomer:aCustomer     
	 |address| 
	address := aCustomer addressAt: aStreetName.
	
	self assert: aStreetName equals: address streetName .
	self assert: aStreetNumber equals: address streetNumber .	
	self assert: aTown equals: address town .	
	self assert: aZipCode equals: address zipCode.
	self assert: aProvince equals: address province

	! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/22/2022 21:32:37'!
assertAddressCount: anAddressesCount ofCustomer: aCustomer
	| addressesCount |
	addressesCount := 0.
	aCustomer withAddressesCollect:[:address | addressesCount := addressesCount +1].
	self assert: anAddressesCount equals: addressesCount.
	! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/16/2022 21:04:22'!
assertCustomer: aCustomer name: aName lastName: aLastName identificationType: anIdentificationType identificationNumber: anIdentificationNumber    

	self assert: aName equals: aCustomer firstName.
	self assert: aLastName equals: aCustomer lastName.
	self assert: anIdentificationType equals: aCustomer identificationType.	
	self assert: anIdentificationNumber equals: aCustomer identificationNumber! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/22/2022 21:20:33'!
createTestDataStream

	"^ StandardFileStream new open: 'input.txt' forWrite: false"
	
	^
	ReadStream on: 'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'.! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/16/2022 21:27:30'!
customerByIdentificationType: anIdentificationType number: anIdentificationNumber 

	^ (session select:[:customer | customer identificationType=anIdentificationType
			and:[customer identificationNumber = anIdentificationNumber].]
		ofType: Customer) anyOne! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/16/2022 20:36:48'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/16/2022 20:37:38'!
tearDown

	session commit.
	session close! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/22/2022 21:32:09'!
test01Import

	| customers cust  addressesCount |
	(CustomerImporter 
		session: session 
		dataStream: (self createTestDataStream)) value.
	
	customers := session selectAllOfType: Customer.
	
	self assert: 2 equals: customers size.
	
	cust := self customerByIdentificationType: 'D' number: '22333444'.	
	self assertCustomer: cust name: 'Pepe' lastName: 'Sanchez' identificationType: 'D' identificationNumber: '22333444'.
	self assertAddressAt: 'San Martin' streetNumber: 3322 town: 'Olivos' zipCode: 1636 province: 'BsAs' ofCustomer: cust.
	self assertAddressAt: 'Maipu' streetNumber: 888 town: 'Florida' zipCode: 1122 province: 'Buenos Aires' ofCustomer: cust.
	self assertAddressCount: 2 ofCustomer: cust.
	
	cust := self customerByIdentificationType:  'C' number: '23-25666777-9'.
	self assertCustomer: cust name: 'Juan' lastName: 'Perez' identificationType: 'C' identificationNumber: '23-25666777-9'.
	self assertAddressAt:  'Alem' streetNumber: 1122 town: 'CABA' zipCode: 1001 province: 'CABA' ofCustomer: cust.
	self assertAddressCount: 1 ofCustomer: cust.
	
	! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'iniesz 6/16/2022 20:55:05'!
atStreet: anStreetName
	^ streetName = anStreetName.! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'testing' stamp: 'iniesz 6/22/2022 21:34:32'!
addresses

	^ addresses copy.! !


!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'iniesz 6/16/2022 20:54:32'!
addressAt: anStreetName
	^ addresses detect: [:anAddress | anAddress atStreet: anStreetName].! !

!Customer methodsFor: 'addresses' stamp: 'iniesz 6/22/2022 21:29:04'!
withAddressesCollect: aBlock
	^ addresses collect: aBlock.! !

!Customer methodsFor: 'addresses' stamp: 'iniesz 6/22/2022 21:28:23'!
withAddressesDo: aBlock
	^ addresses do: aBlock.! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Customer class' category: 'CustomerImporter'!
Customer class
	instanceVariableNames: ''!

!Customer class methodsFor: 'importing' stamp: 'HAW 5/22/2022 00:25:18'!
importCustomers

	"
	self importCustomers
	"
	| inputStream session newCustomer line |

	inputStream := StandardFileStream new open: 'input.txt' forWrite: false.
	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.

	line := inputStream nextLine.
	[ line notNil ] whileTrue: [
		(line beginsWith: 'C') ifTrue: [ | customerData |
			customerData := line findTokens: $,.
			newCustomer := Customer new.
			newCustomer firstName: customerData second.
			newCustomer lastName: customerData third.
			newCustomer identificationType: customerData fourth.
			newCustomer identificationNumber: customerData fourth.
			session persist: newCustomer ].

		(line beginsWith: 'A') ifTrue: [ | addressData newAddress |
			addressData := line findTokens: $,.
			newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: addressData second.
			newAddress streetNumber: addressData third asNumber .
			newAddress town: addressData fourth.
			newAddress zipCode: addressData fifth asNumber .
			newAddress province: addressData fourth ].

		line := inputStream nextLine. ].

	session commit.
	session close.

	inputStream close.
	! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session dataStream'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'iniesz 6/16/2022 21:42:10'!
initializeSession: aSession dataStream: aDataStream
	session := aSession.
	dataStream := aDataStream.! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'iniesz 6/22/2022 21:44:08'!
parseAddress: line ofCustomer: aCustomer 

	| addressData newAddress |
	addressData := line findTokens: $,.
	newAddress := Address new.
	aCustomer addAddress: newAddress.
	newAddress streetName: addressData second.
	newAddress streetNumber: addressData third asNumber .
	newAddress town: addressData fourth.
	newAddress zipCode: addressData fifth asNumber .
	newAddress province: addressData sixth! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'iniesz 6/23/2022 14:18:34'!
parseCustomer: line

	| customer customerData |
	customerData := line findTokens: $,.
	customer := Customer new.
	customer firstName: customerData second.
	customer lastName: customerData third.
	customer identificationType: customerData fourth.
	customer identificationNumber: customerData fifth.
	session persist: customer.
	^ customer.! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'iniesz 6/22/2022 21:43:35'!
value
	| newCustomer line |

	line := dataStream nextLine.
	[ line notNil ] whileTrue: [
		(line beginsWith: 'C') ifTrue: [ 
			newCustomer:= self parseCustomer: line.
			].

		(line beginsWith: 'A') ifTrue: [ self parseAddress: line ofCustomer: newCustomer].

		line := dataStream nextLine. ].

	dataStream close.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'iniesz 6/16/2022 21:42:10'!
session: aSession dataStream: aDataStream
	^self new initializeSession: aSession dataStream: aDataStream! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
