!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testObjectsFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test01NewCartsAreCreatedEmpty

	self assert: testObjectsFactory createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:23:47'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [ cart add: testObjectsFactory itemNotSellByTheStore
				at: testObjectsFactory validCartTime ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:24:29'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore 
				at: testObjectsFactory validCartTime.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:25:04'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 0 of: testObjectsFactory itemSellByTheStore 
			at: testObjectsFactory validCartTime]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:25:42'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 2 of: testObjectsFactory itemNotSellByTheStore  
				at: testObjectsFactory validCartTime ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:26:06'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore
		at: testObjectsFactory validCartTime.
	self assert: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self deny: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:26:52'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: 2 
		of: testObjectsFactory itemSellByTheStore
		at: testObjectsFactory validCartTime.
	self assert: (cart occurrencesOf: testObjectsFactory itemSellByTheStore) = 2! !

!CartTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:22:01'!
test09CartBecomeInvalidAfter30Minutes
	| cart |
	cart := testObjectsFactory createCart.	
	
	self
		should: [ 
			cart add: 2 
				of: testObjectsFactory itemSellByTheStore
				at: testObjectsFactory invalidCartTime
			]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: cart class inactiveCartErrorMessage
				equals: anError messageText. ]
	
	! !


!CartTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 18:09'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'testObjectsFactory debitBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:50'!
test01CanNotCheckoutAnEmptyCart

	| salesBook |
	
	salesBook := OrderedCollection new.
	self 
		should: [ Cashier 
			toCheckout: testObjectsFactory createCart 
			charging: testObjectsFactory notExpiredCreditCard 
			throught: self
			on: testObjectsFactory today
			registeringOn:  salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier cartCanNotBeEmptyErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:27:35'!
test02CalculatedTotalIsCorrect

	| cart cashier |
	
	cart := testObjectsFactory createCart.
	cart add: 2 
		of: testObjectsFactory itemSellByTheStore
		at: testObjectsFactory validCartTime.
	
	cashier :=  Cashier
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard 
		throught: self
		on: testObjectsFactory today 
		registeringOn: OrderedCollection new.
		
	self assert: cashier checkOut = (testObjectsFactory itemSellByTheStorePrice * 2)! !

!CashierTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:27:55'!
test03CanNotCheckoutWithAnExpiredCreditCart

	| cart salesBook |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore
		at: testObjectsFactory validCartTime.
	salesBook := OrderedCollection new.
	
	self
		should: [ Cashier 
				toCheckout: cart 
				charging: testObjectsFactory expiredCreditCard 
				throught: self
				on: testObjectsFactory today
				registeringOn: salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError | 
			self assert: anError messageText = Cashier canNotChargeAnExpiredCreditCardErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:28:01'!
test04CheckoutRegistersASale

	| cart cashier salesBook total |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore
		at: testObjectsFactory validCartTime.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	total := cashier checkOut.
					
	self assert: salesBook size = 1.
	self assert: salesBook first total = total.! !

!CashierTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:28:10'!
test05CashierChargesCreditCardUsingMerchantProcessor

	| cart cashier salesBook total creditCard debitedAmout debitedCreditCard  |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore
		at: testObjectsFactory validCartTime.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	debitBehavior := [ :anAmount :aCreditCard | 
		debitedAmout := anAmount.
		debitedCreditCard := aCreditCard ].
	total := cashier checkOut.
					
	self assert: debitedCreditCard = creditCard.
	self assert: debitedAmout = total.! !

!CashierTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 14:28:25'!
test06CashierDoesNotSaleWhenTheCreditCardHasNoCredit

	| cart cashier salesBook creditCard |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore
		at: testObjectsFactory validCartTime.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 	debitBehavior := [ :anAmount :aCreditCard | self error: Cashier creditCardHasNoCreditErrorMessage].
	
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	self 
		should: [cashier checkOut ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier creditCardHasNoCreditErrorMessage.
			self assert: salesBook isEmpty ]! !


!CashierTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 19:03'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.
	debitBehavior := [ :anAmount :aCreditCard | ]! !


!CashierTest methodsFor: 'merchant processor protocol' stamp: 'HernanWilkinson 6/17/2013 19:02'!
debit: anAmount from: aCreditCard 

	^debitBehavior value: anAmount value: aCreditCard ! !


!classDefinition: #CreditCardTest category: 'TusLibros'!
TestCase subclass: #CreditCardTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 23:30:09'!
test01CanNotCreateCreditCardWithInvalidNumber

	self 
		should: [ 
			CreditCard number: '12345' 
			expirationDate: '' 
			owner: ''. 
		]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidNumberException. ]! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 23:28:23'!
test02CanNotCreateCreditCardWithAnExpirationDateLongerThan6Digits

	self 
		should: [ 
			CreditCard number: self validCreditCardNumber 
			expirationDate: '1020223' 
			owner: ''.
		]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidExpirationMonthAndYearException. ]! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 23:35:02'!
test03CanNotCreateCreditCardWithInvalidExpirationDate

	self 
		should: [ 
			CreditCard number: self validCreditCardNumber 
			expirationDate: '132022' 
			owner: ''. 
		]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidExpirationDateException. ]! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 23:35:06'!
test04CanNotCreateCreditCardWithEmptyOwnerName
	
	self 
		should: [ 
			CreditCard number: self validCreditCardNumber 
			expirationDate: self validExpirationDate  
			owner: ''. 
		]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidOwnerNameException. ]! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 23:35:10'!
test05CanNotCreateCreditCardWithANameLongerThan30Digits

	self 
		should: [ 
			CreditCard number: self validCreditCardNumber 
			expirationDate: self validExpirationDate  
			owner: '0123456789012345678901234567891'. 
		]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidOwnerNameException. ]! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 23:37:34'!
test06CanCreateCreditCardWithValidArguments

	| aCreditCart |
	
	aCreditCart := CreditCard 
		number: self validCreditCardNumber  
		expirationDate: self validExpirationDate  
		owner: self validOwnerName.
	
	self assert: aCreditCart number equals: self validCreditCardNumber.
	self assert: aCreditCart expirationDate equals: self validExpirationDateInMonthFormat .
	self assert: aCreditCart owner equals: self validOwnerName.! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 22:39:16'!
validCreditCardNumber

	^ '8050012398438763' .! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 22:40:39'!
validExpirationDate

	^ '012022' .! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 23:34:47'!
validExpirationDateInMonthFormat

	^ Month month: 01 year: 2022 .! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 22:39:30'!
validOwnerName

	^ 'Pedro Sanchez' .! !


!classDefinition: #TusLibrosTest category: 'TusLibros'!
TestCase subclass: #TusLibrosTest
	instanceVariableNames: 'testObjectsFactory authenticationBehavior debitBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 15:48:35'!
test01CanNotCreateCartWithInvalidUser

	 | tusLibros aCatalog anInvalidUser anInvalidPassword |

	aCatalog := testObjectsFactory defaultCatalog.
	authenticationBehavior := [ :anUser :aPassword | self error: TusLibros invalidUserErrorMessage].
	
	tusLibros := self createTusLibrosWithDefaultCatalog.

	anInvalidUser := 'xxxx'.
	anInvalidPassword := 'xxxx'.
	
	self 
		should: [ tusLibros createCartWithUser: anInvalidUser Password: anInvalidPassword]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = TusLibros invalidUserErrorMessage.
			self assert: tusLibros cartsList isEmpty ]! !

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 15:47:26'!
test02CanCreateCartWithValidUser

	 | tusLibros aCartID |

	tusLibros := self createTusLibrosWithDefaultCatalog.

	aCartID := tusLibros createCartWithUser: testObjectsFactory validUser 
					Password: testObjectsFactory validUserPasssword.

	self assert: tusLibros cartsList size = 1.
	self assert: (tusLibros getCartById: aCartID) isEmpty.! !

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 15:47:26'!
test03CreateCartReturnItsID

	 | tusLibros aCartID aCart |

	tusLibros := self createTusLibrosWithDefaultCatalog.
	
	aCartID := tusLibros createCartWithUser: testObjectsFactory validUser 
					Password: testObjectsFactory validUserPasssword.
	
	aCart := tusLibros getCartById: aCartID.
	
	self assert: aCart isEmpty.! !

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 16:36:32'!
test04CanNotAddToCartWithInvalidCartID

	 | tusLibros aQuantity anItem |
	
	tusLibros := self createTusLibrosWithDefaultCatalog.
	anItem := testObjectsFactory itemSellByTheStore.
	aQuantity := 2.
	
	self
		should: [ tusLibros addToCart: 1 
					item: anItem 
					quantity: aQuantity
					at: testObjectsFactory invalidCartTime.]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = TusLibros invalidCartIDErrorMessage. ]! !

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 16:36:57'!
test05CanAddToCartWithValidCartID

	 | tusLibros aQuantity anItem aCart aCartID |

	tusLibros := self createTusLibrosWithDefaultCatalog.
	
	aCartID := tusLibros createCartWithUser: testObjectsFactory validUser 
					Password: testObjectsFactory validUserPasssword.
	
	anItem := testObjectsFactory itemSellByTheStore.
	aQuantity := 2.
	
	tusLibros 
		addToCart: aCartID 
		item: anItem
		quantity: aQuantity
		at: testObjectsFactory validCartTime.
		
	aCart := tusLibros getCartById: aCartID.
	
	self assert: (aCart occurrencesOf: testObjectsFactory itemSellByTheStore) = 2! !

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 15:47:26'!
test06CanNotListCartWithInvalidCartID

	 | tusLibros |

	tusLibros := self createTusLibrosWithDefaultCatalog.
	
	self
		should: [ tusLibros listCart: 1. ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = TusLibros invalidCartIDErrorMessage. ]! !

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 16:37:23'!
test07listCartReturnsAddedProducts

	 | tusLibros aQuantity anItem aCartID aListCart testingListCart |

	tusLibros := self createTusLibrosWithDefaultCatalog.

	aCartID := tusLibros createCartWithUser: testObjectsFactory validUser 
					Password: testObjectsFactory validUserPasssword.

	anItem := testObjectsFactory itemSellByTheStore.
	aQuantity := 2.
		
	tusLibros 
		addToCart: aCartID 
		item: anItem
		quantity: aQuantity
		at: testObjectsFactory validCartTime.
	
	aListCart := tusLibros listCart: aCartID.

	testingListCart := Dictionary new.
	testingListCart at: anItem put: aQuantity .

	self assert: aListCart equals: testingListCart.
! !

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 15:47:26'!
test08CanNotCheckOutCartWithInvalidCartID

	 | tusLibros |

	tusLibros := self createTusLibrosWithDefaultCatalog.
	
	self
		should: [ tusLibros checkOutCart: 1 
			withCreditCartNumber: '111111' 
			expirationDate: '012022' 
			owner: 'test'
			on: testObjectsFactory validCartTime]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = TusLibros invalidCartIDErrorMessage. ]! !

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 16:37:37'!
test09CanNotCheckOutCartWithValidCartButWithInvalidCreditCard

	 | tusLibros aCartID |

	tusLibros := self createTusLibrosWithDefaultCatalog.
	
	aCartID := tusLibros createCartWithUser: testObjectsFactory validUser 
					Password: testObjectsFactory validUserPasssword .
	
	tusLibros 
		addToCart: aCartID 
		item: testObjectsFactory itemSellByTheStore 
		quantity: 1
		at: testObjectsFactory validCartTime.
	
	self
		should: [ tusLibros checkOutCart: 1 
					withCreditCartNumber: '111111' 
					expirationDate: '012022' 
					owner: 'test'
					on: testObjectsFactory validCartTime. ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidNumberException. ]! !

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 16:37:45'!
test10CanCheckOutValidCartWithValidCreditCard

	 | tusLibros aCartID validCreditCard debitBehaviorExecuted |
	debitBehaviorExecuted := false.
	debitBehavior := [:anAmount :aCreditCard | debitBehaviorExecuted := true.].
	tusLibros := self createTusLibrosWithDefaultCatalog.
	
	validCreditCard := testObjectsFactory validCreditCard.
	
	aCartID := tusLibros createCartWithUser: testObjectsFactory validUser 
					Password: testObjectsFactory validUserPasssword .
	
	tusLibros addToCart: aCartID 
		item: testObjectsFactory itemSellByTheStore 
		quantity: 1
		at: testObjectsFactory validCartTime.
			
	 tusLibros checkOutCart: aCartID 
		withCreditCartNumber: validCreditCard number
		expirationDate: validCreditCard expirationDateFormattedString 
		owner: validCreditCard owner
		on: testObjectsFactory validCartTime.
		
	self assert: testObjectsFactory itemSellByTheStorePrice 
		equals: (tusLibros salesBook at: 1) total.
		
	self assert: debitBehaviorExecuted.
		! !

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 16:38:03'!
test11CanNotCheckOutInactiveCart

	 | tusLibros aCartID validCreditCard debitBehaviorExecuted |
	debitBehaviorExecuted := false.
	debitBehavior := [:anAmount :aCreditCard | debitBehaviorExecuted := false.].
	tusLibros := self createTusLibrosWithDefaultCatalog.
	
	validCreditCard := testObjectsFactory validCreditCard.
	
	aCartID := tusLibros createCartWithUser: testObjectsFactory validUser 
					Password: testObjectsFactory validUserPasssword .
	
	tusLibros addToCart: aCartID 
		item: testObjectsFactory itemSellByTheStore 
		quantity: 1
		at: testObjectsFactory validCartTime.
			
			
	self should:[
	 tusLibros checkOutCart: aCartID 
		withCreditCartNumber: validCreditCard number
		expirationDate: validCreditCard expirationDateFormattedString 
		owner: validCreditCard owner
		on: testObjectsFactory invalidCartTime .
	]
	raise: Error
	withExceptionDo: [
		:anError |
		].
		
	self assert: 0 equals: tusLibros salesBook size.
		
	self deny: debitBehaviorExecuted.
		! !

!TusLibrosTest methodsFor: 'tests' stamp: 'iniesz 6/13/2022 16:44:42'!
test12CanListPurchasesCart
	| aCartID tusLibros validCreditCard aCartId |
	tusLibros := self createTusLibrosWithDefaultCatalog.
	
	validCreditCard := testObjectsFactory validCreditCard.
	
	aCartID := tusLibros createCartWithUser: testObjectsFactory validUser 
					Password: testObjectsFactory validUserPasssword .
	
	tusLibros addToCart: aCartID 
		item: testObjectsFactory itemSellByTheStore 
		quantity: 3
		at: testObjectsFactory validCartTime.
	tusLibros addToCart: aCartID 
		item: testObjectsFactory itemSellByTheStore 
		quantity: 1
		at: testObjectsFactory validCartTime.
		
		
	"tusLibros listPurchases: aCartId."! !


!TusLibrosTest methodsFor: 'authentication protocol' stamp: 'iniesz 6/13/2022 12:41:44'!
debit: anAmount from: aCreditCard
	^ debitBehavior value: anAmount value: aCreditCard.! !

!TusLibrosTest methodsFor: 'authentication protocol' stamp: 'FM 6/12/2022 19:53:46'!
user: anUser password: aPassword 

	^authenticationBehavior value: anUser value: aPassword ! !


!TusLibrosTest methodsFor: 'factory' stamp: 'iniesz 6/13/2022 12:48:19'!
createTusLibrosWithCatalog: aCatalog
	authenticator: anAuthenticationBehavior
	merchantProcessor: aDebitBehavior
	
	
	
	^ TusLibros withCatalog:  aCatalog 
				authenticationBehavior:  anAuthenticationBehavior 
				debitBehavior: aDebitBehavior.! !

!TusLibrosTest methodsFor: 'factory' stamp: 'iniesz 6/13/2022 15:47:26'!
createTusLibrosWithDefaultCatalog

	| catalog |

	catalog := testObjectsFactory defaultCatalog.
	^ TusLibros withCatalog:  catalog 
				authenticationBehavior:  self 
				debitBehavior: self.! !


!TusLibrosTest methodsFor: 'setup' stamp: 'iniesz 6/13/2022 12:20:04'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.
	authenticationBehavior := [ :anUser :aPassword | | users | 
						users := testObjectsFactory createUsers.
							users at: anUser 
							ifPresent:[
								:foundPassword | 
								(aPassword = foundPassword )
								ifFalse:[
									self error: TusLibros invalidUserErrorMessage
								].
								]
							ifAbsent: [
									self error: TusLibros invalidUserErrorMessage.
								].
						
						].
	debitBehavior := [ :anAmount :aCreditCard | ].! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items creationDate'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'iniesz 6/13/2022 16:33:51'!
assertIsActiveCart: aDateAndTime

	((aDateAndTime - creationDate)  >= (Duration minutes: 30) )
		ifTrue:[self error: self class inactiveCartErrorMessage].
	! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:06'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'iniesz 6/13/2022 14:10:09'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.
	
	creationDate := DateAndTime  now.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'new-methods' stamp: 'iniesz 6/13/2022 16:44:15'!
listItemsInto: anItemList

	items do: [ :anItem | 
			(anItemList includes: anItem) ifFalse: [anItemList at: anItem put: (self occurrencesOf: anItem)].
		]! !


!Cart methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 19:09'!
total

	^ items sum: [ :anItem | catalog at: anItem ]! !


!Cart methodsFor: 'adding' stamp: 'iniesz 6/13/2022 14:23:23'!
add: anItem at: aDateAndTime

	^ self add: 1 of: anItem at: aDateAndTime.! !

!Cart methodsFor: 'adding' stamp: 'iniesz 6/13/2022 14:12:33'!
add: aQuantity of: anItem at: aDateAndTime

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.
	self assertIsActiveCart: aDateAndTime.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !


!Cart methodsFor: 'as yet unclassified' stamp: 'iniesz 6/13/2022 16:20:40'!
pricedItems
	^ items collect: [ :anItem | Association key: anItem value: (catalog at:anItem) ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !

!Cart class methodsFor: 'instance creation' stamp: 'iniesz 6/13/2022 14:15:13'!
inactiveCartErrorMessage
	^ 'More than 30 minutes have elapsed and the Cart has been invalidated'.! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'cart salesBook merchantProcessor creditCard total checkoutDate'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:08'!
calculateTotal

	total := cart total.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'iniesz 6/13/2022 16:22:39'!
createSale

	^ Sale of: cart.
! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
debitTotal

	merchantProcessor debit: total from: creditCard.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
registerSale

	salesBook add: self createSale! !


!Cashier methodsFor: 'checkout' stamp: 'iniesz 6/13/2022 14:32:13'!
checkOut
	cart assertIsActiveCart: checkoutDate.
	self calculateTotal.
	self debitTotal.
	self registerSale.

	^ total! !


!Cashier methodsFor: 'initialization' stamp: 'iniesz 6/13/2022 14:31:38'!
initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor on: aDateAndTime registeringOn: aSalesBook
	
	cart := aCart.
	creditCard := aCreditCard.
	merchantProcessor := aMerchantProcessor.
	salesBook := aSalesBook.
	checkoutDate := aDateAndTime.! !

!Cashier methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:53'!
initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook
	
	cart := aCart.
	creditCard := aCreditCard.
	merchantProcessor := aMerchantProcessor.
	salesBook := aSalesBook! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:22'!
assertIsNotEmpty: aCart 
	
	aCart isEmpty ifTrue: [self error: self cartCanNotBeEmptyErrorMessage ]! !

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:23'!
assertIsNotExpired: aCreditCard on: aDate
	
	(aCreditCard isExpiredOn: aDate) ifTrue: [ self error: self canNotChargeAnExpiredCreditCardErrorMessage ]! !


!Cashier class methodsFor: 'instance creation' stamp: 'iniesz 6/13/2022 14:31:18'!
toCheckout: aCart charging: aCreditCard throught: aMerchantProcessor on: aDate registeringOn: aSalesBook
	
	self assertIsNotEmpty: aCart.
	self assertIsNotExpired: aCreditCard on: aDate.
	
	^self new initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor on: aDate registeringOn: aSalesBook! !


!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 18:21'!
canNotChargeAnExpiredCreditCardErrorMessage
	
	^'Can not charge an expired credit card'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:56'!
cartCanNotBeEmptyErrorMessage
	
	^'Can not check out an empty cart'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 19:02'!
creditCardHasNoCreditErrorMessage
	
	^'Credit card has no credit'! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'number expirationDate owner'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'testing' stamp: 'FM 6/12/2022 23:31:39'!
expirationDate

	^ expirationDate.! !

!CreditCard methodsFor: 'testing' stamp: 'iniesz 6/13/2022 09:53:18'!
expirationDateFormattedString
	| monthString yearString |
	monthString := expirationDate monthIndex asString.
	(monthString size = 1) ifTrue:[ monthString:= '0', monthString.].
	
	yearString := expirationDate yearNumber asString.
	
	^ monthString, yearString.
	! !

!CreditCard methodsFor: 'testing' stamp: 'FM 6/12/2022 22:28:44'!
number

	^ number.! !

!CreditCard methodsFor: 'testing' stamp: 'FM 6/12/2022 22:28:51'!
owner

	^ owner.! !


!CreditCard methodsFor: 'validation' stamp: 'FM 6/12/2022 23:38:25'!
isExpiredOn: aDate 
	
	^expirationDate start < (Month month: aDate monthIndex year: aDate yearNumber) start ! !


!CreditCard methodsFor: 'initialization' stamp: 'FM 6/12/2022 23:38:10'!
initializeExpiringOn: aMonth 
	
	expirationDate := aMonth ! !

!CreditCard methodsFor: 'initialization' stamp: 'FM 6/12/2022 23:36:53'!
initializeWithNumber: aCreditCardNumber expirationDate: aMonth owner: aOwnerName

	number := aCreditCardNumber.
	expirationDate := aMonth.
	owner := aOwnerName.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'asserts' stamp: 'FM 6/12/2022 22:26:42'!
assertIsValidCreditCardNumber: aCreditCardNumber
	
	(aCreditCardNumber isString and: aCreditCardNumber size == 16) ifFalse: [ self error: self invalidNumberException ].! !

!CreditCard class methodsFor: 'asserts' stamp: 'FM 6/12/2022 23:21:46'!
assertIsValidExpirationDate: aMonthAndYear
	
	[ self convertAMonthAndYearToCalendarMonth: aMonthAndYear ] on: Error do: [ self error: self invalidExpirationDateException ].! !

!CreditCard class methodsFor: 'asserts' stamp: 'FM 6/12/2022 23:26:50'!
assertIsValidExpirationMonthAndYear: aMonthAndYear
	
	(aMonthAndYear isString and: aMonthAndYear size == 6) ifFalse: [ self error: self invalidExpirationMonthAndYearException ].
	self assertIsValidExpirationDate: aMonthAndYear.! !

!CreditCard class methodsFor: 'asserts' stamp: 'FM 6/12/2022 22:27:16'!
assertIsValidOwnerName: aName
	
	(aName isString and: aName withoutSeparators isEmpty not and: aName size <= 30) ifFalse: [ self error: self invalidOwnerNameException ].! !


!CreditCard class methodsFor: 'exceptions' stamp: 'FM 6/12/2022 22:27:23'!
invalidExpirationDateException

	^ 'The expiration date must have an valid year and month'! !

!CreditCard class methodsFor: 'exceptions' stamp: 'FM 6/12/2022 22:54:51'!
invalidExpirationMonthAndYearException

	^ 'The expiration month must be 6 digits'! !

!CreditCard class methodsFor: 'exceptions' stamp: 'FM 6/12/2022 22:27:39'!
invalidNumberException

	^ 'The card number must be 16 digits'! !

!CreditCard class methodsFor: 'exceptions' stamp: 'FM 6/12/2022 22:27:45'!
invalidOwnerNameException

	^ 'The owner name must not be empty and must not have more than 30 digits'! !


!CreditCard class methodsFor: 'private' stamp: 'FM 6/12/2022 23:20:19'!
convertAMonthAndYearToCalendarMonth: aMonthAndYear
	
	| aMonth aYear |
		
	aMonth := (aMonthAndYear  copyFrom: 1 to: 2) asNumber.
	aYear := (aMonthAndYear copyFrom: 3 to: 6) asNumber.
	
	^ Month month: aMonth year: aYear .
! !


!CreditCard class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:38'!
expiringOn: aMonth 
	
	^self new initializeExpiringOn: aMonth! !

!CreditCard class methodsFor: 'instance creation' stamp: 'FM 6/12/2022 23:37:07'!
number: aCreditCardNumber expirationDate: aMonthAndYear owner: aName

	| aMonth |

	self assertIsValidCreditCardNumber: aCreditCardNumber.
	self assertIsValidExpirationMonthAndYear: aMonthAndYear.
	self assertIsValidOwnerName: aName.
	
	aMonth := self convertAMonthAndYearToCalendarMonth: aMonthAndYear.
	
	^ self new initializeWithNumber: aCreditCardNumber expirationDate: aMonth owner: aName.! !


!classDefinition: #Sale category: 'TusLibros'!
Object subclass: #Sale
	instanceVariableNames: 'summary total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 18:48'!
total
	
	^ total! !


!Sale methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:47'!
initializeTotal: aTotal

	total := aTotal ! !

!Sale methodsFor: 'initialization' stamp: 'iniesz 6/13/2022 16:23:58'!
initializeTotal: aTotal summary: aSummary 

	total := aTotal.
	summary := aSummary.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: 'TusLibros'!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'instance creation' stamp: 'iniesz 6/13/2022 16:24:18'!
of: aCart

	"should assert total is not negative or 0!!"
	^self new initializeTotal: aCart total summary: aCart pricedItems.! !


!classDefinition: #StoreTestObjectsFactory category: 'TusLibros'!
Object subclass: #StoreTestObjectsFactory
	instanceVariableNames: 'today'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStore
	
	^ 'validBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStorePrice
	
	^10! !


!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'HernanWilkinson 6/17/2013 18:08'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'HernanWilkinson 6/17/2013 18:08'!
defaultCatalog
	
	^ Dictionary new
		at: self itemSellByTheStore put: self itemSellByTheStorePrice;
		yourself ! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'iniesz 6/13/2022 14:18:48'!
invalidCartTime
	^ (DateAndTime now) + (Duration minutes: 35).! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'iniesz 6/13/2022 14:17:37'!
validCartTime

	^ DateAndTime now.! !


!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'iniesz 6/13/2022 10:21:51'!
createUsers
	| users |
	users := Dictionary new.
	users at: self validUser 
		put: self validUserPasssword .
	^ users.! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'HernanWilkinson 6/17/2013 18:37'!
expiredCreditCard
	
	^CreditCard expiringOn: (Month month: today monthIndex year: today yearNumber - 1)! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'HernanWilkinson 6/17/2013 18:36'!
notExpiredCreditCard
	
	^CreditCard expiringOn: (Month month: today monthIndex year: today yearNumber + 1)! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'iniesz 6/13/2022 10:04:19'!
validCreditCard
	
	"validDate := (Month month: today monthIndex year: today yearNumber + 1)."
	^ CreditCard number: '8050012398438763'
			expirationDate: self validCreditCardExpirationDate
			owner: 'Juan Perez'.! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'iniesz 6/13/2022 10:04:04'!
validCreditCardExpirationDate
	| monthString yearString |
	monthString := Date today monthIndex asString.
	(monthString size = 1) ifTrue:[ monthString:= '0', monthString.].
	
	yearString := (Date today yearNumber + 1) asString.
	
	^ monthString, yearString.! !


!StoreTestObjectsFactory methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:37'!
initialize

	today := DateAndTime now! !


!StoreTestObjectsFactory methodsFor: 'date' stamp: 'HernanWilkinson 6/17/2013 18:37'!
today
	
	^ today! !


!StoreTestObjectsFactory methodsFor: 'user' stamp: 'iniesz 6/13/2022 10:20:54'!
validUser
	^ 'test_user_1'.! !

!StoreTestObjectsFactory methodsFor: 'user' stamp: 'iniesz 6/13/2022 10:21:03'!
validUserPasssword
	^ 'password'.! !


!classDefinition: #TusLibros category: 'TusLibros'!
Object subclass: #TusLibros
	instanceVariableNames: 'lastGeneratedId authenticationMethod cartsList catalog debitBehavior salesBook'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibros methodsFor: 'initialization' stamp: 'iniesz 6/13/2022 15:51:00'!
initializeWithCatalog: aCatalog authenticationBehavior: anAuthenticationBehavior debitBehavior: aDebitBehavior

	lastGeneratedId := 0.
	catalog := aCatalog.
	authenticationMethod := anAuthenticationBehavior.
	debitBehavior := aDebitBehavior.
	cartsList := Dictionary new.
	salesBook := OrderedCollection new.! !


!TusLibros methodsFor: 'protocol' stamp: 'iniesz 6/13/2022 16:36:08'!
addToCart: aCartID item: anItem quantity: aQuantity at: aDateAndTime

	| aCart |

	aCart := self getCartById: aCartID.

	aCart add: aQuantity of: anItem at: aDateAndTime.! !

!TusLibros methodsFor: 'protocol' stamp: 'iniesz 6/13/2022 14:56:35'!
checkOutCart: aCartID withCreditCartNumber: aCreditCartNumber expirationDate: aCreditCartExpirationDate owner: aCreditCartOwner on: aCheckoutDateAndTime
	| cart cashier creditCard |

	cart := self getCartById: aCartID.
	
	creditCard := CreditCard number: aCreditCartNumber expirationDate: aCreditCartExpirationDate owner: aCreditCartOwner.
	
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: debitBehavior
		on:  aCheckoutDateAndTime
		registeringOn: salesBook.
		
	cashier checkOut.! !

!TusLibros methodsFor: 'protocol' stamp: 'iniesz 6/13/2022 10:16:05'!
createCartWithUser: anUser Password: aPassword

	| aCart newID |
	
	self assertValidUser: anUser password: aPassword.

	aCart := Cart acceptingItemsOf: catalog.
	
	newID := self generateNewID.
	
	cartsList at: newID put: aCart.
	
	^ newID.! !

!TusLibros methodsFor: 'protocol' stamp: 'iniesz 6/13/2022 16:44:15'!
listCart: aCartID 

	| aCart aCartDetail |

	aCart := self getCartById: aCartID.
	aCartDetail := Dictionary new.	
	aCart listItemsInto: aCartDetail.
	^ aCartDetail.! !


!TusLibros methodsFor: 'authentication' stamp: 'iniesz 6/13/2022 10:16:05'!
assertValidUser: anUser password: aPassword.

	authenticationMethod user: anUser password: aPassword.! !


!TusLibros methodsFor: 'private' stamp: 'iniesz 6/13/2022 15:52:01'!
generateNewID
	lastGeneratedId := lastGeneratedId +1.
	^ lastGeneratedId.! !

!TusLibros methodsFor: 'private' stamp: 'FM 6/12/2022 20:58:55'!
getCartById: aCartID

	^ cartsList at: aCartID ifAbsent: [ self error: self class invalidCartIDErrorMessage ].! !


!TusLibros methodsFor: 'testing' stamp: 'FM 6/12/2022 20:16:43'!
cartsList

	^ cartsList.! !

!TusLibros methodsFor: 'testing' stamp: 'iniesz 6/13/2022 12:44:02'!
salesBook
	^ salesBook.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibros class' category: 'TusLibros'!
TusLibros class
	instanceVariableNames: ''!

!TusLibros class methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 20:59:18'!
invalidCartIDErrorMessage

	^ 'The cart does not exist'.! !

!TusLibros class methodsFor: 'as yet unclassified' stamp: 'FM 6/12/2022 19:55:57'!
invalidUserErrorMessage

	^ 'The username or password is incorrect'.! !

!TusLibros class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/13/2022 12:32:36'!
withCatalog: aCatalog authenticationBehavior: anAuthenticationBehavior debitBehavior: aDebitBehavior

	^ self new initializeWithCatalog: aCatalog 
	authenticationBehavior: anAuthenticationBehavior  
	debitBehavior: aDebitBehavior.! !
