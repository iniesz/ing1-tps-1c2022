!classDefinition: #MarsRoverLoggerTest category: 'Ej7-MarsRover-Logger'!
TestCase subclass: #MarsRoverLoggerTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverLoggerTest methodsFor: 'heading-names' stamp: 'FM 6/2/2022 00:44:36'!
east

	^ 'East'! !

!MarsRoverLoggerTest methodsFor: 'heading-names' stamp: 'FM 6/2/2022 00:44:17'!
north

	^ 'North' ! !

!MarsRoverLoggerTest methodsFor: 'heading-names' stamp: 'FM 6/2/2022 00:53:51'!
south

	^ 'South' ! !

!MarsRoverLoggerTest methodsFor: 'heading-names' stamp: 'FM 6/2/2022 00:44:59'!
west

	^ 'West'! !


!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 13:35:11'!
test01EmptyCommandsLogsNothing
	| marsRover logger |
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	Transcript clear.
	logger := MarsRoverPositionLogger outputToTranscript.
	
	marsRover followPositionChangesWith: logger.
	
	
	marsRover process: ''.
	self assert: logger readStream atEnd.! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 13:35:11'!
test02ForwardHeadingNorthLogs
	| marsRover logger logStream |
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	Transcript clear.
	logger := MarsRoverPositionLogger outputToTranscript.
	marsRover followPositionChangesWith: logger.
	marsRover process: 'f'.
	
	logStream := logger readStream.	
	self assert: '1@2' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 13:35:11'!
test03BackwardHeadingNorthLogs
	| marsRover logger logStream |
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	Transcript clear.
	logger := MarsRoverPositionLogger outputToTranscript.
	marsRover followPositionChangesWith: logger.
	marsRover process: 'b'.
	logStream := logger readStream.
	self assert: '1@0' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 13:35:11'!
test04MultipleForwardBackwardHeadingEastLog
	| marsRover logger logStream |
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	Transcript clear.
	logger := MarsRoverPositionLogger outputToTranscript.
	marsRover followPositionChangesWith: logger.
	marsRover process: 'fb'.
	logStream := logger readStream.
	
	self assert: '2@1' equals: logStream nextLine.
	self assert: '1@1' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 07:43:33'!
test05HeadingEastLogsCorrectly
	self assertMarsRoverHeading: MarsRoverHeadingNorth command: 'r' logs: self east .
	self assertMarsRoverHeading: MarsRoverHeadingSouth command: 'l' logs: self east.! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 07:43:37'!
test06HeadingSouthLogsCorrectly
	self assertMarsRoverHeading: MarsRoverHeadingEast command: 'r' logs: self south .
	self assertMarsRoverHeading: MarsRoverHeadingWest command: 'l' logs: self south .! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 07:43:42'!
test07HeadingWestLogsCorrectly
	self assertMarsRoverHeading: MarsRoverHeadingSouth command: 'r' logs: self west.
	self assertMarsRoverHeading: MarsRoverHeadingNorth command: 'l' logs: self west.! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 07:43:50'!
test08HeadingNorthLogsCorrectly
	self assertMarsRoverHeading: MarsRoverHeadingWest command: 'r' logs: self north.
	self assertMarsRoverHeading: MarsRoverHeadingEast command: 'l' logs: self north .! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 11:29:57'!
test09MultipleCommandsLogsCorrecly
"
	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	Transcript clear.
	logger := MarsRoverPositionLogger with: TranscriptLogger new.
	marsRover followPositionAndHeadingChangesWith: logger.
	
	marsRover process: 'frflbb'.
	
	logStream := logger readStream.
	self assert: '1@2' equals: logStream nextLine.
	self assert: (self east) equals: logStream nextLine.
	self assert: '2@2' equals: logStream nextLine.
	self assert: (self north) equals: logStream nextLine.
	self assert: '2@1' equals: logStream nextLine.
	self assert: '2@0' equals: logStream nextLine.
	self assert: logStream atEnd."
! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 13:34:46'!
test10ChangeInPositionLogsCorrectlyLabelMorph

	| marsRover logger positionLabel |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	positionLabel := LabelMorph new.
	logger := MarsRoverPositionLogger outputToLabelMorph: positionLabel.
	marsRover followPositionChangesWith: logger.
	"logStream := "
	
	marsRover process: 'ff'.
	
	self assert: '1@3' equals: logger readStream contents.
	marsRover process: 'b'.
	self assert: '1@2' equals: logger readStream contents.
! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 13:34:46'!
test11ChangeInRotationLogsCorrectlyLabelMorph

	| marsRover logger headingLabel |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	headingLabel := LabelMorph new.
	logger := MarsRoverHeadingDescriptionLogger outputToLabelMorph: headingLabel.
	marsRover followHeadingChangesWith: logger.
	
	marsRover process: 'rrlr'.
	self assert: 'Apuntando al Sur' equals: logger readStream contents.
	marsRover process: 'r'.
	self assert: 'Apuntando al Oeste' equals: logger readStream contents.
	marsRover process: 'r'.
	self assert: 'Apuntando al Norte' equals: logger readStream contents.

! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 13:35:11'!
test12FullLogCorrectlyIntoTranscript
	| headingLabel marsRover headingLogger positionLogger logStream |
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	headingLabel := LabelMorph new.
	Transcript clear.
	positionLogger := MarsRoverPositionLogger outputToTranscript.
	headingLogger := MarsRoverHeadingLogger outputToTranscript.
	
	marsRover followPositionChangesWith: positionLogger.
	marsRover followHeadingChangesWith: headingLogger.
	
	marsRover process: 'ffrfl'.
	logStream := positionLogger readStream.
	
	self assert: '1@2' equals: logStream nextLine.
	self assert: '1@3' equals: logStream nextLine.
	self assert: (self east) equals: logStream nextLine.
	self assert: '2@3' equals: logStream nextLine.
	self assert: (self north) equals: logStream nextLine.
	self assert: logStream atEnd.! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 13:34:46'!
test13FullLogCorrectlyIntoLabelMorph
	| headingLabel marsRover headingLogger positionLogger positionLabel |
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	positionLabel := LabelMorph new.
	headingLabel := LabelMorph new.
	positionLogger := MarsRoverPositionLogger outputToLabelMorph: positionLabel.
	headingLogger := MarsRoverHeadingDescriptionLogger outputToLabelMorph: headingLabel.
	
	marsRover followPositionChangesWith: positionLogger.
	marsRover followHeadingChangesWith: headingLogger.
	
	marsRover process: 'ffrfl'.

	self assert: '2@3' equals: positionLogger readStream contents.
	self assert: 'Apuntando al Norte' equals: headingLogger readStream contents.
! !

!MarsRoverLoggerTest methodsFor: 'tests' stamp: 'iniesz 6/2/2022 13:35:11'!
test14ProxyComposedLogCorrectlyLogsInMultipleOutputs
	| headingLabel marsRover positionLabel transcriptStream |
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	positionLabel := LabelMorph new.
	headingLabel := LabelMorph new.
	Transcript clear.
	marsRover followPositionChangesWith: MarsRoverPositionLogger outputToTranscript.
	marsRover followPositionChangesWith: (MarsRoverPositionLogger outputToLabelMorph: positionLabel).
	marsRover followHeadingChangesWith:  MarsRoverHeadingLogger outputToTranscript.
	marsRover followHeadingChangesWith:  (MarsRoverHeadingDescriptionLogger outputToLabelMorph: headingLabel).
	
	marsRover process: 'ffrfl'.
	transcriptStream := self transcriptStreamWithoutFirstLine.
	
	self assert: '1@2' equals: transcriptStream nextLine.
	self assert: '1@3' equals: transcriptStream nextLine.
	self assert: (self east) equals: transcriptStream nextLine.
	self assert: '2@3' equals: transcriptStream nextLine.
	self assert: (self north) equals: transcriptStream nextLine.
	self assert: transcriptStream atEnd.
	
	self assert: '2@3' equals: positionLabel contents.
	self assert: 'Apuntando al Norte' equals: headingLabel contents .! !


!MarsRoverLoggerTest methodsFor: 'assertions' stamp: 'iniesz 6/1/2022 06:33:51'!
assert: aMarsRover heading: aMarsRoverHeading command: aStringCommand logs: aExpectedString! !

!MarsRoverLoggerTest methodsFor: 'assertions' stamp: 'iniesz 6/2/2022 13:35:10'!
assertMarsRoverHeading: aMarsRoverHeading command: aStringCommand logs: aExpectedString
	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: aMarsRoverHeading.
	Transcript clear.
	logger := MarsRoverHeadingLogger outputToTranscript.
	
	marsRover followHeadingChangesWith: logger.
	marsRover process: aStringCommand.
	logStream := logger readStream.
	self assert: aExpectedString equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverLoggerTest methodsFor: 'assertions' stamp: 'iniesz 6/2/2022 13:21:57'!
transcriptStreamWithoutFirstLine
| stream |
	stream := ReadStream on: Transcript contents.
	stream nextLine.
	^ stream.! !


!classDefinition: #MarsRoverTest category: 'Ej7-MarsRover-Logger'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:21:23'!
test01DoesNotMoveWhenNoCommand

	self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: '' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:12'!
test02IsAtFailsForDifferentPosition

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@2 heading: self north)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:31'!
test03IsAtFailsForDifferentHeading

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@1 heading: self south)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:17'!
test04IncrementsYAfterMovingForwardWhenHeadingNorth

	self 
		assertIsAt: 1@3 
		heading: self north 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:11'!
test06DecrementsYAfterMovingBackwardsWhenHeadingNorth

	self 
		assertIsAt: 1@1 
		heading: self north 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:59'!
test07PointToEashAfterRotatingRightWhenHeadingNorth

	self 
		assertIsAt: 1@2 
		heading: self east 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:51'!
test08PointsToWestAfterRotatingLeftWhenPointingNorth

	self 
		assertIsAt: 1@2 
		heading: self west 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:45'!
test09DoesNotProcessInvalidCommand

	| marsRover |
	
	marsRover := MarsRover at: 1@2 heading: self north.
	
	self 
		should: [ marsRover process: 'x' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: marsRover invalidCommandErrorDescription.
			self assert: (marsRover isAt: 1@2 heading: self north) ]! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:39'!
test10CanProcessMoreThanOneCommand

	self 
		assertIsAt: 1@4 
		heading: self north 
		afterProcessing: 'ff' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:31'!
test11IncrementsXAfterMovingForwareWhenHeadingEast

	self 
		assertIsAt: 2@2 
		heading: self east 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:19'!
test12DecrementsXAfterMovingBackwardWhenHeadingEast

	self 
		assertIsAt: 0@2 
		heading: self east 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:14'!
test13PointsToSouthAfterRotatingRightWhenHeadingEast

		self 
		assertIsAt: 1@2 
		heading: self south 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:05'!
test14PointsToNorthAfterRotatingLeftWhenPointingEast

		self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:00'!
test15ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingSouth

	self 
		assertIsAt: 1@1 
		heading: self west 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self south 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:52'!
test16ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingWest

	self 
		assertIsAt: 0@2 
		heading: self north 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self west 
! !


!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:31'!
east

	^ MarsRoverHeadingEast ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:38'!
north

	^ MarsRoverHeadingNorth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:45'!
south

	^ MarsRoverHeadingSouth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:54'!
west

	^ MarsRoverHeadingWest ! !


!MarsRoverTest methodsFor: 'assertions' stamp: 'HAW 10/7/2021 20:20:47'!
assertIsAt: newPosition heading: newHeadingType afterProcessing: commands whenStartingAt: startPosition heading: startHeadingType

	| marsRover |
	
	marsRover := MarsRover at: startPosition heading: startHeadingType. 
	
	marsRover process: commands.
	
	self assert: (marsRover isAt: newPosition heading: newHeadingType)! !


!classDefinition: #Logger category: 'Ej7-MarsRover-Logger'!
Object subclass: #Logger
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!Logger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:02:08'!
log: aString
	self subclassResponsibility.! !

!Logger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:03:20'!
readStream
	self subclassResponsibility.! !


!classDefinition: #LabelMorphLogger category: 'Ej7-MarsRover-Logger'!
Logger subclass: #LabelMorphLogger
	instanceVariableNames: 'label'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!LabelMorphLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:07:20'!
initializeWith: aLabelMorph
	label := aLabelMorph.! !

!LabelMorphLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:05:26'!
log: aString
	label contents: aString.! !

!LabelMorphLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:05:43'!
readStream
	^ ReadStream on: label contents.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'LabelMorphLogger class' category: 'Ej7-MarsRover-Logger'!
LabelMorphLogger class
	instanceVariableNames: ''!

!LabelMorphLogger class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:07:00'!
with: aLabelMorph
	| logger |
	logger := self new.
	logger initializeWith: aLabelMorph.
	^ logger.! !


!classDefinition: #TranscriptLogger category: 'Ej7-MarsRover-Logger'!
Logger subclass: #TranscriptLogger
	instanceVariableNames: 'transcript'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!TranscriptLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:10:10'!
initialize
	transcript := Transcript.! !

!TranscriptLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:09:42'!
log: aString
	transcript nextPutAll: aString; newLine.! !

!TranscriptLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:10:37'!
readStream
	| stream |
	stream := ReadStream on: transcript contents.
	stream nextLine.
	^ stream.! !


!classDefinition: #MarsRover category: 'Ej7-MarsRover-Logger'!
Object subclass: #MarsRover
	instanceVariableNames: 'position head loggerProxy'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:48:45'!
invalidCommandErrorDescription
	
	^'Invalid command'! !

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:50:26'!
signalInvalidCommand
	
	self error: self invalidCommandErrorDescription ! !


!MarsRover methodsFor: 'initialization' stamp: 'iniesz 6/2/2022 12:20:20'!
initializeAt: aPosition heading: aHeadingType

	position := aPosition.
	head := aHeadingType for: self.
	loggerProxy := MarsRoverLoggerProxy new. ! !


!MarsRover methodsFor: 'heading' stamp: 'iniesz 6/2/2022 05:49:16'!
headEast
	head := MarsRoverHeadingEast for: self.
	loggerProxy headingChangedTo: head! !

!MarsRover methodsFor: 'heading' stamp: 'iniesz 6/2/2022 05:49:10'!
headNorth
	head := MarsRoverHeadingNorth for: self.
	loggerProxy headingChangedTo: head! !

!MarsRover methodsFor: 'heading' stamp: 'iniesz 6/2/2022 05:48:44'!
headSouth
	head := MarsRoverHeadingSouth for: self.
	loggerProxy headingChangedTo: head! !

!MarsRover methodsFor: 'heading' stamp: 'iniesz 6/2/2022 05:48:51'!
headWest
	head := MarsRoverHeadingWest for: self.
	loggerProxy headingChangedTo: head! !

!MarsRover methodsFor: 'heading' stamp: 'iniesz 6/2/2022 05:48:56'!
rotateLeft
	head rotateLeft! !

!MarsRover methodsFor: 'heading' stamp: 'iniesz 6/2/2022 05:49:01'!
rotateRight
	head rotateRight! !


!MarsRover methodsFor: 'testing' stamp: 'iniesz 6/2/2022 05:49:37'!
isAt: aPosition heading: aHeadingType
	^position = aPosition and: [ head isHeading: aHeadingType ]! !

!MarsRover methodsFor: 'testing' stamp: 'iniesz 6/2/2022 05:49:40'!
isBackwardCommand: aCommand
	^aCommand = $b! !

!MarsRover methodsFor: 'testing' stamp: 'iniesz 6/2/2022 05:49:45'!
isForwardCommand: aCommand
	^aCommand = $f ! !

!MarsRover methodsFor: 'testing' stamp: 'iniesz 6/2/2022 05:49:52'!
isRotateLeftCommand: aCommand
	^aCommand = $l! !

!MarsRover methodsFor: 'testing' stamp: 'iniesz 6/2/2022 05:49:55'!
isRotateRightCommand: aCommand
	^aCommand = $r! !


!MarsRover methodsFor: 'moving' stamp: 'FM 6/1/2022 23:59:39'!
addToPositionAndLogNewPosition: newPosition

	position := position + newPosition.
	loggerProxy positionChangedTo: position! !

!MarsRover methodsFor: 'moving' stamp: 'FM 6/1/2022 23:59:45'!
moveBackward

	head moveBackward! !

!MarsRover methodsFor: 'moving' stamp: 'FM 6/1/2022 23:59:48'!
moveForward

	head moveForward! !


!MarsRover methodsFor: 'command processing' stamp: 'HAW 6/30/2018 19:48:26'!
process: aSequenceOfCommands

	aSequenceOfCommands do: [:aCommand | self processCommand: aCommand ]
! !

!MarsRover methodsFor: 'command processing' stamp: 'HAW 8/22/2019 12:08:50'!
processCommand: aCommand

	(self isForwardCommand: aCommand) ifTrue: [ ^ self moveForward ].
	(self isBackwardCommand: aCommand) ifTrue: [ ^ self moveBackward ].
	(self isRotateRightCommand: aCommand) ifTrue: [ ^ self rotateRight ].
	(self isRotateLeftCommand: aCommand) ifTrue: [ ^ self rotateLeft ].

	self signalInvalidCommand.! !


!MarsRover methodsFor: 'logger' stamp: 'FM 6/2/2022 00:27:31'!
followHeadingChangesWith: aMarsRoverLogger 

	loggerProxy addHeadingLogger: aMarsRoverLogger! !

!MarsRover methodsFor: 'logger' stamp: 'FM 6/2/2022 00:27:52'!
followPositionAndHeadingChangesWith: aMarsRoverLogger 

	loggerProxy addPositionAndHeadingLogger: aMarsRoverLogger! !

!MarsRover methodsFor: 'logger' stamp: 'FM 6/2/2022 00:27:41'!
followPositionChangesWith: aMarsRoverLogger 

	loggerProxy addPositionLogger: aMarsRoverLogger! !


!MarsRover methodsFor: 'moving-internal' stamp: 'HAW 5/30/2022 20:38:44'!
moveEast
	
	self addToPositionAndLogNewPosition: 1@0.
	! !

!MarsRover methodsFor: 'moving-internal' stamp: 'HAW 5/30/2022 20:38:44'!
moveNorth
	
	self addToPositionAndLogNewPosition: 0@1.
	! !

!MarsRover methodsFor: 'moving-internal' stamp: 'HAW 5/30/2022 20:38:44'!
moveSouth
	
	self addToPositionAndLogNewPosition: 0@-1.
	! !

!MarsRover methodsFor: 'moving-internal' stamp: 'HAW 5/30/2022 20:38:44'!
moveWest
	
	self addToPositionAndLogNewPosition: -1@0! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'Ej7-MarsRover-Logger'!
MarsRover class
	instanceVariableNames: 'headings'!

!MarsRover class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:10:30'!
at: aPosition heading: aHeadingType
	
	^self new initializeAt: aPosition heading: aHeadingType! !


!classDefinition: #MarsRoverHeading category: 'Ej7-MarsRover-Logger'!
Object subclass: #MarsRoverHeading
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:15:38'!
isHeading: aHeadingType

	^self isKindOf: aHeadingType ! !


!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'initialization' stamp: 'HAW 10/7/2021 20:11:59'!
initializeFor: aMarsRover 
	
	marsRover := aMarsRover.! !


!MarsRoverHeading methodsFor: 'visit-heading' stamp: 'iniesz 6/1/2022 06:43:04'!
accept: aVisitor
	self subclassResponsibility.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeading class' category: 'Ej7-MarsRover-Logger'!
MarsRoverHeading class
	instanceVariableNames: ''!

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:11:35'!
for: aMarsRover 
	
	^self new initializeFor: aMarsRover ! !


!classDefinition: #MarsRoverHeadingEast category: 'Ej7-MarsRover-Logger'!
MarsRoverHeading subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveWest! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveEast! !


!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headNorth! !

!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headSouth! !


!MarsRoverHeadingEast methodsFor: 'visit-heading' stamp: 'HAW 5/30/2022 20:46:21'!
accept: aVisitor

	^aVisitor visitMarsRoverHeadingEast: self! !


!classDefinition: #MarsRoverHeadingNorth category: 'Ej7-MarsRover-Logger'!
MarsRoverHeading subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveSouth! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveNorth! !


!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headWest! !

!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headEast! !


!MarsRoverHeadingNorth methodsFor: 'visit-heading' stamp: 'iniesz 6/1/2022 06:19:31'!
accept: aVisitor

	^aVisitor visitMarsRoverHeadingNorth: self! !


!classDefinition: #MarsRoverHeadingSouth category: 'Ej7-MarsRover-Logger'!
MarsRoverHeading subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'iniesz 5/30/2022 21:44:19'!
accept: aVisitor

	^aVisitor visitMarsRoverHeadingSouth: self.! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveNorth! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveSouth! !


!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headEast! !

!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headWest! !


!classDefinition: #MarsRoverHeadingWest category: 'Ej7-MarsRover-Logger'!
MarsRoverHeading subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	^marsRover moveEast! !

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveWest! !


!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headSouth! !

!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headNorth! !


!MarsRoverHeadingWest methodsFor: 'visit-heading' stamp: 'iniesz 5/30/2022 21:48:15'!
accept: aVisitor

	^aVisitor visitMarsRoverHeadingWest: self.! !


!classDefinition: #MarsRoverLogger category: 'Ej7-MarsRover-Logger'!
Object subclass: #MarsRoverLogger
	instanceVariableNames: 'outputLog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverLogger methodsFor: 'reader' stamp: 'iniesz 6/2/2022 12:50:14'!
readStream
	^ outputLog readStream.! !


!MarsRoverLogger methodsFor: 'initialization' stamp: 'iniesz 6/2/2022 12:49:21'!
initializeWith: aLogger
	outputLog  := aLogger.! !


!MarsRoverLogger methodsFor: 'mars-rover-changes' stamp: 'FM 6/1/2022 23:02:58'!
headingChangedTo: aMarsRoverHeading

	^self subclassResponsibility .! !

!MarsRoverLogger methodsFor: 'mars-rover-changes' stamp: 'FM 6/1/2022 23:41:59'!
positionChangedTo: aPosition

	^self subclassResponsibility .! !


!MarsRoverLogger methodsFor: 'visit-heading' stamp: 'FM 6/1/2022 23:00:26'!
visitMarsRoverHeadingEast: aMarsRoverHeadingEast 

	^self subclassResponsibility .! !

!MarsRoverLogger methodsFor: 'visit-heading' stamp: 'FM 6/1/2022 23:00:21'!
visitMarsRoverHeadingNorth: aMarsRoverHeadingNorth
	^self subclassResponsibility .! !

!MarsRoverLogger methodsFor: 'visit-heading' stamp: 'FM 6/1/2022 23:00:16'!
visitMarsRoverHeadingSouth: aMarsRoverHeadingSouth

	^self subclassResponsibility .! !

!MarsRoverLogger methodsFor: 'visit-heading' stamp: 'FM 6/1/2022 23:00:19'!
visitMarsRoverHeadingWest: aMarsRoverHeadingWest
	^self subclassResponsibility .! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverLogger class' category: 'Ej7-MarsRover-Logger'!
MarsRoverLogger class
	instanceVariableNames: ''!

!MarsRoverLogger class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 13:34:46'!
outputToLabelMorph: aLabelMorph
	^ self with: (LabelMorphLogger with: aLabelMorph).! !

!MarsRoverLogger class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 13:35:10'!
outputToTranscript
	^ self with: TranscriptLogger new.! !

!MarsRoverLogger class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 12:44:19'!
with: aLogger
	| logger |
	logger := self new.
	logger initializeWith: aLogger.
	^ logger.
	! !


!classDefinition: #MarsRoverHeadingDescriptionLogger category: 'Ej7-MarsRover-Logger'!
MarsRoverLogger subclass: #MarsRoverHeadingDescriptionLogger
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverHeadingDescriptionLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:26:30'!
visitMarsRoverHeadingEast: aMarsRoverHeadingEast 
	outputLog log: 'Apuntando al Este'.! !

!MarsRoverHeadingDescriptionLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:26:23'!
visitMarsRoverHeadingNorth: aMarsRoverHeadingNorth
	outputLog log: 'Apuntando al Norte'.! !

!MarsRoverHeadingDescriptionLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:26:17'!
visitMarsRoverHeadingSouth: aMarsRoverHeadingSouth
	outputLog log: 'Apuntando al Sur'.! !

!MarsRoverHeadingDescriptionLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 10:26:06'!
visitMarsRoverHeadingWest: aMarsRoverHeadingWest
	outputLog log: 'Apuntando al Oeste'.! !


!classDefinition: #MarsRoverHeadingLogger category: 'Ej7-MarsRover-Logger'!
MarsRoverLogger subclass: #MarsRoverHeadingLogger
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverHeadingLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 11:27:58'!
visitMarsRoverHeadingEast: aMarsRoverHeadingEast 
	outputLog log: 'East'.! !

!MarsRoverHeadingLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 11:28:16'!
visitMarsRoverHeadingNorth: aMarsRoverHeadingNorth
	outputLog log: 'North'.! !

!MarsRoverHeadingLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 11:28:30'!
visitMarsRoverHeadingSouth: aMarsRoverHeadingSouth
	outputLog log: 'South'.! !

!MarsRoverHeadingLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 11:28:43'!
visitMarsRoverHeadingWest: aMarsRoverHeadingWest
	outputLog log: 'West'.! !


!classDefinition: #MarsRoverPositionLogger category: 'Ej7-MarsRover-Logger'!
MarsRoverLogger subclass: #MarsRoverPositionLogger
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverPositionLogger methodsFor: 'as yet unclassified' stamp: 'iniesz 6/2/2022 12:53:06'!
positionChangedTo: aPosition
	outputLog log: aPosition asString.! !


!classDefinition: #MarsRoverLoggerProxy category: 'Ej7-MarsRover-Logger'!
Object subclass: #MarsRoverLoggerProxy
	instanceVariableNames: 'positionLoggers headingLoggers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej7-MarsRover-Logger'!

!MarsRoverLoggerProxy methodsFor: 'mars-rover-changes' stamp: 'FM 6/1/2022 23:11:27'!
headingChangedTo: aMarsRoverHeading

	aMarsRoverHeading accept: self! !

!MarsRoverLoggerProxy methodsFor: 'mars-rover-changes' stamp: 'FM 6/1/2022 23:10:00'!
positionChangedTo: aPosition

	positionLoggers do: [ :logger | logger positionChangedTo: aPosition ].
! !


!MarsRoverLoggerProxy methodsFor: 'initialization' stamp: 'iniesz 6/2/2022 12:19:39'!
initialize

	positionLoggers := OrderedCollection new.
	headingLoggers := OrderedCollection new.! !


!MarsRoverLoggerProxy methodsFor: 'add-loggers' stamp: 'FM 6/2/2022 00:36:43'!
addHeadingLogger: aMarsRoverLogger 
	
	headingLoggers add: aMarsRoverLogger! !

!MarsRoverLoggerProxy methodsFor: 'add-loggers' stamp: 'FM 6/2/2022 00:27:14'!
addPositionAndHeadingLogger: aMarsRoverLogger 
	
	positionLoggers add: aMarsRoverLogger.
	headingLoggers add: aMarsRoverLogger.! !

!MarsRoverLoggerProxy methodsFor: 'add-loggers' stamp: 'FM 6/2/2022 00:26:51'!
addPositionLogger: aMarsRoverLogger 
	
	positionLoggers add: aMarsRoverLogger! !


!MarsRoverLoggerProxy methodsFor: 'visit-heading' stamp: 'FM 6/1/2022 23:11:38'!
visitMarsRoverHeadingEast: aMarsRoverHeadingEast 
	
	headingLoggers do: [ :logger | logger visitMarsRoverHeadingEast: aMarsRoverHeadingEast ].! !

!MarsRoverLoggerProxy methodsFor: 'visit-heading' stamp: 'FM 6/1/2022 23:11:47'!
visitMarsRoverHeadingNorth: aMarsRoverHeadingNorth
	
	headingLoggers do: [ :logger | logger visitMarsRoverHeadingNorth: aMarsRoverHeadingNorth ].! !

!MarsRoverLoggerProxy methodsFor: 'visit-heading' stamp: 'FM 6/1/2022 23:11:40'!
visitMarsRoverHeadingSouth: aMarsRoverHeadingSouth
	
	headingLoggers do: [ :logger | logger visitMarsRoverHeadingSouth: aMarsRoverHeadingSouth ].! !

!MarsRoverLoggerProxy methodsFor: 'visit-heading' stamp: 'FM 6/1/2022 23:11:43'!
visitMarsRoverHeadingWest: aMarsRoverHeadingWest
	
	headingLoggers do: [ :logger | logger visitMarsRoverHeadingWest: aMarsRoverHeadingWest ].! !
