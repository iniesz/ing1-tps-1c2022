!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'tests' stamp: 'iniesz 6/26/2022 19:32:14'!
assertCustomerImporterWithData: aDataStream raisesErrorWithMessage: anErrorMessage 

	^ self should: 
		[CustomerImporter 
			valueFrom: aDataStream
			into: session.] 
	raise: Error 
	withExceptionDo: [:error |
			self assert: 		anErrorMessage
				equals: error messageText. 
		]! !

!ImportTest methodsFor: 'tests' stamp: 'iniesz 6/23/2022 19:44:55'!
test01ImportValidDataCorrectly

	CustomerImporter valueFrom: self validImportData into: session..

	self assertImportedRightNumberOfCustomers.
	self assertPepeSanchezWasImportedCorrecty.
	self assertJuanPerezWasImportedCorrectly ! !

!ImportTest methodsFor: 'tests' stamp: 'iniesz 6/26/2022 19:32:15'!
test02RecordWithInvalidTypeRaisesError
	self assertCustomerImporterWithData: 
	 	self invalidTypeRecordImportData 
		raisesErrorWithMessage: 	CustomerImporter invalidTypeRecordErrorDescription.! !

!ImportTest methodsFor: 'tests' stamp: 'iniesz 6/26/2022 19:32:14'!
test03CustomerRecordWithLessThanFiveFieldsRaisesError
	self assertCustomerImporterWithData: 
		self invalidCustomerRecordLessThanFiveFieldsImportData 
		raisesErrorWithMessage: 		CustomerImporter invalidCustomerRecordMissingFieldsErrorDescription.! !

!ImportTest methodsFor: 'tests' stamp: 'iniesz 6/26/2022 19:32:15'!
test04CustomerRecordWithMoreThanFiveFieldsRaisesError
	self assertCustomerImporterWithData: 
		self invalidCustomerRecordMoreThanFiveFieldsImportData 
		raisesErrorWithMessage: 	CustomerImporter invalidCustomerRecordExtraFieldsErrorDescription.! !

!ImportTest methodsFor: 'tests' stamp: 'iniesz 6/26/2022 19:32:14'!
test05AddressRecordWithLessThanSixFieldsRaisesError
	self assertCustomerImporterWithData: 
		self invalidAddressRecordLessThanSixFieldsImportData 
		raisesErrorWithMessage: 	CustomerImporter invalidAddressRecordMissingFieldsErrorDescription.! !

!ImportTest methodsFor: 'tests' stamp: 'iniesz 6/26/2022 19:32:14'!
test06AddressRecordWithMoreThanSixFieldsRaisesError

	self assertCustomerImporterWithData: 
		self invalidAddressRecordMoreThanSixFieldsImportData 
		raisesErrorWithMessage: 	CustomerImporter invalidAddressRecordExtraFieldsErrorDescription.! !

!ImportTest methodsFor: 'tests' stamp: 'iniesz 6/26/2022 19:32:16'!
test07AddressRecordWithNoAssociatedCustomerRaisesError

	self assertCustomerImporterWithData: 
		self invalidAddressRecordWithNoAssociatedCustomer 
		raisesErrorWithMessage: CustomerImporter invalidAddressRecordWithNoAssociatedCustomer.! !


!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:22:05'!
assertAddressOf: importedCustomer at: aStreetName hasNumber: aNumber town: aTown zipCode: aZipCode province: aProvince

	| importedAddress |

	importedAddress := importedCustomer addressAt: aStreetName ifNone: [ self fail ].
	self assert: aStreetName equals: importedAddress streetName.
	self assert: aNumber equals: importedAddress streetNumber.
	self assert: aTown equals: importedAddress town.
	self assert: aZipCode equals: importedAddress zipCode.
	self assert: aProvince equals: importedAddress province.

	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:27:57'!
assertCustomerWithIdentificationType: anIdType number: anIdNumber hasFirstName: aFirstName lastName: aLastName

	| importedCustomer |

	importedCustomer := self customerWithIdentificationType: anIdType number: anIdNumber.

	self assert: aFirstName equals: importedCustomer firstName.
	self assert: aLastName equals: importedCustomer lastName.
	self assert: anIdType equals: importedCustomer identificationType.
	self assert: anIdNumber equals: importedCustomer identificationNumber.

	^importedCustomer

	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:12:18'!
assertImportedRightNumberOfCustomers

	^ self assert: 2 equals: (session selectAllOfType: Customer) size! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:45'!
assertJuanPerezWasImportedCorrectly

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'C' number: '23-25666777-9' hasFirstName: 'Juan' lastName: 'Perez'.
	self assertAddressOf: importedCustomer at: 'Alem' hasNumber: 1122 town: 'CABA' zipCode: 1001 province: 'CABA'
	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:05'!
assertPepeSanchezWasImportedCorrecty

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'D' number: '22333444' hasFirstName: 'Pepe' lastName: 'Sanchez'.
	self assertAddressOf: importedCustomer at: 'San Martin' hasNumber: 3322 town: 'Olivos' zipCode: 1636 province: 'BsAs'.
	self assertAddressOf: importedCustomer at: 'Maipu' hasNumber: 888 town: 'Florida' zipCode: 1122 province: 'Buenos Aires'.


	! !


!ImportTest methodsFor: 'setUp/tearDown' stamp: 'HAW 5/22/2022 00:27:50'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.
! !

!ImportTest methodsFor: 'setUp/tearDown' stamp: 'HAW 5/22/2022 00:28:23'!
tearDown

	session commit.
	session close.
	! !


!ImportTest methodsFor: 'customer' stamp: 'HAW 5/22/2022 18:14:22'!
customerWithIdentificationType: anIdType number: anIdNumber

	^ (session
		select: [ :aCustomer | aCustomer identificationType = anIdType and: [ aCustomer identificationNumber = anIdNumber ]]
		ofType: Customer) anyOne! !


!ImportTest methodsFor: 'test data' stamp: 'iniesz 6/23/2022 20:35:53'!
invalidAddressRecordLessThanSixFieldsImportData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'test data' stamp: 'iniesz 6/23/2022 20:40:21'!
invalidAddressRecordMoreThanSixFieldsImportData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,163,6,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'test data' stamp: 'iniesz 6/23/2022 21:00:31'!
invalidAddressRecordWithNoAssociatedCustomer
	^ ReadStream on:
'A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'test data' stamp: 'iniesz 6/23/2022 20:21:35'!
invalidCustomerRecordLessThanFiveFieldsImportData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'test data' stamp: 'iniesz 6/23/2022 20:31:08'!
invalidCustomerRecordMoreThanFiveFieldsImportData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23,25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'test data' stamp: 'iniesz 6/23/2022 20:17:10'!
invalidTypeRecordImportData

	^ ReadStream on:
'CC,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'test data' stamp: 'HAW 5/22/2022 18:08:08'!
validImportData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 17:55:46'!
isAt: aStreetName

	^streetName = aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 17:55:17'!
addressAt: aStreetName ifNone: aNoneBlock

	^addresses detect: [ :address | address isAt: aStreetName ] ifNone: aNoneBlock ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session readStream newCustomer line record firstCustomerImported'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'iniesz 6/23/2022 21:09:44'!
initializeFrom: aReadStream into: aSession
	session := aSession.
	readStream := aReadStream.
	firstCustomerImported := false.! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'iniesz 6/23/2022 19:43:14'!
hasNextRecord

	line := readStream nextLine. 
	^ line notNil.! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'iniesz 6/23/2022 21:06:55'!
importAddresses
	| newAddress |
	
	self assertAddressRecordFieldsLength.
	self assertAddressRecordAssociatedToCustomer.
	
	newAddress := Address new.
	newCustomer addAddress: newAddress.
	newAddress streetName: record second.
	newAddress streetNumber: record third asNumber .
	newAddress town: record fourth.
	newAddress zipCode: record fifth asNumber .
	newAddress province: record sixth! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'iniesz 6/23/2022 21:05:26'!
importCustomer

	self assertCustomerRecordFieldsLength.

	firstCustomerImported := true.
	
	newCustomer := Customer new.
	newCustomer firstName: record second.
	newCustomer lastName: record third.
	newCustomer identificationType: record fourth.
	newCustomer identificationNumber: record fifth.
	session persist: newCustomer! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'iniesz 6/23/2022 20:49:56'!
importRecord
	
	self assertRecordValidType.
	
	(self isCustomerRecord) ifTrue: [ ^ self importCustomer ].
	(self isAddressRecord) ifTrue: [ ^ self importAddresses ].
	
	
	! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'iniesz 6/23/2022 19:57:33'!
isAddressRecord

	^ record first = 'A'.! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'iniesz 6/23/2022 19:57:24'!
isCustomerRecord

	^ record first = 'C'.! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'iniesz 6/23/2022 19:41:33'!
readRecord

	^ record := line findTokens: $,! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'iniesz 6/23/2022 19:43:48'!
value

	[ self hasNextRecord] 
		whileTrue: [
			self readRecord.
			self importRecord.
		].

	! !


!CustomerImporter methodsFor: 'assertions' stamp: 'iniesz 6/23/2022 21:06:32'!
assertAddressRecordAssociatedToCustomer
	
	(firstCustomerImported)
		ifFalse:[
			self error: self class invalidAddressRecordWithNoAssociatedCustomer.
			]! !

!CustomerImporter methodsFor: 'assertions' stamp: 'iniesz 6/23/2022 20:43:28'!
assertAddressRecordFieldsLength
	(record size <6)
	ifTrue:[ self error: self class invalidAddressRecordMissingFieldsErrorDescription.].
	
	(record size >6)
	ifTrue:[ self error: self class invalidAddressRecordExtraFieldsErrorDescription.].! !

!CustomerImporter methodsFor: 'assertions' stamp: 'iniesz 6/23/2022 20:44:40'!
assertCustomerRecordFieldsLength
	(record size < 5)
	ifTrue:[ self error: self class invalidCustomerRecordMissingFieldsErrorDescription ].
	
	(record size > 5)
	ifTrue:[ self error: self class invalidCustomerRecordExtraFieldsErrorDescription ].! !

!CustomerImporter methodsFor: 'assertions' stamp: 'iniesz 6/23/2022 20:15:39'!
assertRecordValidType
	((self isCustomerRecord) or:[ self isAddressRecord])
	ifFalse:[self error: self class invalidTypeRecordErrorDescription].
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 18:06:47'!
from: aReadStream into: aSession
	^self new initializeFrom: aReadStream into: aSession! !


!CustomerImporter class methodsFor: 'importing' stamp: 'HAW 5/22/2022 18:11:27'!
valueFrom: aReadStream into: aSession

	^(self from: aReadStream into: aSession) value! !


!CustomerImporter class methodsFor: 'error-descriptions' stamp: 'iniesz 6/23/2022 20:40:57'!
invalidAddressRecordExtraFieldsErrorDescription
	^ 'The record has extra fields, an address record should have 6 fields.'.! !

!CustomerImporter class methodsFor: 'error-descriptions' stamp: 'iniesz 6/23/2022 20:36:44'!
invalidAddressRecordMissingFieldsErrorDescription
	^ 'The record is missing fields, an address record should have 6 fields.'.! !

!CustomerImporter class methodsFor: 'error-descriptions' stamp: 'iniesz 6/23/2022 21:01:36'!
invalidAddressRecordWithNoAssociatedCustomer
	^ 'Invalid address record have no customer associated'.! !

!CustomerImporter class methodsFor: 'error-descriptions' stamp: 'iniesz 6/23/2022 20:31:56'!
invalidCustomerRecordExtraFieldsErrorDescription
	^ 'The record has extra fields, a customer record should have 5 fields.'.! !

!CustomerImporter class methodsFor: 'error-descriptions' stamp: 'iniesz 6/23/2022 20:24:33'!
invalidCustomerRecordMissingFieldsErrorDescription
	^ 'The record is missing fields, a customer record should have 5 fields.'.! !

!CustomerImporter class methodsFor: 'error-descriptions' stamp: 'iniesz 6/23/2022 19:55:41'!
invalidTypeRecordErrorDescription
	^ 'The record have an invalid type.'! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
