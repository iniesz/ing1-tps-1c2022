!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'iniesz 4/17/2022 20:56:03'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| customerBook |

	customerBook := CustomerBook new.
		
	self assertBlock: [ customerBook addCustomerNamed: 'John Lennon'. ] executionTimeIsLowerThan: (50 * millisecond).! !

!CustomerBookTest methodsFor: 'testing' stamp: 'iniesz 4/18/2022 14:56:20'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook paulMcCartney |
		
	paulMcCartney := 'Paul McCartney'.
	customerBook := self newCustomerBookWith: (Bag with: paulMcCartney).
		
	self assertBlock: [ customerBook removeCustomerNamed: 	paulMcCartney. ] 
		executionTimeIsLowerThan: (100 * millisecond).
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'iniesz 4/18/2022 12:09:23'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.

	self assertBlock:[customerBook addCustomerNamed: ''.]
		throwsException: Error
		withExceptionDo: [ :anError | 
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty ].! !

!CustomerBookTest methodsFor: 'testing' stamp: 'iniesz 4/18/2022 14:55:56'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
	
	johnLennon := 'John Lennon'.
	customerBook := self newCustomerBookWith: (Bag with: johnLennon).
	
		
	self assertBlock: [ customerBook removeCustomerNamed: 'Paul McCartney'. ]
		throwsException: NotFound
		withExceptionDo: [
				self assert: customerBook numberOfCustomers = 1.
				self assert: (customerBook includesCustomerNamed: johnLennon). 
			].
		! !

!CustomerBookTest methodsFor: 'testing' stamp: 'iniesz 4/18/2022 14:57:02'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney testCustomers |
	
	paulMcCartney := 'Paul McCartney'.
	testCustomers := (Bag with: paulMcCartney).
	customerBook := self newCustomerBookWith: testCustomers
					thenSuspend: testCustomers.
	
	self assertCustomerBook: customerBook
		hasCustomersCount: 1
		hasActiveCustomersCount: 0
		hasSuspendedCustomersCount: 1
		andAsserts: [ customerBook includesCustomerNamed: paulMcCartney ] .! !

!CustomerBookTest methodsFor: 'testing' stamp: 'iniesz 4/18/2022 14:58:04'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney testCustomers |
		
	paulMcCartney := 'Paul McCartney'.
	testCustomers := (Bag with: paulMcCartney).
	customerBook := self newCustomerBookWith: testCustomers
					thenSuspend: testCustomers
					thenRemove: testCustomers.
					
	self assertCustomerBook: customerBook
		hasCustomersCount: 0
		hasActiveCustomersCount: 0
		hasSuspendedCustomersCount: 0
		andAsserts: [ (customerBook includesCustomerNamed: paulMcCartney) not ].! !

!CustomerBookTest methodsFor: 'testing' stamp: 'iniesz 4/18/2022 14:58:33'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon testCustomers |

	johnLennon := 'John Lennon'.
	testCustomers := Bag with: johnLennon.
	customerBook := self newCustomerBookWith: testCustomers.
	
	
	self assertBlock:[ customerBook suspendCustomerNamed: 'George Harrison'. ] 
		throwsException: CantSuspend
		withExceptionDo: [ :anError | 
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: johnLennon). ].! !

!CustomerBookTest methodsFor: 'testing' stamp: 'iniesz 4/18/2022 14:59:10'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon testCustomers |
	
	johnLennon := 'John Lennon'.
	testCustomers := (Bag with: johnLennon).
	customerBook := self newCustomerBookWith: testCustomers
					thenSuspend: testCustomers.				
		
	self assertBlock:[ customerBook suspendCustomerNamed: johnLennon. ] 
		throwsException: CantSuspend
		withExceptionDo: [ :anError | 
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: johnLennon). ].! !


!CustomerBookTest methodsFor: 'operations' stamp: 'iniesz 4/17/2022 20:56:22'!
assertBlock: aBlock executionTimeIsLowerThan: amountOfTime
	"comment stating purpose of message"

	| millisecondsBeforeRunning millisecondsAfterRunning |
	
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	aBlock value.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	^self assert: millisecondsAfterRunning - millisecondsBeforeRunning < amountOfTime! !

!CustomerBookTest methodsFor: 'operations' stamp: 'iniesz 4/18/2022 12:33:05'!
assertBlock: aBlock throwsException: anException withExceptionDo: assertionBlock 
	"comment stating purpose of message"
	
	
	[ aBlock value.
	self fail. ]
		on: anException 
		do: assertionBlock.! !

!CustomerBookTest methodsFor: 'operations' stamp: 'iniesz 4/18/2022 13:40:50'!
assertCustomerBook: aCustomerBook  hasCustomersCount: customersCount
hasActiveCustomersCount: activeCustomersCount 
hasSuspendedCustomersCount: suspendedCustomersCount
andAsserts: condition
	
	self assert: customersCount equals: aCustomerBook numberOfCustomers.	
	self assert: activeCustomersCount equals: aCustomerBook numberOfActiveCustomers.
	self assert: suspendedCustomersCount equals: aCustomerBook numberOfSuspendedCustomers.
	self assert: condition value.! !

!CustomerBookTest methodsFor: 'operations' stamp: 'iniesz 4/18/2022 13:19:36'!
newCustomerBookWith: customers
	| customerBook |
	
	customerBook := CustomerBook new.
	customers do: [:aCustomer | customerBook addCustomerNamed: aCustomer].
	
	^customerBook.! !

!CustomerBookTest methodsFor: 'operations' stamp: 'iniesz 4/18/2022 13:20:12'!
newCustomerBookWith: customers thenSuspend: customersToSuspend
	| customerBook |
	
	customerBook := self newCustomerBookWith: customers.
	customersToSuspend do: 
		[:aCustomerToSuspend | 
			customerBook suspendCustomerNamed: aCustomerToSuspend].
	
	^customerBook.! !

!CustomerBookTest methodsFor: 'operations' stamp: 'iniesz 4/18/2022 13:20:21'!
newCustomerBookWith: customers thenSuspend: customersToSuspend thenRemove: customersToRemove
	| customerBook |
	
	customerBook := self newCustomerBookWith: customers thenSuspend: customersToSuspend.
	customersToRemove do: 
		[:aCustomerToRemove |
			customerBook removeCustomerNamed: aCustomerToRemove].
	
	^customerBook.! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'iniesz 4/18/2022 14:36:06'!
activeCustomers
	^ active.! !

!CustomerBook methodsFor: 'testing' stamp: 'iniesz 4/18/2022 13:55:15'!
includesCustomerNamed: aName

	^(self isActiveCustomer: aName) or: [ self isSuspendedCustomer: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'iniesz 4/17/2022 20:36:45'!
isActiveCustomer: aName
	^ active includes: aName.! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !

!CustomerBook methodsFor: 'testing' stamp: 'iniesz 4/17/2022 20:37:07'!
isSuspendedCustomer: aName
	^ suspended includes: aName.! !

!CustomerBook methodsFor: 'testing' stamp: 'iniesz 4/18/2022 14:36:13'!
suspendedCustomers
	^ suspended.! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'iniesz 4/17/2022 20:41:57'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfCustomers
	
	^active size + suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'iniesz 4/18/2022 14:27:55'!
removeCustomerNamed: aName 
 	active remove: aName 
		ifAbsent: [ 
			suspended remove: aName 
			ifAbsent: 
				[^ NotFound signal.]].
			
	^ aName.
	
	
	
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
suspendCustomerNamed: aName 
	
	(active includes: aName) ifFalse: [^CantSuspend signal].
	
	active remove: aName.
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/11/2022 07:18:12'!
customerAlreadyExistsErrorMessage

	^'Customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/11/2022 07:18:16'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty!!!!!!'! !
