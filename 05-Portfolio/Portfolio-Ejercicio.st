!classDefinition: #ReceptiveAccountTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:44'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/21/2022 01:54:14'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	self on: account deposit: 100.
		
	self assert: 100 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:52'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:32'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 5/16/2022 16:18:39'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 5/16/2022 16:19:11'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| deposit withdraw account |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 200.
	withdraw := Withdraw for: 50.
	
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:14:01'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := Deposit register: 50 on: account1.
		
	self assert: 1 equals: account1 transactions size.
	self assert: (account1 transactions includes: deposit1).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/19/2022 20:30:03'!
test08EmptyPorfolioBalanceIsZero
	| portfolio |
	
	portfolio := Portfolio new.
	
	self assert: portfolio balance equals: 0.! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/19/2022 20:49:21'!
test09AddZeroBalanceAccountToPortfolioKeepsBalanceAtZero
"test09PortfolioWithAccountBalanceIsZeroIfAccountBalanceIsZero"
	| portfolio account |
	
	account := ReceptiveAccount new.
	portfolio := Portfolio new.
	
	portfolio add: account.
	
	self assert: portfolio balance equals: 0.! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/21/2022 01:55:26'!
test10PortfolioAddsNonZeroBalanceAccountThenBalanceIsAccountBalance
"test09PortfolioWithAccountBalanceIsZeroIfAccountBalanceIsZero"
	| portfolio account |
	
	account := self createAccountBalance50.
	portfolio := Portfolio new.
	
	portfolio add: account.
	
	self assert: portfolio balance equals: account balance.! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/21/2022 01:56:17'!
test11PortfolioAddsMultipleAccountsThenBalanceIsSumOfAccountsBalance
"test09PortfolioWithAccountBalanceIsZeroIfAccountBalanceIsZero"
	| portfolio account1 account2 |
	
	account1 := self createAccountBalance50.
	account2 := self createAccountBalance100.

	portfolio := Portfolio new.
	portfolio add: account1.
	portfolio add: account2.
	
	self assert: portfolio balance equals: (account1 balance + account2 balance).! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/19/2022 21:41:54'!
test12EmptyPortfolioHasNotRegisteredTransactions
	| portfolio  account transaction |
	
	account := ReceptiveAccount new.
	transaction := Deposit register: 100 on: account.
	portfolio := Portfolio new.
	
	self assert: (portfolio hasRegistered: transaction) equals: false.! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/21/2022 00:27:21'!
test13PortfolioWithAccountRegistersAccountTransactions
	| portfolio  account transaction |
	
	account := ReceptiveAccount new.
	transaction := Deposit register: 100 on: account.
	portfolio := Portfolio new.
	portfolio add: account.
	
	self assert: (portfolio hasRegistered: transaction).! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/21/2022 00:41:14'!
test14PortfolioWithMultipleAccountsRegistersAccountsTransactions
	| portfolio account1 account2 transaction1 transaction2 |
	
	account1 := ReceptiveAccount new.
	transaction1 := Deposit register: 100 on: account1.

	account2 := ReceptiveAccount new.
	transaction2 := Deposit register: 150 on: account2.	
	
	portfolio := Portfolio new.
	portfolio add: account1.
	portfolio add: account2.
	
	self assert: (portfolio hasRegistered: transaction1).
	self assert: (portfolio hasRegistered: transaction2).! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/21/2022 00:43:50'!
test15PortfolioDoesNotRegisterTransactionsThatDontBelongToItsAccounts
	| portfolio  account transaction accountNotInPortfolio transactionNotInPortfolio |
	
	account := ReceptiveAccount new.
	transaction := Deposit register: 100 on: account.
	portfolio := Portfolio new.
	portfolio add: account.
	
	accountNotInPortfolio := ReceptiveAccount new.
	transactionNotInPortfolio := Deposit register: 200 on: accountNotInPortfolio.
		
	self deny: (portfolio hasRegistered: transactionNotInPortfolio ).! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/21/2022 01:19:42'!
test16PortfolioShouldFailWhenAddingSomethingInvalid
	| portfolio  |
	portfolio := Portfolio new.
	
	self should: [portfolio add: 'This is an invalid object to add to a portfolio']
		raise: Error 
		description: Portfolio errorDescriptionYouCanOnlyAddAccountsOrPortfoliosToPortfolio.
	
	
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/21/2022 01:58:41'!
test17PortfolioWithOnePortfolioAddsItsBalance
	| account portfolio anotherAccount includedPortfolio |
	account := self createAccountBalance100. 
	portfolio := Portfolio new.
	portfolio add: account.
	
	anotherAccount := self createAccountBalance50.
	includedPortfolio := Portfolio new.
	includedPortfolio add: anotherAccount.
	
	portfolio add: includedPortfolio.
	
	self assert: portfolio balance 
		equals: (account balance + includedPortfolio balance).
	
	
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/21/2022 02:05:32'!
test18PortfolioWithMultiplePortfoliosAddsItsBalance
	| account portfolio includedPortfolio1 includedPortfolio2  |
	portfolio := Portfolio new.
	account := self createAccountBalanceNegative30.
	portfolio add: account.
	
	includedPortfolio1 := self createPortfolioBalance170.
	includedPortfolio2 := self createPortfolioBalance200.
	
	portfolio add: includedPortfolio1.
	portfolio add: includedPortfolio2.
	
	self assert: portfolio balance 
		equals: (account balance + includedPortfolio1 balance + includedPortfolio2 balance).
	
	
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/21/2022 12:30:42'!
test19PortfolioRaisesErrorWhenAddingAlreadyIncludedAccount
	| portfolio account |
	portfolio := Portfolio new.
	account := self createAccountBalance100.
	
	portfolio add: account.
	
	self should: [portfolio add: account]
		raise: Error
		description: Portfolio errorDescriptionAccountAlreadyBelongsToPortfolio.! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/21/2022 12:32:04'!
test20PortfolioRaisesErrorWhenAddingAlreadyIncludedPortfolio
	| portfolio account includedPortfolio |
	portfolio := Portfolio new. 
	includedPortfolio := Portfolio new.
	
	portfolio add: includedPortfolio.
	
	self should: [portfolio add: includedPortfolio]
		raise: Error
		description: Portfolio errorDescriptionPortfolioAlreadyBelongsToPortfolio .! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/22/2022 20:19:50'!
test21ReceptiveAccountAddedToPortfolioIsRegisteredInPortfolio
	| portfolio account |
	portfolio := Portfolio new.
	account := self createAccountBalance100.
	
	portfolio add: account.
	self assert: (portfolio hasRegistered: account).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 13:34:47'!
test21ReceptiveAccountNotAddedToPortfolioIsNotRegisteredInPortfolio
	| portfolio account |
	portfolio := Portfolio new.
	account := self createAccountBalance100.
	
	self deny: (portfolio hasRegistered: account).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/22/2022 20:20:42'!
test22PortfolioAddedToPortfolioIsRegisteredInPortfolio
	| portfolio includedPortfolio |
	portfolio := Portfolio new.
	
	includedPortfolio := self createPortfolioBalance170.
	portfolio add: includedPortfolio.
	
	self assert: (portfolio hasRegistered: includedPortfolio).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 13:35:23'!
test22PortfolioNotAddedToPortfolioIsNotRegisteredInPortfolio
	| portfolio otherPortfolio |
	portfolio := Portfolio new.
	otherPortfolio := self createPortfolioBalance170.
	
	self deny: (portfolio hasRegistered: otherPortfolio).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/22/2022 20:47:21'!
test23ReceptiveAccountAddedToPortfolioIsRegisteredInParentPortfolio
	| portfolio account childPortfolio |
	portfolio := Portfolio new.
	childPortfolio := self createPortfolioBalance170.
	account := self createAccountBalance100.
	childPortfolio add:account.
	
	portfolio add: childPortfolio.
	
	self assert: (portfolio hasRegistered: account).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 13:36:07'!
test23ReceptiveAccountNotAddedToPortfolioIsNotRegisteredInParentPortfolio
	| portfolio account childPortfolio |
	portfolio := Portfolio new.
	childPortfolio := self createPortfolioBalance170.
	portfolio add: childPortfolio.
	
	account := self createAccountBalance100.
	
	self deny: (childPortfolio hasRegistered: account).
	self deny: (portfolio hasRegistered: account).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/22/2022 20:49:24'!
test24PortfolioAddedToPortfolioIsRegisteredInParentPortfolio
	| portfolio account portfolioSecondLevel portfolioThirdLevel |
	portfolio := Portfolio new.
	portfolioSecondLevel := self createPortfolioBalance170.
	portfolioThirdLevel := self createPortfolioBalance170 .
	
	portfolioSecondLevel add: portfolioThirdLevel.
	portfolio add: portfolioSecondLevel.
	
	
	self assert: (portfolio hasRegistered: portfolioSecondLevel).
	self assert: (portfolio hasRegistered: portfolioThirdLevel).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 13:37:45'!
test24PortfolioNotAddedToPortfolioIsNotRegisteredInParentPortfolio
	| portfolio childPortfolio otherPortfolio |
	portfolio := Portfolio new.
	childPortfolio := self createPortfolioBalance170.
	otherPortfolio := self createPortfolioBalance170 .
	
	portfolio add: childPortfolio.
	
	
	self assert: (portfolio hasRegistered: childPortfolio).
	self deny: (childPortfolio hasRegistered: otherPortfolio).
	self deny: (portfolio hasRegistered: otherPortfolio).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 13:38:20'!
test25PortfolioIsNotReferencedByNotAddedReceptiveAccount
	| portfolio account |
	portfolio := Portfolio new.
	account := self createAccountBalance100.
	
	self deny: (account isRegisteredBy: portfolio).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/22/2022 21:07:57'!
test25PortfolioIsReferencedByAddedReceptiveAccount
	| portfolio account |
	portfolio := Portfolio new.
	account := self createAccountBalance100.
	
	portfolio add: account.
	self assert: (account isRegisteredBy: portfolio).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 13:39:01'!
test26PortfolioIsNotReferencedByNotAddedPortfolio
	| portfolio otherPortfolio |
	portfolio := Portfolio new.
	otherPortfolio := self createPortfolioBalance170.
	self deny: (otherPortfolio isRegisteredBy: portfolio).
	
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/22/2022 21:08:20'!
test26PortfolioIsReferencedByAddedPortfolio
	| portfolio includedPortfolio |
	portfolio := Portfolio new.
	
	includedPortfolio := self createPortfolioBalance170.
	portfolio add: includedPortfolio.
	self assert: (includedPortfolio isRegisteredBy: portfolio).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 13:39:54'!
test27ParentPortfolioIsNotReferencedByNotAddedReceptiveAccount
	| portfolio account parentPortfolio |
	portfolio := Portfolio new.
	parentPortfolio := Portfolio new.
	parentPortfolio add: portfolio.
	
	account := self createAccountBalance100.
	
	
	self deny: (account isRegisteredBy: portfolio).
	self deny: (account isRegisteredBy: parentPortfolio).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/22/2022 21:06:57'!
test27ParentPortfolioIsReferencedByAddedReceptiveAccount
	| portfolio account parentPortfolio |
	portfolio := Portfolio new.
	account := self createAccountBalance100.
	portfolio add: account.
	
	parentPortfolio := Portfolio new.
	parentPortfolio add: portfolio.
	
	self assert: (account isRegisteredBy: portfolio).
	self assert: (account isRegisteredBy: parentPortfolio).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 13:51:28'!
test28ParentPortfolioIsNotReferencedByNotAddedPortfolio
	| portfolio parentPortfolio childPortfolio otherPortfolio |
	childPortfolio := self createPortfolioBalance170. 
	parentPortfolio := Portfolio new.
	parentPortfolio add: childPortfolio.
	
	otherPortfolio := self createPortfolioBalance200.
	
	self deny: (otherPortfolio isRegisteredBy: portfolio).
	self deny: (otherPortfolio isRegisteredBy: parentPortfolio).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 13:52:51'!
test28ParentPortfolioIsReferencedByAddedPortfolio
	| portfolio account parentPortfolio childPortfolio otherPortfolio |
	childPortfolio := self createPortfolioBalance170. 
	parentPortfolio := Portfolio new.
	parentPortfolio add: childPortfolio.
	
	portfolio := self createPortfolioBalance200.
	childPortfolio add: portfolio.
	
	self assert: (portfolio isRegisteredBy: childPortfolio).
	self assert: (portfolio isRegisteredBy: parentPortfolio).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 14:06:32'!
test29PortfolioTreeWithMultipleAddsCorrectlyRegisters
	| parent1 parent2 child1 child2 child3 otherPortfolio |
	parent1 := Portfolio new.
	parent2 := self createPortfolioBalance170.
	otherPortfolio := self createPortfolioBalance170.
	
	child1 := self createPortfolioBalance200. 
	child2 := Portfolio new.
	child3 := self createPortfolioBalance200.  
	
	parent1 add:child1.
	parent1 add:child2.
	parent2 add:child2.
	parent2 add:child3.
	
	self assert: (parent1 hasRegistered: child1).
	self assert: (parent1 hasRegistered: child2).
	self assert: (parent2 hasRegistered: child2).
	self assert: (parent2 hasRegistered: child3).
	
	self deny: (parent1 hasRegistered: child3).
	self deny: (parent2 hasRegistered: child1).
	self deny: (otherPortfolio hasRegistered: child1).
	self deny: (otherPortfolio hasRegistered: child2).
	self deny: (otherPortfolio hasRegistered: child3).
		
	self assert: (child1 isRegisteredBy: parent1).
	self assert: (child2 isRegisteredBy: parent1).
	self assert: (child2 isRegisteredBy: parent2).
	self assert: (child3 isRegisteredBy: parent2).
	
	self deny: (child3 isRegisteredBy: parent1).
	self deny: (child1 isRegisteredBy: parent2).
	self deny: (child2 isRegisteredBy: otherPortfolio).
	self deny: (child1 isRegisteredBy: otherPortfolio).
	
	
	
	
	
	
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 16:33:39'!
test30TwoPortfoliosWithSameAccountIncludeAccount

	| sameChild portfolio1 portfolio2 |
	portfolio1 := self createPortfolioBalance170.
	portfolio2 := self createPortfolioBalance200.	
	
	sameChild := self createAccountBalance100.
	
	portfolio1 add: sameChild.
	portfolio2 add: sameChild.
	
	self assert: (portfolio1 hasRegistered: sameChild).
	self assert: (portfolio2 hasRegistered: sameChild).! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 16:33:44'!
test31TwoPortfoliosWithSamePortfolioIncludePortfolio

	| sameChild portfolio1 portfolio2 |
	portfolio1 := self createPortfolioBalance170.
	portfolio2 := self createPortfolioBalance200.	
	
	sameChild := self createPortfolioBalance170. 
	
	portfolio1 add: sameChild.
	portfolio2 add: sameChild.
	
	self assert: (portfolio1 hasRegistered: sameChild).
	self assert: (portfolio2 hasRegistered: sameChild).! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'iniesz 5/23/2022 16:33:49'!
test32TwoPortfoliosWithTheSameParentAreIncluded
	| portfolio1 portfolio2 parentPortfolio |
	portfolio1 := self createPortfolioBalance170.
	portfolio2 := self createPortfolioBalance200.	
	
	parentPortfolio := self createPortfolioBalance170. 
	
	parentPortfolio add: portfolio1.
	parentPortfolio add: portfolio2.
	
	self assert: (parentPortfolio hasRegistered: portfolio2).
	self assert: (parentPortfolio hasRegistered: portfolio1).! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'FM 5/23/2022 18:06:52'!
test33PortfoliosHasCorrectlyIntersections

	| portfolio1 portfolio2 portfolio3 accountX portfolioX |
	
	portfolio1 := self createPortfolioBalance170.
	portfolio2 := self createPortfolioBalance200.	
	portfolio3 := self createPortfolioBalance200.	
	portfolioX := self createPortfolioBalance170.
	accountX := self createAccountBalance100.

	portfolio3 add: portfolio1.
	portfolio3 add: portfolio2.
	portfolio2 add: accountX.
	portfolioX add: accountX.

	self assert: (portfolio3 existIntersectionWith: portfolio2).
	self assert: (portfolio3 existIntersectionWith: portfolio1).
	self assert: (portfolio3 existIntersectionWith: accountX).
	self assert: (portfolio3 existIntersectionWith: portfolioX).
	
	self assert: (portfolio2 existIntersectionWith: accountX).
	
	self assert: (portfolioX existIntersectionWith: accountX).
	self assert: (portfolioX existIntersectionWith: portfolio3).
	self deny: (portfolioX existIntersectionWith: portfolio1).
	! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'FM 5/23/2022 17:54:13'!
test34PortfoliosCantAddAccountIfExistIntersections

	| portfolio1 portfolio2 portfolio3 accountX portfolioX |
	
	portfolio1 := self createPortfolioBalance170.
	portfolio2 := self createPortfolioBalance200.	
	portfolio3 := self createPortfolioBalance200.	
	portfolioX := self createPortfolioBalance170.
	accountX := self createAccountBalance100.
	
	portfolio3 add: portfolio1.
	portfolio3 add: portfolio2.
	portfolio2 add: accountX.
	portfolioX add: accountX.
	
	self should: [portfolio3 add: accountX]
		raise: Error
		description: Portfolio errorDescriptionOverlappingAccountingBooks .! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'FM 5/23/2022 17:56:16'!
test35PortfoliosCantAddPortfolioIfExistIntersections

	| portfolio1 portfolio2 portfolio3 accountX portfolioX |
	
	portfolio1 := self createPortfolioBalance170.
	portfolio2 := self createPortfolioBalance200.	
	portfolio3 := self createPortfolioBalance200.	
	portfolioX := self createPortfolioBalance170.
	accountX := self createAccountBalance100.
	
	portfolio3 add: portfolio1.
	portfolio3 add: portfolio2.
	portfolio2 add: accountX.
	portfolioX add: accountX.
	
	self should: [portfolioX add: portfolio2.]
		raise: Error
		description: Portfolio errorDescriptionOverlappingAccountingBooks .! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'FM 5/23/2022 17:59:20'!
test36PortfoliosCanAddPortfolioIfNotExistIntersections

	| portfolio1 portfolio2 portfolio3 accountX portfolioX |
	
	portfolio1 := self createPortfolioBalance170.
	portfolio2 := self createPortfolioBalance200.	
	portfolio3 := self createPortfolioBalance200.	
	portfolioX := self createPortfolioBalance170.
	accountX := self createAccountBalance100.
	
	portfolio3 add: portfolio1.
	portfolio3 add: portfolio2.
	portfolio2 add: accountX.
	portfolioX add: accountX.
	
	portfolioX add: portfolio1.

	self assert: (portfolio1 isRegisteredBy: portfolioX).
	self assert: (portfolioX includes: portfolio1).! !


!ReceptiveAccountTest methodsFor: 'accounts' stamp: 'iniesz 5/21/2022 01:50:29'!
createAccountBalance100
	| account |
	account := ReceptiveAccount new.
	self on: account deposit: 100.
	^ account.! !

!ReceptiveAccountTest methodsFor: 'accounts' stamp: 'iniesz 5/21/2022 01:51:04'!
createAccountBalance50
	| account |
	account := self createAccountBalance100.
	
	self on: account withdraw: 50.
	
	^ account.! !

!ReceptiveAccountTest methodsFor: 'accounts' stamp: 'FM 5/23/2022 18:06:05'!
createAccountBalanceNegative30

	| account |
	
	account := self createAccountBalance50.
	
	self on: account withdraw: 80.
	
	^ account.! !

!ReceptiveAccountTest methodsFor: 'accounts' stamp: 'iniesz 5/21/2022 01:29:36'!
on: anAccount deposit: aDepositSum 
	^ Deposit register: aDepositSum on: anAccount.! !

!ReceptiveAccountTest methodsFor: 'accounts' stamp: 'iniesz 5/21/2022 01:30:06'!
on: anAccount withdraw: aWithdrawSum 
	^ Withdraw register: aWithdrawSum on: anAccount.! !


!ReceptiveAccountTest methodsFor: 'portfolios' stamp: 'iniesz 5/21/2022 02:01:18'!
createPortfolioBalance170
	| portfolio |
	portfolio := self createPortfolioBalance200.
	portfolio add: self createAccountBalanceNegative30 .
	^ portfolio.! !

!ReceptiveAccountTest methodsFor: 'portfolios' stamp: 'iniesz 5/21/2022 02:00:43'!
createPortfolioBalance200
	| portfolio |
	portfolio := Portfolio new.
	
	portfolio add: self createAccountBalance100.
	portfolio add: self createAccountBalance50.
	portfolio add: self createAccountBalance50.	
	
	^ portfolio.! !

!ReceptiveAccountTest methodsFor: 'portfolios' stamp: 'FM 5/23/2022 18:10:22'!
createPortfolioTreeOfHeight2

	| parentPortfolio |
	
	parentPortfolio := Portfolio new.
	
	parentPortfolio add: (self createPortfolioBalance200).
	parentPortfolio add: (self createPortfolioBalance170).
	
	^ parentPortfolio.! !


!classDefinition: #Accounting category: 'Portfolio-Ejercicio'!
Object subclass: #Accounting
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Accounting methodsFor: 'balance' stamp: 'iniesz 5/21/2022 14:30:41'!
balance
	self subclassResponsibility.! !


!classDefinition: #AccountTransaction category: 'Portfolio-Ejercicio'!
Accounting subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'balance' stamp: 'iniesz 5/21/2022 14:26:55'!
balance 

	self subclassResponsibility ! !


!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/17/2019 03:22:00'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'balance' stamp: 'iniesz 5/19/2022 20:24:50'!
balance
	^ self value.! !


!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Withdraw category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'balance' stamp: 'iniesz 5/19/2022 20:25:18'!
balance
	^ self value * -1.! !


!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #AccountingBook category: 'Portfolio-Ejercicio'!
Accounting subclass: #AccountingBook
	instanceVariableNames: 'accountingEntries referringAccountingBooks'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountingBook methodsFor: 'initialization' stamp: 'FM 5/23/2022 17:19:26'!
initialize

	accountingEntries := OrderedCollection new.
	
	referringAccountingBooks := OrderedCollection new.! !


!AccountingBook methodsFor: 'accounting-private' stamp: 'FM 5/23/2022 17:19:32'!
addReferring: anAccountingBook

	referringAccountingBooks add: anAccountingBook.! !


!AccountingBook methodsFor: 'accounting' stamp: 'FM 5/23/2022 17:09:01'!
add: anAccountingObject

	self subclassResponsibility.! !

!AccountingBook methodsFor: 'accounting' stamp: 'FM 5/23/2022 17:19:49'!
hasRegistered: anAccounting

	self subclassResponsibility.! !

!AccountingBook methodsFor: 'accounting' stamp: 'FM 5/23/2022 16:58:25'!
includes: anAccountingEntry

	^ self subclassResponsibility.! !

!AccountingBook methodsFor: 'accounting' stamp: 'FM 5/23/2022 17:27:57'!
transactions

	^ self subclassResponsibility.! !


!AccountingBook methodsFor: 'testing' stamp: 'FM 5/23/2022 17:19:12'!
isRegisteredBy: anAccountingBook

	^ (referringAccountingBooks includes: anAccountingBook) 
	
	or: 
	
	[referringAccountingBooks anySatisfy: [:aReferringAccountingBook | aReferringAccountingBook isRegisteredBy: anAccountingBook]].! !


!AccountingBook methodsFor: 'balance' stamp: 'FM 5/23/2022 17:19:39'!
balance

	^ accountingEntries sum: [:anAccountingEntry | anAccountingEntry balance]
	
					ifEmpty:[0].! !


!AccountingBook methodsFor: 'intersection-private' stamp: 'FM 5/23/2022 17:08:06'!
getRoots

	| roots |

	referringAccountingBooks isEmpty ifTrue: [ ^ OrderedCollection with: self ].

	roots := OrderedCollection new.

	referringAccountingBooks do: [ :anAccountingBook | anAccountingBook getRoots do: [ :aRoot | roots add: aRoot ]].

	^ roots.! !

!AccountingBook methodsFor: 'intersection-private' stamp: 'FM 5/23/2022 16:53:52'!
isIncludedInTree: aRoot

	^ self subclassResponsibility .! !


!AccountingBook methodsFor: 'intersection' stamp: 'FM 5/23/2022 17:21:20'!
existIntersectionWith: anAccountingEntry

    | roots |

    roots := self getRoots.

    ^ roots detect: [ :root | anAccountingEntry isIncludedInTree: root]
		ifFound:[ :root |  ^ true ]
		ifNone:[ ^ false ].
		
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountingBook class' category: 'Portfolio-Ejercicio'!
AccountingBook class
	instanceVariableNames: ''!

!AccountingBook class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/21/2022 14:38:09'!
errorDescriptionAccountingAlreadyIncluded
	self subclassResponsibility.! !

!AccountingBook class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/21/2022 14:46:58'!
errorDescriptionOnlyAccountingBooksCanBeAdded
	^ 'Only accounting books objects can be added'.! !


!classDefinition: #Portfolio category: 'Portfolio-Ejercicio'!
AccountingBook subclass: #Portfolio
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Portfolio methodsFor: 'testing' stamp: 'FM 5/23/2022 17:15:49'!
hasAccount: anAccount

	^ accountingEntries includes: anAccount.! !


!Portfolio methodsFor: 'accounting' stamp: 'FM 5/23/2022 17:48:43'!
add: anAccountingBook

	(anAccountingBook isKindOf: AccountingBook)
		ifFalse:[self error: self class errorDescriptionOnlyAccountingBooksCanBeAdded.].
		
	(self existIntersectionWith: anAccountingBook )
		ifTrue:[self error: self class errorDescriptionOverlappingAccountingBooks].
	
	(accountingEntries includes: anAccountingBook)
	
		ifFalse:[ 
			anAccountingBook addReferring: self.
			
			accountingEntries add: anAccountingBook.]
		
		ifTrue:[self error: self class errorDescriptionAccountingAlreadyIncluded.]! !

!Portfolio methodsFor: 'accounting' stamp: 'FM 5/23/2022 17:15:39'!
hasRegistered: anAccountingBook

	^  (accountingEntries includes: anAccountingBook ) 
	
	or:
	
	(accountingEntries anySatisfy: [:anAccountingEntry | anAccountingEntry hasRegistered: anAccountingBook]).! !

!Portfolio methodsFor: 'accounting' stamp: 'FM 5/23/2022 17:17:16'!
includes: anAccountingEntry

	^ (accountingEntries includes: anAccountingEntry) 
	
	or:
	
	(accountingEntries anySatisfy: [:anAccountingBook | anAccountingBook includes: anAccountingEntry]).! !

!Portfolio methodsFor: 'accounting' stamp: 'FM 5/23/2022 17:28:28'!
transactions

	| transactions |
	
	transactions := OrderedCollection new.
	
	accountingEntries do: [ :anAccountingEntry | (anAccountingEntry transactions) do: [ :aTransaction | transactions add: aTransaction] ].

	^ transactions.! !


!Portfolio methodsFor: 'intersection-private' stamp: 'FM 5/23/2022 17:23:45'!
isIncludedInTree: aRoot
	
	^ (aRoot includes: self) 
	
	or:
	
	(accountingEntries anySatisfy: [:anAccountingBook | anAccountingBook isIncludedInTree: aRoot]).! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: 'Portfolio-Ejercicio'!
Portfolio class
	instanceVariableNames: ''!

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/21/2022 12:18:54'!
errorDescriptionAccountAlreadyBelongsToPortfolio
	^ 'The account already belongs to the portfolio'.! !

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/21/2022 14:40:09'!
errorDescriptionAccountingAlreadyIncluded
	^ 'This portfolio is already included in the portfolio'.! !

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/23/2022 15:22:10'!
errorDescriptionOverlappingAccountingBooks
	^ 'The portfolio is overlapping and can not be added to avoid duplicated information inconsistency. '.! !

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/21/2022 12:31:22'!
errorDescriptionPortfolioAlreadyBelongsToPortfolio
	^ 'The portfolio already belongs to the portfolio'.! !

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/21/2022 01:14:39'!
errorDescriptionYouCanOnlyAddAccountsOrPortfoliosToPortfolio
	^ 'You can only add accounts or portfolios to a portfolio.'.! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Ejercicio'!
AccountingBook subclass: #ReceptiveAccount
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'transactions' stamp: 'FM 5/23/2022 17:24:13'!
add: aTransaction

	(aTransaction isKindOf: AccountTransaction)
	
		ifFalse:[self error: self class errorDescriptionOnlyAccountingBooksCanBeAdded.].
		
	(accountingEntries includes: aTransaction)
	
		ifFalse:[ 
			
			"aTransaction addReferring: self."
			
			accountingEntries add: aTransaction.]
		
		ifTrue:[self error: self class errorDescriptionAccountingAlreadyIncluded.]! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'FM 5/23/2022 17:17:06'!
hasRegistered: aTransaction

	^ accountingEntries includes: aTransaction.
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'FM 5/23/2022 16:57:54'!
includes: anAccountingEntry

	^ (accountingEntries includes: anAccountingEntry) .! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'FM 5/23/2022 17:28:51'!
register: aTransaction

	self add: aTransaction.
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'FM 5/23/2022 17:16:58'!
transactions 

	^ accountingEntries copy.! !


!ReceptiveAccount methodsFor: 'intersection-private' stamp: 'FM 5/23/2022 17:16:18'!
isIncludedInTree: aRoot

	^ (aRoot includes: self).! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ReceptiveAccount class' category: 'Portfolio-Ejercicio'!
ReceptiveAccount class
	instanceVariableNames: ''!

!ReceptiveAccount class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/21/2022 14:40:56'!
errorDescriptionAccountingAlreadyIncluded
	^ 'This account is already already included in the portfolio'.! !
