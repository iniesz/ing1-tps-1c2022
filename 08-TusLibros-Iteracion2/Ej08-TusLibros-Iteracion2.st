!classDefinition: #CartTest category: 'Ej08-TusLibros-Iteracion2'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testSetUp'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej08-TusLibros-Iteracion2'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:08'!
test01NewCartsAreCreatedEmpty

	self assert: self createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [ cart add: self itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 0 of: self itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 2 of: self itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test06CartRemembersAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self assert: (cart includes: self itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := self createCart.
	
	self deny: (cart includes: self itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: 2 of: self itemSellByTheStore.
	self assert: (cart occurrencesOf: self itemSellByTheStore) = 2! !

!CartTest methodsFor: 'tests' stamp: 'iniesz 6/6/2022 18:58:04'!
test09CanNotNonIntengerQuantity

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 1.1 of: self itemSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'iniesz 6/6/2022 19:46:48'!
test10CartReturnsACopyOfItems

	| cart items |
	cart := self createCart.
	cart add: self itemSellByTheStore.
	cart add: self itemSellByTheStore.
	items := cart items.
	items add: 'Not a book'.
	self assert: 2 equals: cart items size.
	
	! !


!CartTest methodsFor: 'support' stamp: 'iniesz 6/9/2022 14:51:42'!
createCart
	
	^ testSetUp createEmptyCart.! !

!CartTest methodsFor: 'support' stamp: 'iniesz 6/6/2022 19:25:59'!
defaultCatalog
	^ testSetUp defaultCatalog.! !

!CartTest methodsFor: 'support' stamp: 'iniesz 6/6/2022 19:26:15'!
itemNotSellByTheStore
	^ testSetUp itemNotSellByTheStore.! !

!CartTest methodsFor: 'support' stamp: 'iniesz 6/6/2022 19:26:28'!
itemSellByTheStore
	^ testSetUp itemSellByTheStore .! !


!CartTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 19:25:19'!
initialize
	testSetUp := TusLibrosTestSetUp new.! !


!classDefinition: #CashierTest category: 'Ej08-TusLibros-Iteracion2'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'testSetUp'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej08-TusLibros-Iteracion2'!

!CashierTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:51:42'!
createCart
	
	^ testSetUp createEmptyCart.! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:51:11'!
createEmptyCart
	^ testSetUp createEmptyCartNoCatalogue.! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 19:28:19'!
defaultCatalog
	^ testSetUp defaultCatalog.! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 19:27:18'!
initialize
	testSetUp := TusLibrosTestSetUp new.! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:51:11'!
test01CheckoutOfEmptyCartRaisesError
	| emptyCart cashier creditCard merchantProcessor |
	merchantProcessor := testSetUp createMerchantProcessorSimulator.
	emptyCart := testSetUp createEmptyCartNoCatalogue.
	
	cashier := Cashier new.
	creditCard := testSetUp createValidTestCreditCard.
	
	self should: [cashier checkout: emptyCart 
					with: creditCard
					to: merchantProcessor]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = cashier class errorDescriptionCheckoutOfEmptyCart.
			self assert: cashier sales isEmpty].! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:55:04'!
test02CheckoutCartWithOneItemRegistersSale
	| cashier cart sales creditCard merchantProcessor |
	merchantProcessor := testSetUp createMerchantProcessorSimulator.
	creditCard := testSetUp createValidTestCreditCard.
	cashier := Cashier with: self defaultCatalog.
	cart := testSetUp createCartWithOneItem.
	cashier checkout: cart with: creditCard to: merchantProcessor.
	sales := cashier sales.
	self assert: 1  equals: sales size.
	self assert: 9.99 equals:  (sales at: 1).! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:55:19'!
test03CheckoutCartWithMultipleItemsRegistersSale
	| cashier cart sales creditCard merchantProcessor |
	merchantProcessor := testSetUp createMerchantProcessorSimulator.
	creditCard := testSetUp createValidTestCreditCard.
	cashier := Cashier with: self defaultCatalog.
	cart := testSetUp createCartWithThreeItems.
	cashier checkout: cart with: creditCard to: merchantProcessor.
	sales := cashier sales.
	
	self assert: 1 equals: sales size.
	self assert: 15.0 equals:  (sales at: 1) value.! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:56:12'!
test04MultipleCheckoutsRegisterMultipleSales
	| cashier cart sales anotherCart creditCard merchantProcessor |
	merchantProcessor := testSetUp createMerchantProcessorSimulator.
	cashier := Cashier with: self defaultCatalog.
	creditCard := testSetUp  createValidTestCreditCard.
	cart := testSetUp createCartWithThreeItems.
	anotherCart := testSetUp createCartWithTwoItems.
	
	cashier checkout: cart with: creditCard to: merchantProcessor.
	cashier checkout: anotherCart with: creditCard to: merchantProcessor.
	sales := cashier sales.
	
	self assert: 2 equals: sales size.
	self assert: 15.0 equals:  (sales at: 1) value.
	self assert: 10.0 equals:  (sales at: 2) value.
	! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:56:22'!
test05CheckoutExpiredCreditCardRaisesError
	| cashier cart expiredCreditCard merchantProcessor |
	merchantProcessor := testSetUp createMerchantProcessorSimulator.
	cashier := Cashier with: self defaultCatalog.
	expiredCreditCard := testSetUp  createExpiredTestCreditCard .
	cart := testSetUp createCartWithThreeItems.
	
	self should: [cashier checkout: cart 
			with: expiredCreditCard 
			to: merchantProcessor.]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: cashier class errorDescriptionTheCreditCardHasExpired equals: anError messageText.
			self assert: cashier sales isEmpty].! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:56:30'!
test06CheckoutOutOfFundsCreditCardRaisesError
	| cashier cart outOfFundsCreditCard merchantProcessor |
	merchantProcessor := testSetUp createMerchantProcessorSimulator.
	cashier := Cashier with: self defaultCatalog.
	outOfFundsCreditCard := testSetUp createOutOfFundsTestCreditCard .
	cart := testSetUp createCartWithOneItem.
	
	self should: [cashier checkout: cart 
					with: outOfFundsCreditCard 
					to: merchantProcessor.]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: cashier class errorDescriptionTheCreditCardHasNoFunds equals: anError messageText.
			self assert: cashier sales isEmpty].! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:56:54'!
test07CheckoutNotEnoughFundsCreditCardRaisesError
	| cashier cart outOfFundsCreditCard merchantProcessor |
	merchantProcessor := testSetUp createMerchantProcessorSimulator.
	cashier := Cashier with: self defaultCatalog.
	outOfFundsCreditCard := testSetUp createOutOfFundsTestCreditCard .
	cart := testSetUp createCartWithTwoItems.
	cart add: testSetUp book3SellByTheStore.  
	cart add: testSetUp book3SellByTheStore.  
		
	self should: [cashier checkout: cart 
					with: outOfFundsCreditCard 
					to: merchantProcessor.]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: cashier class errorDescriptionTheCreditCardHasNoFunds equals: anError messageText.
			self assert: cashier sales isEmpty].! !


!classDefinition: #CreditCardTest category: 'Ej08-TusLibros-Iteracion2'!
TestCase subclass: #CreditCardTest
	instanceVariableNames: 'testSetUp'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej08-TusLibros-Iteracion2'!

!CreditCardTest methodsFor: 'support' stamp: 'FM 6/8/2022 23:50:33'!
validCreditCardNumber

	^ '8050012398438763' .! !

!CreditCardTest methodsFor: 'support' stamp: 'FM 6/8/2022 23:52:00'!
validExpirationMonth

	^ '01' .! !

!CreditCardTest methodsFor: 'support' stamp: 'FM 6/8/2022 23:51:55'!
validExpirationYear

	^ '2022' .! !

!CreditCardTest methodsFor: 'support' stamp: 'FM 6/8/2022 23:57:15'!
validOwnerName

	^ 'Pedro Sanchez' .! !


!CreditCardTest methodsFor: 'tests' stamp: 'iniesz 6/9/2022 14:31:50'!
test01CanNotCreateCreditCartWithInvalidNumber

	self 
		should: [ 
			CreditCard number: '12345' 
			expirationYear: '' 
			expirationMonth: '' 
			owner: ''. 
		]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidNumberException. ]! !

!CreditCardTest methodsFor: 'tests' stamp: 'iniesz 6/9/2022 14:31:50'!
test02CanNotCreateCreditCartWithInvalidExpirationYear

	self 
		should: [ 
			CreditCard number: self validCreditCardNumber 
			expirationYear: '20223' 
			expirationMonth: '' 
			owner: ''. 
		]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidExpirationYearException. ]! !

!CreditCardTest methodsFor: 'tests' stamp: 'iniesz 6/9/2022 14:31:50'!
test03CanNotCreateCreditCartWithInvalidExpirationMonth

	self 
		should: [ 
			CreditCard number: self validCreditCardNumber 
			expirationYear: self validExpirationYear 
			expirationMonth: '012' 
			owner: ''. 
		]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidExpirationMonthException. ]! !

!CreditCardTest methodsFor: 'tests' stamp: 'iniesz 6/9/2022 14:31:50'!
test04CanNotCreateCreditCartWithInvalidExpirationDate

	self 
		should: [ 
			CreditCard number: self validCreditCardNumber 
			expirationYear: self validExpirationYear 
			expirationMonth: '13' 
			owner: ''. 
		]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidExpirationDateException. ]! !

!CreditCardTest methodsFor: 'tests' stamp: 'iniesz 6/9/2022 14:31:50'!
test05CanNotCreateCreditCartWithEmptyOwnerName
	
	self 
		should: [ 
			CreditCard number: self validCreditCardNumber 
			expirationYear: self validExpirationYear 
			expirationMonth: self validExpirationMonth 
			owner: ''. 
		]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidOwnerNameException. ]! !

!CreditCardTest methodsFor: 'tests' stamp: 'iniesz 6/9/2022 14:31:50'!
test06CanNotCreateCreditCartWithANameLongerThan30Digits

	self 
		should: [ 
			CreditCard number: self validCreditCardNumber 
			expirationYear: self validExpirationYear 
			expirationMonth: self validExpirationMonth  
			owner: '0123456789012345678901234567891'. 
		]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidOwnerNameException. ]! !

!CreditCardTest methodsFor: 'tests' stamp: 'iniesz 6/9/2022 14:31:50'!
test07CanCreateCreditCartWithValidArguments

	| aCreditCart |
	
	aCreditCart := CreditCard number: self validCreditCardNumber 
		expirationYear: self validExpirationYear 
		expirationMonth: self validExpirationMonth 
		owner: self validOwnerName.
	
	self assert: aCreditCart number equals: self validCreditCardNumber.
	self assert: aCreditCart expirationYear equals: self validExpirationYear.
	self assert: aCreditCart expirationMonth equals: self validExpirationMonth.
	self assert: aCreditCart owner equals: self validOwnerName.! !


!classDefinition: #Cart category: 'Ej08-TusLibros-Iteracion2'!
Object subclass: #Cart
	instanceVariableNames: 'catalogue items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej08-TusLibros-Iteracion2'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'items-assertions' stamp: 'iniesz 6/9/2022 14:36:51'!
assertIsValidItem: anItem

	(catalogue includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'items-assertions' stamp: 'iniesz 6/6/2022 18:53:26'!
assertIsValidQuantity: aQuantity

	(aQuantity strictlyPositive and: aQuantity isInteger) ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'iniesz 6/9/2022 14:36:51'!
initializeAcceptingItemsOf: aCatalog

	catalogue := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'iniesz 6/6/2022 19:42:00'!
items
	^ items copy.! !

!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !

!Cart methodsFor: 'queries' stamp: 'iniesz 6/9/2022 14:36:51'!
totalPrice
	^ items sum:[:item | catalogue at: item].! !

!Cart methodsFor: 'queries' stamp: 'iniesz 6/9/2022 10:08:14'!
withItemsCollect: aBlock
	^ items collect:aBlock.! !


!Cart methodsFor: 'testing' stamp: 'iniesz 6/6/2022 19:34:23'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'iniesz 6/6/2022 18:58:13'!
add: aQuantity of: anItem
	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'Ej08-TusLibros-Iteracion2'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'Ej08-TusLibros-Iteracion2'!
Object subclass: #Cashier
	instanceVariableNames: 'sales catalogue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej08-TusLibros-Iteracion2'!

!Cashier methodsFor: 'checkout-asserts' stamp: 'iniesz 6/9/2022 11:03:37'!
assertCartIsNotEmpty: aCart
	(aCart isEmpty)
		ifTrue:[self error: (self class errorDescriptionCheckoutOfEmptyCart )].! !

!Cashier methodsFor: 'checkout-asserts' stamp: 'iniesz 6/9/2022 11:02:36'!
assertCreditCardHasNotExpired: aCreditCard
	(aCreditCard hasExpiredOn: Date today)
		ifTrue:[self error: self class errorDescriptionTheCreditCardHasExpired].! !


!Cashier methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:37:33'!
checkout: aCart with: aCreditCard to: aMerchantProcessor
	| saleTotal itemsPrices |
	
	self assertCartIsNotEmpty: aCart.
	self assertCreditCardHasNotExpired: aCreditCard.
	
	"itemsPrices := (aCart withItemsCollect:[:item | self price: item]).
	saleTotal := itemsPrices sum."
	saleTotal := aCart totalPrice.
	
	"self assertCreditCardHasFunds: aCreditCard."
	[aMerchantProcessor debit: saleTotal from: aCreditCard.]
	on: Error
	do:[ self error: self class errorDescriptionTheCreditCardHasNoFunds ].
	
	sales add: saleTotal.! !

!Cashier methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 19:12:26'!
initialize
	sales := OrderedCollection new.! !

!Cashier methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:38:25'!
initializeWith: aCatalogue
	sales := OrderedCollection new.
	catalogue := aCatalogue.! !

!Cashier methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 19:53:09'!
price: anItem
	^ catalogue at: anItem.! !

!Cashier methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 19:06:27'!
sales
	^ sales! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'Ej08-TusLibros-Iteracion2'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 19:07:28'!
errorDescriptionCheckoutOfEmptyCart
	^ 'Invalid operation: Checkout over empty Cart.'! !

!Cashier class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 11:01:33'!
errorDescriptionTheCreditCardHasExpired
	^ 'Invalid operation: The credit card has already expired.'! !

!Cashier class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 11:29:38'!
errorDescriptionTheCreditCardHasNoFunds
	^ 'Invalid operation: The credit card has no available funds for the puchase.'! !

!Cashier class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 19:52:18'!
with: aCatalogue
	^ self new initializeWith: aCatalogue; yourself.! !


!classDefinition: #CreditCard category: 'Ej08-TusLibros-Iteracion2'!
Object subclass: #CreditCard
	instanceVariableNames: 'number expirationYear expirationMonth owner'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej08-TusLibros-Iteracion2'!

!CreditCard methodsFor: 'testing' stamp: 'FM 6/8/2022 23:26:05'!
expirationMonth

	^ expirationMonth.! !

!CreditCard methodsFor: 'testing' stamp: 'FM 6/8/2022 23:25:55'!
expirationYear

	^ expirationYear.! !

!CreditCard methodsFor: 'testing' stamp: 'FM 6/8/2022 22:40:43'!
number

	^ number.! !

!CreditCard methodsFor: 'testing' stamp: 'FM 6/8/2022 23:38:25'!
owner

	^ owner.! !


!CreditCard methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 11:08:41'!
hasExpiredOn: aDate

	(expirationYear asNumber < aDate yearNumber)
		ifTrue:[^ true].
		
	(expirationYear asNumber = aDate yearNumber
	and: (expirationMonth asNumber < aDate monthIndex))
		ifTrue:[^true].
		
	^ false.
	
							! !

!CreditCard methodsFor: 'as yet unclassified' stamp: 'FM 6/8/2022 23:41:39'!
initializeWithNumber: aCreditCardNumber expirationYear: aYear expirationMonth: aMonth owner: aOwnerName

	number := aCreditCardNumber.
	expirationYear := aYear.
	expirationMonth := aMonth.
	owner := aOwnerName.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'Ej08-TusLibros-Iteracion2'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'exceptions' stamp: 'FM 6/8/2022 23:09:49'!
invalidExpirationDateException

	^ 'The expiration date must have an valid year and month'! !

!CreditCard class methodsFor: 'exceptions' stamp: 'FM 6/8/2022 23:08:56'!
invalidExpirationMonthException

	^ 'The expiration month must be 2 digits'! !

!CreditCard class methodsFor: 'exceptions' stamp: 'FM 6/8/2022 23:02:13'!
invalidExpirationYearException

	^ 'The expiration year must be 4 digits'! !

!CreditCard class methodsFor: 'exceptions' stamp: 'FM 6/8/2022 22:29:31'!
invalidNumberException

	^ 'The card number must be 16 digits'! !

!CreditCard class methodsFor: 'exceptions' stamp: 'FM 6/8/2022 23:30:53'!
invalidOwnerNameException

	^ 'The owner name must not be empty and must not have more than 30 digits'! !


!CreditCard class methodsFor: 'asserts' stamp: 'FM 6/8/2022 23:42:38'!
assertIsValidCreditCardNumber: aCreditCardNumber
	
	(aCreditCardNumber isString and: aCreditCardNumber size == 16) ifFalse: [ self error: self invalidNumberException ].
! !

!CreditCard class methodsFor: 'asserts' stamp: 'FM 6/8/2022 23:45:31'!
assertIsValidExpirationDateWithYear: aYear andMonth: aMonth
	
	[ Date year: aYear asNumber month: aMonth asNumber day: 01 ] on: Error do: [ self error: self invalidExpirationDateException ].! !

!CreditCard class methodsFor: 'asserts' stamp: 'FM 6/8/2022 23:43:14'!
assertIsValidExpirationMonth: aMonth.
			
	(aMonth isString and: aMonth size == 2) ifFalse: [ self error: self invalidExpirationMonthException ].! !

!CreditCard class methodsFor: 'asserts' stamp: 'FM 6/8/2022 23:43:04'!
assertIsValidExpirationYear: aYear
	
	(aYear isString and: aYear size == 4) ifFalse: [ self error: self invalidExpirationYearException ].! !

!CreditCard class methodsFor: 'asserts' stamp: 'FM 6/8/2022 23:45:51'!
assertIsValidExpirationYear: aYear andMonth: aMonth.
	
	self assertIsValidExpirationYear: aYear.
	self assertIsValidExpirationMonth: aMonth.
	self assertIsValidExpirationDateWithYear: aYear andMonth: aMonth.! !

!CreditCard class methodsFor: 'asserts' stamp: 'FM 6/8/2022 23:34:02'!
assertIsValidOwnerName: aName
	
	(aName isString and: aName withoutSeparators isEmpty not and: aName size <= 30) ifFalse: [ self error: self invalidOwnerNameException ].! !


!CreditCard class methodsFor: 'instance creation' stamp: 'FM 6/8/2022 23:41:39'!
number: aCreditCardNumber expirationYear: aYear expirationMonth: aMonth owner: aName

	self assertIsValidCreditCardNumber: aCreditCardNumber.
	self assertIsValidExpirationYear: aYear andMonth: aMonth.
	self assertIsValidOwnerName: aName.
	
	^ self new initializeWithNumber: aCreditCardNumber expirationYear: aYear expirationMonth: aMonth owner: aName.! !


!classDefinition: #MerchantProcessorSimulator category: 'Ej08-TusLibros-Iteracion2'!
Object subclass: #MerchantProcessorSimulator
	instanceVariableNames: 'accountsBalance'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej08-TusLibros-Iteracion2'!

!MerchantProcessorSimulator methodsFor: 'payment' stamp: 'iniesz 6/9/2022 14:11:20'!
debit: anAmount from: aCreditCard
	
	self assertCreditCardHasNotExpired: aCreditCard.
	self assertCreditCard: aCreditCard haveAtLeast: anAmount.! !


!MerchantProcessorSimulator methodsFor: 'initialization' stamp: 'iniesz 6/9/2022 14:03:22'!
initializeWith: anAccountsBalanceDictionary
	accountsBalance := anAccountsBalanceDictionary.! !


!MerchantProcessorSimulator methodsFor: 'payment-asserts' stamp: 'iniesz 6/9/2022 14:09:30'!
assertCreditCard: aCreditCard haveAtLeast: anAmount
	
	| funds |
	funds := accountsBalance at: aCreditCard number.
	
	(anAmount <= funds)
		ifFalse:[self error: self class errorDescriptionTheCreditCardHaveNotEnoughFunds].
		
	
! !

!MerchantProcessorSimulator methodsFor: 'payment-asserts' stamp: 'iniesz 6/9/2022 11:20:09'!
assertCreditCardHasNotExpired: aCreditCard
	(aCreditCard hasExpiredOn: Date today)
		ifTrue:[self error: self class errorDescriptionTheCreditCardHasExpired].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MerchantProcessorSimulator class' category: 'Ej08-TusLibros-Iteracion2'!
MerchantProcessorSimulator class
	instanceVariableNames: ''!

!MerchantProcessorSimulator class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 11:20:40'!
errorDescriptionTheCreditCardHasExpired
	^ 'Invalid operation: The credit card has already expired'.! !

!MerchantProcessorSimulator class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:10:16'!
errorDescriptionTheCreditCardHaveNotEnoughFunds
	^ 'Invalid operation: The credit card have not enough funds.'.! !

!MerchantProcessorSimulator class methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:03:39'!
with: anAccountsBalanceDictionary
	^ self new initializeWith: anAccountsBalanceDictionary; yourself.! !


!classDefinition: #TusLibrosTestSetUp category: 'Ej08-TusLibros-Iteracion2'!
Object subclass: #TusLibrosTestSetUp
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej08-TusLibros-Iteracion2'!

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 10:02:59'!
book1SellByTheStore
	^ self itemSellByTheStore.! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 10:03:13'!
book2SellByTheStore
	^ 'validBook2'.! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 10:03:17'!
book3SellByTheStore
	^ 'validBook3'.! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:53:50'!
createCartWithOneItem
	
	| cart |
	cart := self createEmptyCart.
	
	cart add: self book1SellByTheStore.
	^ cart.
	
	! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:54:30'!
createCartWithThreeItems
	
	| cart |
	cart := self createCartWithTwoItems.
	
	cart add: self book3SellByTheStore.
	
	^ cart.
	
	! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:54:14'!
createCartWithTwoItems
	
	| cart |
	cart := self createCartWithOneItem.
	
	cart add: self book2SellByTheStore.
	
	^ cart.
	
	! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:51:41'!
createEmptyCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:51:11'!
createEmptyCartNoCatalogue
	
	^Cart acceptingItemsOf: Dictionary new.! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:31:50'!
createExpiredTestCreditCard

	| monthString today yearString |
	today := Date today.

	monthString := today monthIndex asString.
	(monthString size = 1) ifTrue:[monthString := '0',monthString].
	
	yearString := 	(today yearNumber - 1) asString.

	^ CreditCard number:  '8050012398438762'
			expirationYear:  yearString
			expirationMonth: monthString 
			owner: 'Maximiliano Sanchez'.! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:27:20'!
createMerchantProcessorSimulator
	| accountsBalance validTestCreditCard outOfFundsCreditCard |
	accountsBalance := Dictionary new.
	
	validTestCreditCard := self createValidTestCreditCard.
	outOfFundsCreditCard := self createOutOfFundsTestCreditCard.
	
	accountsBalance at: validTestCreditCard number put: 15.0.
	accountsBalance at: outOfFundsCreditCard number put: 0.0.	
	
	^ MerchantProcessorSimulator with: accountsBalance.
	! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:31:50'!
createOutOfFundsTestCreditCard

	| monthString today yearString |
	today := Date today.

	monthString := today monthIndex asString.
	(monthString size = 1) ifTrue:[monthString := '0',monthString].
	
	yearString := 	today yearNumber asString.

	^ CreditCard number:  '8050012398438761'
			expirationYear:  yearString
			expirationMonth: monthString 
			owner: 'Perez Gonzalez'.! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/9/2022 14:31:50'!
createValidTestCreditCard

	| monthString today yearString |
	today := Date today.

	monthString := today monthIndex asString.
	(monthString size = 1) ifTrue:[monthString := '0',monthString].
	
	yearString := 	today yearNumber asString.

	^ CreditCard number:  '8050012398438763'
			expirationYear:  yearString
			expirationMonth: monthString 
			owner: 'Juan Perez'.! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 21:27:32'!
defaultCatalog
	
	"^ Array with: self itemSellByTheStore"
	| catalog |
	catalog := Dictionary new.
	
	catalog at: self itemSellByTheStore put: 9.99.
	
	catalog at:'validBook2' put: 0.01.
	catalog at:'validBook3' put: 5.00.
	
	^ catalog.
	! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 19:23:56'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!TusLibrosTestSetUp methodsFor: 'as yet unclassified' stamp: 'iniesz 6/6/2022 19:24:05'!
itemSellByTheStore
	
	^ 'validBook'! !
