!classDefinition: #I category: 'Numeros-Romanos'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Romanos'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'Numeros-Romanos'!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'next & previous' stamp: 'iniesz 4/11/2022 15:25:11'!
hasPrevious
	^false.! !

!I class methodsFor: 'next & previous' stamp: 'iniesz 4/11/2022 15:42:48'!
next
	^II! !

!I class methodsFor: 'next & previous' stamp: 'iniesz 4/11/2022 15:42:42'!
previous
	^ Zero.! !


!I class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 13:31:40'!
* aNumber
	^ aNumber.! !

!I class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 13:41:38'!
+ aNatural
	^aNatural next! !

!I class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 15:51:37'!
- aNumber
	^ self previous - aNumber previous.! !

!I class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 15:11:45'!
/ aNumber
	^ aNumber divideTo: self.! !

!I class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 14:59:09'!
<= aNumber
	^ true.! !

!I class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 14:05:38'!
divideTo: aNumber
	^ aNumber.! !

!I class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 13:56:16'!
substractTo: aNumber
	^ aNumber previous.! !

!I class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 15:21:29'!
validatedDivideTo: aNumber
	^ self divideTo: aNumber.! !


!I class methodsFor: 'error-descriptions' stamp: 'iniesz 4/11/2022 13:58:30'!
canNotSubstractToOneErrorDescription
	^ 'No se puede restar por debajo de I en el sistema de numeros romanos'.! !

!I class methodsFor: 'error-descriptions' stamp: 'iniesz 4/11/2022 15:50:09'!
negativeNumbersNotSupportedErrorDescription
	^ 'Negative numbers not supported.'.! !


!classDefinition: #Zero category: 'Numeros-Romanos'!
I subclass: #Zero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Romanos'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Zero class' category: 'Numeros-Romanos'!
Zero class
	instanceVariableNames: ''!

!Zero class methodsFor: 'as yet unclassified' stamp: 'iniesz 4/11/2022 15:39:48'!
* aNumber
	^ Zero.! !

!Zero class methodsFor: 'as yet unclassified' stamp: 'iniesz 4/11/2022 15:40:00'!
+ aNumber
	^ aNumber.! !

!Zero class methodsFor: 'as yet unclassified' stamp: 'iniesz 4/11/2022 15:52:30'!
- aNumber
	(aNumber = Zero)
		ifTrue:[^Zero].
		
	^ self error: 'No se representan los numeros negativos.'! !

!Zero class methodsFor: 'as yet unclassified' stamp: 'iniesz 4/11/2022 15:41:17'!
/ aNumber
	^ Zero.! !

!Zero class methodsFor: 'as yet unclassified' stamp: 'iniesz 4/11/2022 15:54:26'!
<= aNumber
	^true.! !

!Zero class methodsFor: 'as yet unclassified' stamp: 'iniesz 4/11/2022 15:54:41'!
hasPrevious
	^false.! !

!Zero class methodsFor: 'as yet unclassified' stamp: 'iniesz 4/11/2022 15:44:00'!
next
	^ I.! !

!Zero class methodsFor: 'as yet unclassified' stamp: 'iniesz 4/11/2022 15:43:51'!
previous
	^ self error: 'Zero does not have a previous number'.! !


!classDefinition: #II category: 'Numeros-Romanos'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Romanos'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'Numeros-Romanos'!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 13:30:47'!
* aNumber
	^ previous * aNumber + aNumber.! !

!II class methodsFor: 'operations' stamp: 'ARM 3/31/2022 21:27:43'!
+ unNatural

	^previous + unNatural next! !

!II class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 15:09:28'!
- unNatural
	^ unNatural substractTo: self.! !

!II class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 15:46:14'!
/ aNumber
	^ aNumber intDivideTo: self.
! !

!II class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 15:26:17'!
<= aNumber
	(aNumber hasPrevious) 
		ifFalse: [^false.].
	
	^self previous <= aNumber previous.! !

!II class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 15:46:14'!
intDivideTo: aNumber
	(self = aNumber) 
		ifTrue: [^ I].
	(self <= aNumber) 
		ifTrue: [^ (self intDivideTo: (aNumber - self) ) + I.].
	^ Zero.! !

!II class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 13:53:38'!
substractTo: aNumber
	^ (aNumber previous) - previous.! !

!II class methodsFor: 'operations' stamp: 'iniesz 4/11/2022 15:46:14'!
validatedDivideTo: aNumber
	(self <= aNumber) 
		ifTrue: [aNumber intDivideTo: self].
	^ self error: 'Error div'.! !


!II class methodsFor: 'next & previous' stamp: 'iniesz 4/11/2022 15:24:58'!
hasPrevious
	^true.! !

!II class methodsFor: 'next & previous' stamp: 'MB 4/4/2022 21:41:37'!
nameOfNext

	"999 a 1000"
	(self name endsWith: 'CMXCIX') ifTrue: [^(self name withoutSuffix: 'CMXCIX'), 'M'].
	"899 a 900"
	(self name endsWith: 'DCCCXCIX') ifTrue: [^(self name withoutSuffix: 'DCCCXCIX'), 'CM'].
	"499 a 500"
	(self name endsWith: 'CDXCIX') ifTrue: [^(self name withoutSuffix: 'CDXCIX'), 'D'].
	"399 a 400"
	(self name endsWith: 'CCCXCIX') ifTrue: [^(self name withoutSuffix: 'CCCXCIX'), 'CD'].
	"99 a 100"
	(self name endsWith: 'XCIX') ifTrue: [^(self name withoutSuffix: 'XCIX'), 'C'].
	"89 a 90"
	(self name endsWith: 'LXXXIX') ifTrue: [^(self name withoutSuffix: 'LXXXIX'), 'XC'].
	"49 a 50"
	(self name endsWith: 'XLIX') ifTrue: [^(self name withoutSuffix: 'XLIX'), 'L'].
	"39 a 40"
	(self name endsWith: 'XXXIX') ifTrue: [^(self name withoutSuffix: 'XXXIX'), 'XL'].
	"9 a 10"
	(self name endsWith: 'IX') ifTrue: [^(self name withoutSuffix: 'IX'), 'X'].
	"8 a 9"
	(self name endsWith: 'VIII') ifTrue: [^(self name withoutSuffix: 'VIII'), 'IX'].
	"4 a 5"
	(self name endsWith: 'IV') ifTrue: [^(self name withoutSuffix: 'IV'), 'V'].
	"3 a 4"
	(self name endsWith: 'III') ifTrue: [^(self name withoutSuffix: 'III'), 'IV'].
	
	^self name, 'I'.! !

!II class methodsFor: 'next & previous' stamp: 'MB 4/4/2022 21:00:41'!
next
	next ifNotNil:[^next].
	next _ II createChildNamed: self nameOfNext.
	next previous: self.
	^next! !

!II class methodsFor: 'next & previous' stamp: 'ARM 3/31/2022 21:26:45'!
previous

	^previous! !

!II class methodsFor: 'next & previous' stamp: 'ARM 3/31/2022 21:20:55'!
previous: unNatural 
	
	previous _ unNatural! !


!II class methodsFor: 'cleanup' stamp: 'MB 4/4/2022 21:24:49'!
removeAllNext

	next ifNotNil: 
	[
		next removeAllNext.
		next removeFromSystem.
		next := nil.
	]! !


!II class methodsFor: '--** private fileout/in **--' stamp: 'iniesz 4/11/2022 15:56:06'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	previous := I.! !


!classDefinition: #III category: 'Numeros-Romanos'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Romanos'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: 'Numeros-Romanos'!
III class
	instanceVariableNames: ''!

!III class methodsFor: '--** private fileout/in **--' stamp: 'iniesz 4/11/2022 15:56:07'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IV.
	previous := II.! !


!classDefinition: #IV category: 'Numeros-Romanos'!
II subclass: #IV
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Romanos'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IV class' category: 'Numeros-Romanos'!
IV class
	instanceVariableNames: ''!

!IV class methodsFor: '--** private fileout/in **--' stamp: 'iniesz 4/11/2022 15:56:07'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := V.
	previous := III.! !


!classDefinition: #IX category: 'Numeros-Romanos'!
II subclass: #IX
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Romanos'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IX class' category: 'Numeros-Romanos'!
IX class
	instanceVariableNames: ''!

!IX class methodsFor: '--** private fileout/in **--' stamp: 'iniesz 4/11/2022 15:56:07'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := X.
	previous := VIII.! !


!classDefinition: #V category: 'Numeros-Romanos'!
II subclass: #V
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Romanos'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'V class' category: 'Numeros-Romanos'!
V class
	instanceVariableNames: ''!

!V class methodsFor: '--** private fileout/in **--' stamp: 'iniesz 4/11/2022 15:56:07'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := VI.
	previous := IV.! !


!classDefinition: #VI category: 'Numeros-Romanos'!
II subclass: #VI
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Romanos'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'VI class' category: 'Numeros-Romanos'!
VI class
	instanceVariableNames: ''!

!VI class methodsFor: '--** private fileout/in **--' stamp: 'iniesz 4/11/2022 15:56:07'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := VII.
	previous := V.! !


!classDefinition: #VII category: 'Numeros-Romanos'!
II subclass: #VII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Romanos'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'VII class' category: 'Numeros-Romanos'!
VII class
	instanceVariableNames: ''!

!VII class methodsFor: '--** private fileout/in **--' stamp: 'iniesz 4/11/2022 15:56:07'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := VIII.
	previous := VI.! !


!classDefinition: #VIII category: 'Numeros-Romanos'!
II subclass: #VIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Romanos'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'VIII class' category: 'Numeros-Romanos'!
VIII class
	instanceVariableNames: ''!

!VIII class methodsFor: '--** private fileout/in **--' stamp: 'iniesz 4/11/2022 15:56:07'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IX.
	previous := VII.! !


!classDefinition: #X category: 'Numeros-Romanos'!
II subclass: #X
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Romanos'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'X class' category: 'Numeros-Romanos'!
X class
	instanceVariableNames: ''!

!X class methodsFor: '--** private fileout/in **--' stamp: 'iniesz 4/11/2022 15:56:07'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	previous := IX.! !

II initializeAfterFileIn!
III initializeAfterFileIn!
IV initializeAfterFileIn!
IX initializeAfterFileIn!
V initializeAfterFileIn!
VI initializeAfterFileIn!
VII initializeAfterFileIn!
VIII initializeAfterFileIn!
X initializeAfterFileIn!