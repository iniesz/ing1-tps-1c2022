!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'iniesz 4/27/2022 17:51:45'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'iniesz 4/27/2022 17:51:49'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'iniesz 4/27/2022 17:52:04'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'iniesz 4/27/2022 17:52:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'iniesz 4/27/2022 17:52:13'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'iniesz 4/27/2022 17:52:17'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'iniesz 4/27/2022 17:52:21'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'iniesz 4/27/2022 17:52:24'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'iniesz 4/27/2022 17:52:28'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'iniesz 4/27/2022 17:52:31'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'currentPosition size'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'initialization' stamp: 'iniesz 4/27/2022 17:54:14'!
initialize
	size := 0.
	self newCurrentPosition: OOStackBase new.! !


!OOStack methodsFor: 'private' stamp: 'iniesz 4/26/2022 17:45:19'!
currentPosition
	^ currentPosition.! !

!OOStack methodsFor: 'private' stamp: 'iniesz 4/27/2022 16:53:31'!
newCurrentPosition: newCurrentPosition
	currentPosition := newCurrentPosition .! !


!OOStack methodsFor: 'operations' stamp: 'iniesz 4/27/2022 17:53:40'!
isEmpty
	^ currentPosition isEmpty.! !

!OOStack methodsFor: 'operations' stamp: 'iniesz 4/27/2022 17:44:25'!
pop
	size := size-1.
	^ currentPosition pop.! !

!OOStack methodsFor: 'operations' stamp: 'iniesz 4/27/2022 17:44:16'!
push: anElement
	currentPosition:= OOStackTopElement overStack: self pushElement: anElement.
	size := size+1.! !

!OOStack methodsFor: 'operations' stamp: 'iniesz 4/27/2022 17:44:05'!
size 
	^ size.! !

!OOStack methodsFor: 'operations' stamp: 'iniesz 4/27/2022 16:34:54'!
top
	^ currentPosition top.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: 'currentPosition'!

!OOStack class methodsFor: 'error descriptions' stamp: 'iniesz 4/27/2022 17:45:02'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #OOStackPosition category: 'Stack-Exercise'!
Object subclass: #OOStackPosition
	instanceVariableNames: 'ownerStack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackPosition methodsFor: 'operations' stamp: 'iniesz 4/27/2022 17:48:33'!
isEmpty
	^ self subclassResponsibility.! !

!OOStackPosition methodsFor: 'operations' stamp: 'iniesz 4/27/2022 17:48:08'!
push: anElement
	self subclassResponsibility.! !

!OOStackPosition methodsFor: 'operations' stamp: 'iniesz 4/27/2022 17:47:57'!
top
	self subclassResponsibility.! !


!OOStackPosition methodsFor: 'initialization' stamp: 'iniesz 4/26/2022 16:14:23'!
initializeForStack: anOwnerStack
	ownerStack := anOwnerStack.
	! !


!classDefinition: #OOStackBase category: 'Stack-Exercise'!
OOStackPosition subclass: #OOStackBase
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackBase methodsFor: 'as yet unclassified' stamp: 'iniesz 4/25/2022 20:40:15'!
isEmpty
	^ true.! !

!OOStackBase methodsFor: 'as yet unclassified' stamp: 'iniesz 4/27/2022 17:55:12'!
pop
	^ self error: OOStack stackEmptyErrorDescription.! !

!OOStackBase methodsFor: 'as yet unclassified' stamp: 'iniesz 4/27/2022 17:55:19'!
top
	^ self error: OOStack stackEmptyErrorDescription.! !


!classDefinition: #OOStackTopElement category: 'Stack-Exercise'!
OOStackPosition subclass: #OOStackTopElement
	instanceVariableNames: 'element previousPosition'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTopElement methodsFor: 'initialization' stamp: 'iniesz 4/27/2022 16:57:07'!
initializeOverStack: anOwnerStack withElement: anElement
	self initializeForStack: anOwnerStack.
	element := anElement.
	previousPosition := ownerStack currentPosition.
	ownerStack newCurrentPosition: self.! !


!OOStackTopElement methodsFor: 'operations' stamp: 'iniesz 4/26/2022 15:37:37'!
isEmpty
	^ false.! !

!OOStackTopElement methodsFor: 'operations' stamp: 'iniesz 4/27/2022 16:56:19'!
pop
	|stackCurrentPosition|
	stackCurrentPosition := ownerStack currentPosition.
	
	ownerStack newCurrentPosition: stackCurrentPosition previousPosition.
	
	^ stackCurrentPosition top.! !

!OOStackTopElement methodsFor: 'operations' stamp: 'iniesz 4/26/2022 16:08:16'!
top
	^ element.! !


!OOStackTopElement methodsFor: 'private' stamp: 'iniesz 4/27/2022 16:54:42'!
previousPosition
	^ previousPosition.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStackTopElement class' category: 'Stack-Exercise'!
OOStackTopElement class
	instanceVariableNames: ''!

!OOStackTopElement class methodsFor: 'instance creation' stamp: 'iniesz 4/27/2022 16:59:40'!
overStack: anStack pushElement: anElement
	^ self new initializeOverStack: anStack  withElement: anElement.! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!
