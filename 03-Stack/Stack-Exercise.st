!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'FM 4/26/2022 22:12:38'!
test01StackShouldBeEmptyWhenCreated
	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty.! !

!OOStackTest methodsFor: 'test' stamp: 'FM 4/26/2022 22:28:50'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'FM 4/26/2022 22:29:54'!
test03PopRemovesElementsFromTheStack
	| stack |
	
	stack := OOStack new.
	
	stack push: 'something'.
	
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'FM 4/26/2022 22:31:11'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'FM 4/26/2022 22:31:41'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	
	firstPushedObject := 'firstSomething'.
	
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	
	self assert: stack pop = firstPushedObject.
	
	self assert: stack isEmpty.! !

!OOStackTest methodsFor: 'test' stamp: 'FM 4/26/2022 22:31:56'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'FM 4/26/2022 22:32:59'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	
	stack top.
	
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'FM 4/26/2022 22:34:49'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
! !

!OOStackTest methodsFor: 'test' stamp: 'FM 4/26/2022 22:35:07'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	
	stack push: 'something'.
	
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
! !

!OOStackTest methodsFor: 'test' stamp: 'FM 4/26/2022 22:35:22'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: 'helloWorldSentence emptySentence aloaloSentence rosesSentence emptySentences oneEmptySentenc oneEmptySentence oneNonEmptySentence wavesSentences multipleSentences rosesLowerSentence beginningSentence armadilloSentence allSentencesStartWithUpperA twoSentencesUpperLOneSentenceLowerL sentencesUpperL sentencesLowerL rosesSentence2 multipleSentences2'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'iniesz 4/28/2022 05:06:58'!
setUp
	emptySentence := ''.
	helloWorldSentence := 'Hello World!!!!'.
	wavesSentences :='Las olas y el viento.'.
	aloaloSentence := 'Alo alo'.
	rosesSentence := 'Las rosas son rojas, las violetas azules'.
	rosesLowerSentence := 'las rosas son rojas, las violetas azules'.
	beginningSentence := 'A new beginning'.
	armadilloSentence := 'Armadillo una subespecie de mamifero...'.
	rosesSentence2 := 'Las rosas son flores.'.
	
	
	emptySentences := OrderedCollection new.
	oneEmptySentence := OrderedCollection with: emptySentence.
	
	oneNonEmptySentence := OrderedCollection with: helloWorldSentence.
	
	multipleSentences := OrderedCollection with: helloWorldSentence
										with: aloaloSentence
										with: emptySentence
										with: beginningSentence
										with: rosesSentence
										with: armadilloSentence.
										
	allSentencesStartWithUpperA := OrderedCollection with: aloaloSentence
												with: beginningSentence 
												with: armadilloSentence.
												
	sentencesUpperL := OrderedCollection with: wavesSentences
										with:rosesSentence.
										
	sentencesLowerL := OrderedCollection with: rosesLowerSentence.
	
	twoSentencesUpperLOneSentenceLowerL := OrderedCollection with:wavesSentences 
																with:rosesSentence 
																with:rosesLowerSentence .

	multipleSentences2 := OrderedCollection with: wavesSentences
											with: rosesSentence 
											with:rosesLowerSentence 
											with:rosesSentence2.! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'iniesz 4/28/2022 02:56:22'!
test01SentenceFinderStackedSentencesEqualsSentencesCollection
	
	self assert: (SentenceFinderByPrefix with: emptySentences) stackedSentencesAsCollection 
		equals: emptySentences.

	self assert: (SentenceFinderByPrefix with: oneEmptySentence) stackedSentencesAsCollection 
		equals: oneEmptySentence.

	self assert: (SentenceFinderByPrefix with: oneNonEmptySentence) stackedSentencesAsCollection 
		equals: oneNonEmptySentence.	
	
	self assert: (SentenceFinderByPrefix with: multipleSentences ) stackedSentencesAsCollection
		equals: multipleSentences .! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'iniesz 4/28/2022 02:39:41'!
test02SentenceFinderWithEmptySentencesFindsEmptyCollection	
	| sentenceFinder emptyPrefix arbitraryPrefix oneLetterPrefix emptyCollection |
	sentenceFinder := SentenceFinderByPrefix with: emptySentences.
	
	emptyCollection := OrderedCollection new.

	arbitraryPrefix:='blablabla'.
	oneLetterPrefix:='a'.
	emptyPrefix:=''.
	
	self assert: (sentenceFinder find:arbitraryPrefix) equals: emptyCollection.
	self assert: (sentenceFinder find:oneLetterPrefix) equals: emptyCollection.
	self assert: (sentenceFinder find:emptyPrefix) equals: emptyCollection.
	! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'iniesz 4/28/2022 02:42:14'!
test03SentenceFinderFindNotFoundPrefixEqualsEmptyCollection
	| sentenceFinder notFoundPrefix emptyCollection |
	sentenceFinder := SentenceFinderByPrefix with: multipleSentences.

	emptyCollection := OrderedCollection new.
	notFoundPrefix := 'This prefix does not appear in any sentence'.
	
	self assert: (sentenceFinder find: notFoundPrefix) equals: emptyCollection .! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'iniesz 4/28/2022 03:13:32'!
test04SentenceFinderFindPrefixInAllSentencesReturnsAllSentences
	| sentenceFinder allSentences prefixInAllSentences |
	sentenceFinder := SentenceFinderByPrefix with: allSentencesStartWithUpperA .
	
	allSentences := allSentencesStartWithUpperA .
	prefixInAllSentences:='A'.
	
	self assert: (sentenceFinder find: prefixInAllSentences) equals: allSentences .! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'iniesz 4/28/2022 03:16:02'!
test05SentenceFinderFindPrefixPreservesOrderInReturnedCollection
	| sentenceFinder prefixInAllSentences allSentencesStartingWithA |
	sentenceFinder := SentenceFinderByPrefix with: multipleSentences .
	
	allSentencesStartingWithA := allSentencesStartWithUpperA .
	prefixInAllSentences:='A'.
	
	self assert: (sentenceFinder find: prefixInAllSentences) equals: allSentencesStartingWithA .! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'iniesz 4/28/2022 03:23:53'!
test06SentenceFinderFindIsCaseSensitive
	| sentenceFinder lowerLPrefix upperLPrefix |
	sentenceFinder := SentenceFinderByPrefix with: twoSentencesUpperLOneSentenceLowerL .
	
	upperLPrefix := 'L'.
	lowerLPrefix:='l'.
	
	self assert: (sentenceFinder find: upperLPrefix) equals: sentencesUpperL.
	self assert: (sentenceFinder find: lowerLPrefix) equals: sentencesLowerL.! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'iniesz 4/28/2022 03:35:14'!
test07SentenceFinderFindCorrectlyFilterSentences
	| sentenceFinder expectedSentences prefixUpperA |
	sentenceFinder := SentenceFinderByPrefix with: multipleSentences .
	
	prefixUpperA := 'A'.
	expectedSentences := OrderedCollection with: aloaloSentence 
										with: beginningSentence 
										with: armadilloSentence.
	
	self assert: (sentenceFinder find: prefixUpperA) equals: expectedSentences.! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'iniesz 4/28/2022 03:43:13'!
test08SentenceFinderFindPrefixComposedOfMultipleWords
	| sentenceFinder expectedSentences prefixComposedMultipleWords |
	sentenceFinder := SentenceFinderByPrefix with: multipleSentences2.
	
	prefixComposedMultipleWords := 'Las rosas son '.
	expectedSentences := OrderedCollection with: rosesSentence 
										with: rosesSentence2.
										
	self assert: (sentenceFinder find: prefixComposedMultipleWords) equals: expectedSentences.! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'stack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'operation auxiliar' stamp: 'FM 4/26/2022 22:56:37'!
popEmpty
	
	^self error: self class stackEmptyErrorDescription.! !

!OOStack methodsFor: 'operation auxiliar' stamp: 'FM 4/26/2022 22:56:26'!
popNotEmpty
	
	^stack removeLast.! !

!OOStack methodsFor: 'operation auxiliar' stamp: 'FM 4/26/2022 22:56:51'!
topEmpty
	
	^self error: self class stackEmptyErrorDescription.! !

!OOStack methodsFor: 'operation auxiliar' stamp: 'FM 4/26/2022 22:57:02'!
topNotEmpty
	
	^stack last.! !


!OOStack methodsFor: 'initialization' stamp: 'FM 4/26/2022 22:28:23'!
initialize
	
	stack := OrderedCollection new.! !


!OOStack methodsFor: 'operation' stamp: 'FM 4/26/2022 22:34:21'!
isEmpty
	
	^self size = 0.! !

!OOStack methodsFor: 'operation' stamp: 'FM 4/26/2022 23:01:04'!
pop
	
	^(StackStatus statusOf: self) pop.! !

!OOStack methodsFor: 'operation' stamp: 'FM 4/26/2022 22:29:36'!
push: anValue
	
	stack add: anValue.! !

!OOStack methodsFor: 'operation' stamp: 'FM 4/26/2022 22:34:12'!
size
	
	^stack size.! !

!OOStack methodsFor: 'operation' stamp: 'FM 4/26/2022 23:01:10'!
top
	
	^(StackStatus statusOf: self) top.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/16/2021 17:39:43'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: 'stackedSentences'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'initialization' stamp: 'iniesz 4/27/2022 22:09:05'!
initializeWithSentences: theStackedSentences
	stackedSentences := theStackedSentences.
	! !


!SentenceFinderByPrefix methodsFor: 'finder operations' stamp: 'iniesz 4/28/2022 03:29:01'!
find: aPrefix
	^ self find: aPrefix putFoundSentencesInto: OrderedCollection new.! !


!SentenceFinderByPrefix methodsFor: 'testing' stamp: 'iniesz 4/27/2022 22:10:11'!
stackedSentences 
	^ stackedSentences.! !

!SentenceFinderByPrefix methodsFor: 'testing' stamp: 'iniesz 4/28/2022 01:30:54'!
stackedSentencesAsCollection
	^ self stackedSentencesInCollection: OrderedCollection new.! !

!SentenceFinderByPrefix methodsFor: 'testing' stamp: 'iniesz 4/28/2022 03:03:44'!
stackedSentencesInCollection: aCollection
	self doWithStackedSentences: [:sentence | aCollection add: sentence].
	^ aCollection.
	
	"(stackedSentences isEmpty)
		ifFalse: [
			|sentence|
			sentence := stackedSentences pop.
			self stackedSentencesInCollection: aCollection.
			aCollection add: sentence.
			stackedSentences push:sentence.].
		
	^ aCollection."! !


!SentenceFinderByPrefix methodsFor: 'private' stamp: 'iniesz 4/28/2022 04:58:32'!
doWithStackedSentences: aBlock
	(stackedSentences isEmpty)
		ifFalse: [
			|sentence|
			sentence := stackedSentences pop.
			self doWithStackedSentences: aBlock.
			aBlock value: sentence.
			stackedSentences push:sentence.].
		
	"^ [] value."! !

!SentenceFinderByPrefix methodsFor: 'private' stamp: 'iniesz 4/28/2022 03:27:55'!
find: aPrefix putFoundSentencesInto: aCollection
	self doWithStackedSentences: [:sentence |
			(sentence beginsWith: aPrefix)
				ifTrue:[aCollection add: sentence].
			].
		
	^aCollection.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'as yet unclassified' stamp: 'iniesz 4/27/2022 22:09:23'!
with: theSentences
	| stackedSentences |
	stackedSentences := OOStack new.
	theSentences do: [:aSentence | stackedSentences push: aSentence].

	^ self new initializeWithSentences: stackedSentences.! !


!classDefinition: #StackStatus category: 'Stack-Exercise'!
Object subclass: #StackStatus
	instanceVariableNames: 'stack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackStatus methodsFor: 'operation' stamp: 'FM 4/26/2022 22:57:19'!
pop

	self subclassResponsibility.
! !

!StackStatus methodsFor: 'operation' stamp: 'FM 4/26/2022 22:57:42'!
top

	self subclassResponsibility.! !


!StackStatus methodsFor: 'initialization' stamp: 'FM 4/26/2022 22:54:39'!
initializeFor: anStack
	
	stack := anStack.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StackStatus class' category: 'Stack-Exercise'!
StackStatus class
	instanceVariableNames: ''!

!StackStatus class methodsFor: 'status search' stamp: 'FM 4/26/2022 23:04:43'!
isStatusFor: anStack

	self subclassResponsibility.! !

!StackStatus class methodsFor: 'status search' stamp: 'FM 4/26/2022 23:05:14'!
statusOf: anStack

	| stackStatusClass |

	stackStatusClass _ self subclasses detect: [ :anStackStatusClass | anStackStatusClass isStatusFor: anStack ].
	
	^ stackStatusClass for: anStack.! !


!StackStatus class methodsFor: 'instance creation' stamp: 'FM 4/26/2022 22:53:02'!
for: anStack

	^self new initializeFor: anStack.! !


!classDefinition: #StackStatusEmpty category: 'Stack-Exercise'!
StackStatus subclass: #StackStatusEmpty
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackStatusEmpty methodsFor: 'operation' stamp: 'FM 4/26/2022 22:58:54'!
pop

	^stack popEmpty.! !

!StackStatusEmpty methodsFor: 'operation' stamp: 'FM 4/26/2022 22:58:58'!
top

	^stack topEmpty.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StackStatusEmpty class' category: 'Stack-Exercise'!
StackStatusEmpty class
	instanceVariableNames: ''!

!StackStatusEmpty class methodsFor: 'status search' stamp: 'FM 4/26/2022 23:04:48'!
isStatusFor: anStack

	^anStack isEmpty! !


!classDefinition: #StackStatusNoEmpty category: 'Stack-Exercise'!
StackStatus subclass: #StackStatusNoEmpty
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackStatusNoEmpty methodsFor: 'operation' stamp: 'FM 4/26/2022 22:58:40'!
pop

	^stack popNotEmpty.! !

!StackStatusNoEmpty methodsFor: 'operation' stamp: 'FM 4/26/2022 22:58:47'!
top

	^stack topNotEmpty.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StackStatusNoEmpty class' category: 'Stack-Exercise'!
StackStatusNoEmpty class
	instanceVariableNames: ''!

!StackStatusNoEmpty class methodsFor: 'status search' stamp: 'FM 4/26/2022 23:04:57'!
isStatusFor: anStack

	^anStack isEmpty not! !
