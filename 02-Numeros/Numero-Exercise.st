!classDefinition: #NumeroTest category: 'Numero-Exercise'!
TestCase subclass: #NumeroTest
	instanceVariableNames: 'zero one two four oneFifth oneHalf five twoFifth twoTwentyfifth fiveHalfs three eight negativeOne negativeTwo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:11'!
test01isCeroReturnsTrueWhenAskToZero

	self assert: zero isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:12'!
test02isCeroReturnsFalseWhenAskToOthersButZero

	self deny: one isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test03isOneReturnsTrueWhenAskToOne

	self assert: one isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test04isOneReturnsFalseWhenAskToOtherThanOne

	self deny: zero isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:14'!
test05EnteroAddsWithEnteroCorrectly

	self assert: one + one equals: two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:18'!
test06EnteroMultipliesWithEnteroCorrectly

	self assert: two * two equals: four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:20'!
test07EnteroDividesEnteroCorrectly

	self assert: two / two equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:38'!
test08FraccionAddsWithFraccionCorrectly
"
    La suma de fracciones es:
	 
	a/b + c/d = (a.d + c.b) / (b.d)
	 
	SI ESTAN PENSANDO EN LA REDUCCION DE FRACCIONES NO SE PREOCUPEN!!
	TODAVIA NO SE ESTA TESTEANDO ESE CASO
"
	| sevenTenths |

	sevenTenths := (Entero with: 7) / (Entero with: 10).

	self assert: oneFifth + oneHalf equals: sevenTenths! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:52'!
test09FraccionMultipliesWithFraccionCorrectly
"
    La multiplicacion de fracciones es:
	 
	(a/b) * (c/d) = (a.c) / (b.d)
"

	self assert: oneFifth * twoFifth equals: twoTwentyfifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:56'!
test10FraccionDividesFraccionCorrectly
"
    La division de fracciones es:
	 
	(a/b) / (c/d) = (a.d) / (b.c)
"

	self assert: oneHalf / oneFifth equals: fiveHalfs! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test11EnteroAddsFraccionCorrectly
"
	Ahora empieza la diversion!!
"

	self assert: one + oneFifth equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test12FraccionAddsEnteroCorrectly

	self assert: oneFifth + one equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:50'!
test13EnteroMultipliesFraccionCorrectly

	self assert: two * oneFifth equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:52'!
test14FraccionMultipliesEnteroCorrectly

	self assert: oneFifth * two equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:57'!
test15EnteroDividesFraccionCorrectly

	self assert: one / twoFifth equals: fiveHalfs  ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:59'!
test16FraccionDividesEnteroCorrectly

	self assert: twoFifth / five equals: twoTwentyfifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:38'!
test17AFraccionCanBeEqualToAnEntero

	self assert: two equals: four / two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:39'!
test18AparentFraccionesAreEqual

	self assert: oneHalf equals: two / four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:40'!
test19AddingFraccionesCanReturnAnEntero

	self assert: oneHalf + oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test20MultiplyingFraccionesCanReturnAnEntero

	self assert: (two/five) * (five/two) equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test21DividingFraccionesCanReturnAnEntero

	self assert: oneHalf / oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:43'!
test22DividingEnterosCanReturnAFraccion

	self assert: two / four equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test23CanNotDivideEnteroByZero

	self 
		should: [ one / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test24CanNotDivideFraccionByZero

	self 
		should: [ oneHalf / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test25AFraccionCanNotBeZero

	self deny: oneHalf isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test26AFraccionCanNotBeOne

	self deny: oneHalf isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 4/15/2021 16:45:35'!
test27EnteroSubstractsEnteroCorrectly

	self assert: four - one equals: three! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:47:41'!
test28FraccionSubstractsFraccionCorrectly
	
	self assert: twoFifth - oneFifth equals: oneFifth.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:00'!
test29EnteroSubstractsFraccionCorrectly

	self assert: one - oneHalf equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:05'!
test30FraccionSubstractsEnteroCorrectly

	| sixFifth |
	
	sixFifth := (Entero with: 6) / (Entero with: 5).
	
	self assert: sixFifth - one equals: oneFifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:08'!
test31SubstractingFraccionesCanReturnAnEntero

	| threeHalfs |
	
	threeHalfs := (Entero with: 3) / (Entero with: 2).
	
	self assert: threeHalfs - oneHalf equals: one.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:48'!
test32SubstractingSameEnterosReturnsZero

	self assert: one - one equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:01'!
test33SubstractingSameFraccionesReturnsZero

	self assert: oneHalf - oneHalf equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:14'!
test34SubstractingAHigherValueToANumberReturnsANegativeNumber

	| negativeThreeHalfs |
	
	negativeThreeHalfs := (Entero with: -3) / (Entero with: 2).	

	self assert: one - fiveHalfs equals: negativeThreeHalfs.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:23'!
test35FibonacciZeroIsOne

	self assert: zero fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:32'!
test36FibonacciOneIsOne

	self assert: one fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:39'!
test37FibonacciEnteroReturnsAddingPreviousTwoFibonacciEnteros

	self assert: four fibonacci equals: five.
	self assert: three fibonacci equals: three. 
	self assert: five fibonacci equals: four fibonacci + three fibonacci.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:47'!
test38FibonacciNotDefinedForNegativeNumbers

	self 
		should: [negativeOne fibonacci]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Entero negativeFibonacciErrorDescription ].! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:55'!
test39NegationOfEnteroIsCorrect

	self assert: two negated equals: negativeTwo.
		! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:03'!
test40NegationOfFraccionIsCorrect

	self assert: oneHalf negated equals: negativeOne / two.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:11'!
test41SignIsCorrectlyAssignedToFractionWithTwoNegatives

	self assert: oneHalf equals: (negativeOne / negativeTwo)! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:17'!
test42SignIsCorrectlyAssignedToFractionWithNegativeDivisor

	self assert: oneHalf negated equals: (one / negativeTwo)! !


!NumeroTest methodsFor: 'setup' stamp: 'NR 9/23/2018 23:46:28'!
setUp

	zero := Entero with: 0.
	one := Entero with: 1.
	two := Entero with: 2.
	three:= Entero with: 3.
	four := Entero with: 4.
	five := Entero with: 5.
	eight := Entero with: 8.
	negativeOne := Entero with: -1.
	negativeTwo := Entero with: -2.
	
	oneHalf := one / two.
	oneFifth := one / five.
	twoFifth := two / five.
	twoTwentyfifth := two / (Entero with: 25).
	fiveHalfs := five / two.
	! !


!classDefinition: #Numero category: 'Numero-Exercise'!
Object subclass: #Numero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
* aMultiplier

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
+ anAdder

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 22:21:28'!
- aSubtrahend

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
/ aDivisor

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
invalidNumberType

	self error: self class invalidNumberTypeErrorDescription! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 23:37:13'!
negated
	
	^self * (Entero with: -1)! !


!Numero methodsFor: 'testing' stamp: 'NR 9/23/2018 23:36:49'!
isNegative

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isOne

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isZero

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Numero class' category: 'Numero-Exercise'!
Numero class
	instanceVariableNames: ''!

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:02'!
canNotDivideByZeroErrorDescription

	^'No se puede dividir por cero!!!!!!'! !

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:09'!
invalidNumberTypeErrorDescription
	
	^ 'Tipo de n�mero inv�lido!!!!!!'! !


!classDefinition: #Entero category: 'Numero-Exercise'!
Numero subclass: #Entero
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Entero methodsFor: 'arithmetic operations' stamp: 'iniesz 4/22/2022 22:58:40'!
* aMultiplier 
	^ aMultiplier multiplyToEntero: self.
"
	(aMultiplier isKindOf: self class) 
		ifTrue:[
			^ aMultiplier multiplyToEntero: self.
			].
		
	(aMultiplier isKindOf: Fraccion)
		ifTrue:[
			^(self * aMultiplier numerator) / aMultiplier denominator.
			].


	self invalidNumberType."
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'iniesz 4/22/2022 22:48:08'!
+ anAdder 
	^ anAdder addToEntero: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'iniesz 4/22/2022 23:29:49'!
- aSubtrahend 
	^ aSubtrahend substractToEntero: self.
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'iniesz 4/22/2022 23:36:08'!
/ aDivisor 
	^ aDivisor divideToEntero: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'iniesz 4/22/2022 23:41:58'!
// aDivisor 
	^ aDivisor integerDivisionToEntero: self.
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'iniesz 4/25/2022 14:47:12'!
fibonacci

	| one two |
	
	one := Entero with: 1.
	two := Entero with: 2.
	
	^ (self - one) fibonacci + (self - two) fibonacci.
		! !

!Entero methodsFor: 'arithmetic operations' stamp: 'iniesz 4/25/2022 14:42:54'!
greatestCommonDivisorWith: anEntero 
	
	^self class theEnteroInstantiator with: (value gcd: anEntero integerValue)! !


!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 21:01'!
= anObject

	^(anObject isKindOf: self class) and: [ value = anObject integerValue ]! !

!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:17'!
hash

	^value hash! !


!Entero methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 20:09'!
initalizeWith: aValue 
	
	value := aValue! !


!Entero methodsFor: 'value' stamp: 'HernanWilkinson 5/7/2016 21:02'!
integerValue

	"Usamos integerValue en vez de value para que no haya problemas con el mensaje value implementado en Object - Hernan"
	
	^value! !


!Entero methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:53:19'!
printOn: aStream

	aStream print: value ! !


!Entero methodsFor: 'testing' stamp: 'iniesz 4/23/2022 21:09:00'!
isNegative
	
	^ false.! !

!Entero methodsFor: 'testing' stamp: 'iniesz 4/23/2022 17:04:08'!
isOne
	
	^ false.! !

!Entero methodsFor: 'testing' stamp: 'iniesz 4/23/2022 17:04:13'!
isZero
	
	^ false.! !


!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 10:06:31'!
addToEntero: anEnteroTargetOfAddition
	
	^ self doIntegerArithmeticOperation: [:targetEntero | targetEntero integerValue + value]
		toNumero: anEnteroTargetOfAddition.
! !

!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 01:06:07'!
addToFraccion: fraccionTargetOfAddition
	| newNumerator |		
	newNumerator := self * fraccionTargetOfAddition denominator + fraccionTargetOfAddition numerator.
	
	^newNumerator / fraccionTargetOfAddition denominator. ! !

!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 10:07:53'!
divideToEntero: anEnteroTargetOfDivision

	^ self doArithmeticOperation:[:targetEntero | Fraccion with: targetEntero over: self] 
		toNumero: anEnteroTargetOfDivision.! !

!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 00:48:17'!
divideToFraccion: aFraccionTargetOfDivision
	^ aFraccionTargetOfDivision numerator / (aFraccionTargetOfDivision denominator * self).! !

!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 10:11:37'!
doArithmeticOperation: aBlockArithmeticOperation toNumero: aNumeroTargetOfOperation

	^ (aBlockArithmeticOperation value: aNumeroTargetOfOperation) value
	! !

!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/25/2022 14:44:49'!
doIntegerArithmeticOperation: aBlockIntegerOperation toNumero: aNumeroTargetOfOperation

	^ self class theEnteroInstantiator with: 
		(self doArithmeticOperation: aBlockIntegerOperation 
			toNumero: aNumeroTargetOfOperation) value.! !

!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 10:06:31'!
integerDivisionToEntero: anEnteroTargetOfIntegerDivision
	^self doIntegerArithmeticOperation: [:targetEntero | targetEntero integerValue // value]
		toNumero: anEnteroTargetOfIntegerDivision.
! !

!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 10:06:31'!
integerDivisionToFraccion: aFraccionTargetOfIntegerDivision

	^self doIntegerArithmeticOperation: 
			[:targetFraccion | 
				|targetNumerator targetDenominator|
				targetNumerator:= targetFraccion numerator.
				targetDenominator:= targetFraccion denominator.
				targetNumerator integerValue // (targetDenominator * self) integerValue]
		toNumero: aFraccionTargetOfIntegerDivision.

	"^ aFraccionTargetOfIntegerDivision numerator integerValue // (aFraccionTargetOfIntegerDivision denominator * self) integerValue."! !

!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 10:06:31'!
multiplyToEntero: anEnteroTargetOfMultiplication
	^self doIntegerArithmeticOperation: [:targetEntero | targetEntero integerValue * value]
		toNumero: anEnteroTargetOfMultiplication.
! !

!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 00:40:06'!
multiplyToFraccion: aFraccionTargetOfMultiplication
	^ aFraccionTargetOfMultiplication numerator * self / aFraccionTargetOfMultiplication denominator.! !

!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 10:06:31'!
substractToEntero: anEnteroTargetOfSubstraction
	^ self doIntegerArithmeticOperation: [:targetEntero | targetEntero integerValue - value]
		toNumero: anEnteroTargetOfSubstraction.
! !

!Entero methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 00:43:04'!
substractToFraccion: aFraccionTargetOfSubstraction
	| newNumerator |		
	newNumerator := aFraccionTargetOfSubstraction numerator - (self * aFraccionTargetOfSubstraction denominator).
			
	^newNumerator / aFraccionTargetOfSubstraction denominator. ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Entero class' category: 'Numero-Exercise'!
Entero class
	instanceVariableNames: ''!

!Entero class methodsFor: 'instance creation' stamp: 'NR 4/15/2021 16:42:24'!
negativeFibonacciErrorDescription
	^ ' Fibonacci no est� definido aqu� para Enteros Negativos!!!!!!'! !

!Entero class methodsFor: 'instance creation' stamp: 'iniesz 4/23/2022 17:48:16'!
theEnteroInstantiator
	^ Entero.! !

!Entero class methodsFor: 'instance creation' stamp: 'iniesz 4/23/2022 21:14:29'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	
	(aValue = 0)
		ifTrue:[^ Cero new.].
	
	(aValue = 1)
		ifTrue:[^ Uno new.].
		
	(aValue < 0)
		ifTrue:[^ EnteroNegativo new initalizeWith: aValue].
	
	^self theEnteroInstantiator new initalizeWith: aValue! !


!classDefinition: #Cero category: 'Numero-Exercise'!
Entero subclass: #Cero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Cero methodsFor: 'testing' stamp: 'iniesz 4/23/2022 21:10:07'!
isNegative
	^ false.! !

!Cero methodsFor: 'testing' stamp: 'iniesz 4/23/2022 17:03:45'!
isOne
	^ false.! !

!Cero methodsFor: 'testing' stamp: 'iniesz 4/23/2022 17:03:51'!
isZero
	^ true.! !


!Cero methodsFor: 'arithmetic operations' stamp: 'iniesz 4/23/2022 21:25:37'!
fibonacci
	^ self class theEnteroInstantiator with: 1.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cero class' category: 'Numero-Exercise'!
Cero class
	instanceVariableNames: ''!

!Cero class methodsFor: 'instance creation' stamp: 'iniesz 4/25/2022 14:39:23'!
new
	^ super new initalizeWith: 0.! !

!Cero class methodsFor: 'instance creation' stamp: 'iniesz 4/25/2022 14:34:11'!
with: aValue
	self error: 'Cero no puede ser instanciado a traves de un valor.'.! !


!classDefinition: #EnteroNegativo category: 'Numero-Exercise'!
Entero subclass: #EnteroNegativo
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroNegativo methodsFor: 'testing' stamp: 'iniesz 4/23/2022 21:09:23'!
isNegative
	^ true.! !

!EnteroNegativo methodsFor: 'testing' stamp: 'iniesz 4/23/2022 21:09:48'!
isOne
	^ false.! !

!EnteroNegativo methodsFor: 'testing' stamp: 'iniesz 4/23/2022 21:09:42'!
isZero
	^ false.! !


!EnteroNegativo methodsFor: 'arithmetic operations' stamp: 'iniesz 4/23/2022 21:17:28'!
fibonacci
	^ self error: Entero negativeFibonacciErrorDescription.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroNegativo class' category: 'Numero-Exercise'!
EnteroNegativo class
	instanceVariableNames: ''!

!EnteroNegativo class methodsFor: 'as yet unclassified' stamp: 'iniesz 4/23/2022 21:10:59'!
new
	^ super new.! !

!EnteroNegativo class methodsFor: 'as yet unclassified' stamp: 'iniesz 4/25/2022 14:36:22'!
with: aValue
	self error: 'Los enteros negativos deben instanciarse a traves de Entero with: aValue'.! !


!classDefinition: #Uno category: 'Numero-Exercise'!
Entero subclass: #Uno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Uno methodsFor: 'testing' stamp: 'iniesz 4/23/2022 21:10:16'!
isNegative
	^ false.! !

!Uno methodsFor: 'testing' stamp: 'iniesz 4/23/2022 17:02:57'!
isOne
	^ true.! !

!Uno methodsFor: 'testing' stamp: 'iniesz 4/23/2022 17:03:05'!
isZero
	^ false.! !


!Uno methodsFor: 'arithmetic operations' stamp: 'iniesz 4/23/2022 21:26:10'!
fibonacci
	^  self class theEnteroInstantiator with: 1! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Uno class' category: 'Numero-Exercise'!
Uno class
	instanceVariableNames: ''!

!Uno class methodsFor: 'instance creation' stamp: 'iniesz 4/23/2022 17:11:30'!
new
	^ super new initalizeWith: 1.! !

!Uno class methodsFor: 'instance creation' stamp: 'iniesz 4/25/2022 14:34:21'!
with: aValue
	self error: 'Uno no puede ser instanciado a traves de un valor.'.! !


!classDefinition: #Fraccion category: 'Numero-Exercise'!
Numero subclass: #Fraccion
	instanceVariableNames: 'numerator denominator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Fraccion methodsFor: 'arithmetic operations' stamp: 'iniesz 4/23/2022 00:40:19'!
* aMultiplier 
	^ aMultiplier multiplyToFraccion: self.
	
	
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'iniesz 4/23/2022 00:37:42'!
+ anAdder 
	^ anAdder addToFraccion: self.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'iniesz 4/23/2022 00:45:27'!
- aSubtrahend  
	^ aSubtrahend substractToFraccion: self.
! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'iniesz 4/23/2022 00:49:00'!
/ aDivisor 
	^ aDivisor divideToFraccion: self.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'iniesz 4/23/2022 00:53:22'!
// aDivisor 
	^ aDivisor integerDivisionToFraccion: self.! !


!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:42'!
= anObject

	^(anObject isKindOf: self class) and: [ (numerator * anObject denominator) = (denominator * anObject numerator) ]! !

!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:50'!
hash

	^(numerator hash / denominator hash) hash! !


!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
denominator

	^ denominator! !

!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
numerator

	^ numerator! !


!Fraccion methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 22:54'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	numerator := aNumerator.
	denominator := aDenominator ! !


!Fraccion methodsFor: 'testing' stamp: 'NR 9/23/2018 23:41:38'!
isNegative
	
	^numerator < 0! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isOne
	
	^false! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isZero
	
	^false! !


!Fraccion methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:54:46'!
printOn: aStream

	aStream 
		print: numerator;
		nextPut: $/;
		print: denominator ! !


!Fraccion methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 01:09:11'!
addToEntero: anEnteroTargetOfAddition
	^anEnteroTargetOfAddition * denominator + numerator / denominator.
				
			
			
			! !

!Fraccion methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 00:37:19'!
addToFraccion: aFraccionTargetOfAddition
	| newNumerator newDenominator |		
	newNumerator := (numerator * aFraccionTargetOfAddition denominator) + (denominator * aFraccionTargetOfAddition numerator).
	newDenominator := denominator * aFraccionTargetOfAddition denominator.
			
	^newNumerator / newDenominator .! !

!Fraccion methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 01:09:23'!
divideToEntero: anEnteroTargetOfDivision
	^ (anEnteroTargetOfDivision *  denominator) / numerator.
! !

!Fraccion methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 01:02:44'!
divideToFraccion: aFraccionTargetOfDivision		
	^(aFraccionTargetOfDivision numerator * self denominator) / (aFraccionTargetOfDivision denominator * self numerator).! !

!Fraccion methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/25/2022 14:52:38'!
integerDivisionToEntero: anEnteroTargetOfIntegerDivision
	^ Entero with: 
		(anEnteroTargetOfIntegerDivision integerValue *  numerator integerValue) // denominator integerValue.! !

!Fraccion methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 00:51:18'!
integerDivisionToFraccion: aFraccionTargetOfIntegerDivision
	^ (aFraccionTargetOfIntegerDivision numerator * numerator) // (aFraccionTargetOfIntegerDivision denominator * denominator).! !

!Fraccion methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 01:09:48'!
multiplyToEntero: anEnteroTargetOfMultiplication
	^ anEnteroTargetOfMultiplication * numerator / denominator.! !

!Fraccion methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 00:38:53'!
multiplyToFraccion: aFraccionTargetOfMultiplication
	^ (aFraccionTargetOfMultiplication numerator * self numerator) / (aFraccionTargetOfMultiplication denominator * self denominator).
	! !

!Fraccion methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 01:10:03'!
substractToEntero: anEnteroReceiverOfSubstraction
	^anEnteroReceiverOfSubstraction * denominator - numerator / denominator.! !

!Fraccion methodsFor: 'dispatched-arithmetic-operations' stamp: 'iniesz 4/23/2022 00:41:50'!
substractToFraccion: aFraccionTargetOfSubstraction
	| newNumerator newDenominator |		
	newNumerator := (aFraccionTargetOfSubstraction numerator * denominator) - (numerator * aFraccionTargetOfSubstraction denominator).
	newDenominator :=  denominator * aFraccionTargetOfSubstraction denominator.
	
	^newNumerator / newDenominator 
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Fraccion class' category: 'Numero-Exercise'!
Fraccion class
	instanceVariableNames: ''!

!Fraccion class methodsFor: 'intance creation' stamp: 'NR 9/23/2018 23:45:19'!
with: aDividend over: aDivisor

	| greatestCommonDivisor numerator denominator |
	
	aDivisor isZero ifTrue: [ self error: self canNotDivideByZeroErrorDescription ].
	aDividend isZero ifTrue: [ ^aDividend ].
	
	aDivisor isNegative ifTrue:[ ^aDividend negated / aDivisor negated].
	
	greatestCommonDivisor := aDividend greatestCommonDivisorWith: aDivisor. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := aDivisor // greatestCommonDivisor.
	
	denominator isOne ifTrue: [ ^numerator ].

	^self new initializeWith: numerator over: denominator
	! !
