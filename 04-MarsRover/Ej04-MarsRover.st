!classDefinition: #MarsRoverTest category: 'Ej04-MarsRover'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 01:59:05'!
assertNewMarsRoverIsAt: aPosition heading: aDirectionSymbol
	
	| rover |
	rover := MarsRover at: aPosition heading: aDirectionSymbol.
	
	self assert: (rover isAt: aPosition heading: aDirectionSymbol).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 00:09:48'!
test01NewMarsRoverStartsAtOriginHeadingNorth
	
	| rover |
	rover := MarsRover new.
	
	self assert: (rover isAt: 0@0 heading: #North) ! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 00:19:13'!
test02MarsRoverHeadingEastAfterRotateRightCommand
	
	| rover |
	rover := MarsRover new.
	
	rover command: 'r'.
	
	self assert: (rover isAt: 0@0 heading: #East) ! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 00:27:30'!
test03MarsRoverHeadingSouthAfterTwoRotateRightCommands
	
	| rover |
	rover := MarsRover new.
	
	self with: rover processCommand: 'r' times: 2.
	
	self assert: (rover isAt: 0@0 heading: #South).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 00:27:37'!
test04MarsRoverHeadingWestAfterThreeRotateRightCommands
	
	| rover |
	rover := MarsRover new.
	
	self with: rover processCommand: 'r' times: 3.
	
	self assert: (rover isAt: 0@0 heading: #West).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 00:28:12'!
test05MarsRoverHeadingNorthAfterFourRotateRightCommands
	
	| rover |
	rover := MarsRover new.
	
	self with: rover processCommand: 'r' times: 4.
	
	self assert: (rover isAt: 0@0 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 00:30:06'!
test06MarsRoverHeadingWestAfterRotateLeftCommand
	
	| rover |
	rover := MarsRover new.
	
	rover command: 'l'.
	
	self assert: (rover isAt: 0@0 heading: #West).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 00:35:56'!
test07MarsRoverHeadingSouthAfterRotateLeftCommandTwoTimes
	
	| rover |
	rover := MarsRover new.
	
	self with: rover processCommand: 'l' times: 2.
	
	self assert: (rover isAt: 0@0 heading: #South).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 00:36:03'!
test08MarsRoverHeadingEastAfterRotateLeftCommandThreeTimes
	
	| rover |
	rover := MarsRover new.
	
	self with: rover processCommand: 'l' times: 3.
	
	self assert: (rover isAt: 0@0 heading: #East).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 00:36:12'!
test09MarsRoverHeadingNorthAfterRotateLeftCommandFourTimes
	
	| rover |
	rover := MarsRover new.
	
	self with: rover processCommand: 'l' times: 4.
	
	self assert: (rover isAt: 0@0 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 01:24:57'!
test10MarsRoverHeadingNorthForwards
	
	| rover |
	rover := MarsRover new.
	
	rover command: 'f'.
	
	self assert: (rover isAt: 0@1 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 01:34:08'!
test11MarsRoverHeadingNorthForwardsTwoTimes
	
	| rover |
	rover := MarsRover new.
	
	self with: rover processCommand: 'f' times: 2.
	
	self assert: (rover isAt: 0@2 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 01:33:59'!
test12MarsRoverHeadingNorthBackwards
	
	| rover |
	rover := MarsRover new.
	
	rover command: 'b'.
	
	self assert: (rover isAt: 0@-1 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 01:37:24'!
test13MarsRoverHeadingNorthBackwardsTwoTimes
	
	| rover |
	rover := MarsRover new.
	
	self with: rover processCommand: 'b' times: 2.
	
	self assert: (rover isAt: 0@-2 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:21:08'!
test14MarsRoverAtPositionHeadingEast
	
	"| rover aPosition |
	aPosition := 123@1984.
	rover := MarsRover at: aPosition heading: #East.
	
	self assert: (rover isAt: aPosition heading: #East)."
	self assertNewMarsRoverIsAt:  123@1984 heading: #East.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:21:20'!
test15MarsRoverAtPositionHeadingSouth
	
	"| rover aPosition headingDirection |
	aPosition := -888@2222.
	headingDirection := #South.
	rover := MarsRover at: aPosition heading: headingDirection.
	
	self assert: (rover isAt: aPosition heading: headingDirection)."
	
	self assertNewMarsRoverIsAt:  -888@2222 heading: #South.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:21:30'!
test16MarsRoverAtPositionHeadingWest
	
	"| rover aPosition headingDirection |
	aPosition := -555@-345.
	headingDirection := #West.
	rover := MarsRover at: aPosition heading: headingDirection.
	
	self assert: (rover isAt: aPosition heading: headingDirection)."
	self assertNewMarsRoverIsAt:  -555@-345 heading: #West.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:21:41'!
test17MarsRoverAtPositionHeadingNorth
	
	"| rover aPosition headingDirection |
	aPosition := 33333@101.
	headingDirection := #North.
	rover := MarsRover at: aPosition heading: headingDirection.
	
	self assert: (rover isAt: aPosition heading: headingDirection)."
	self assertNewMarsRoverIsAt: 33333@101 heading: #North.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:25:03'!
test18MarsRoverHeadingEastForwards
	
	| rover |
	rover := MarsRover at: 0@0 heading: #East.
	
	rover command: 'f'.
	
	self assert: (rover isAt: 1@0 heading: #East).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:26:42'!
test19MarsRoverHeadingEastForwardsTwoTimes
	
	| rover |
	rover := MarsRover at: 0@0 heading: #East.
	
	self with: rover processCommand: 'f' times: 2.
	
	self assert: (rover isAt: 2@0 heading: #East).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:27:18'!
test20MarsRoverHeadingEastBackwards
	
	| rover |
	rover := MarsRover at: 0@0 heading: #East.
	
	rover command: 'b'.
	
	self assert: (rover isAt: -1@0 heading: #East).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:29:19'!
test21MarsRoverHeadingEastBackwardsTwoTimes
	
	| rover |
	rover := MarsRover at: 0@0 heading: #East.
	
	rover command: 'b'.
	rover command: 'b'.
		
	self assert: (rover isAt: -2@0 heading: #East).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:30:36'!
test22MarsRoverHeadingSouthForwards
	
	| rover |
	rover := MarsRover at: 0@0 heading: #South.
	
	rover command: 'f'.
		
	self assert: (rover isAt: 0@-1 heading: #South).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:33:21'!
test23MarsRoverHeadingSouthForwardsTwoTimes
	
	| rover |
	rover := MarsRover at: 0@0 heading: #South.
	
	rover command: 'f'.
	rover command: 'f'.
			
	self assert: (rover isAt: 0@-2 heading: #South).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:33:12'!
test24MarsRoverHeadingSouthBackwards
	
	| rover |
	rover := MarsRover at: 0@0 heading: #South.
	
	rover command: 'b'.
	
	self assert: (rover isAt: 0@1 heading: #South).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:34:14'!
test25MarsRoverHeadingSouthBackwardsTwoTimes
	
	| rover |
	rover := MarsRover at: 0@0 heading: #South.
	
	rover command: 'b'.
	rover command: 'b'.
		
	self assert: (rover isAt: 0@2 heading: #South).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:35:56'!
test26MarsRoverHeadingWestForwards
	
	| rover |
	rover := MarsRover at: 0@0 heading: #West.
	
	rover command: 'f'.
		
	self assert: (rover isAt: -1@0 heading: #West).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:36:54'!
test27MarsRoverHeadingWestForwardsTwoTimes
	
	| rover |
	rover := MarsRover at: 0@0 heading: #West.
	
	rover command: 'f'.
	rover command: 'f'.
			
	self assert: (rover isAt: -2@0 heading: #West).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:37:59'!
test28MarsRoverHeadingWestBackwards
	
	| rover |
	rover := MarsRover at: 0@0 heading: #West.
	
	rover command: 'b'.
			
	self assert: (rover isAt: 1@0 heading: #West).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 02:39:04'!
test29MarsRoverHeadingWestBackwardsTwoTimes
	
	| rover |
	rover := MarsRover at: 0@0 heading: #West.
	
	rover command: 'b'.
	rover command: 'b'.
			
	self assert: (rover isAt: 2@0 heading: #West).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 16:51:27'!
test30MarsRoverCommandsStringTwoForwardCommands
	
	| rover |
	rover := MarsRover at: 0@0 heading: #North.
	
	rover command: 'ff'.
			
	self assert: (rover isAt: 0@2 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 16:46:37'!
test31MarsRoverCommandsStringTwoBackwardsCommands
	
	| rover |
	rover := MarsRover at: 0@0 heading: #North.
	
	rover command: 'bb'.
			
	self assert: (rover isAt: 0@-2 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 16:52:20'!
test32MarsRoverCommandsStringThreeForwardsThenTwoBackwards
	
	| rover |
	rover := MarsRover at: 0@0 heading: #North.
	
	rover command: 'fffbb'.
			
	self assert: (rover isAt: 0@1 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:51:11'!
test33MarsRoverCommandsStringThreeBackwardsThenTwoForward
	
	| rover |
	rover := MarsRover at: 0@0 heading: #North.
	
	rover command: 'bbbff'.
			
	self assert: (rover isAt: 0@-1 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:52:17'!
test34MarsRoverCommandsStringThreeForwardsThenTwoBackwardsThenFiveForwards
	
	| rover |
	rover := MarsRover at: 0@0 heading: #North.
	
	rover command: 'fffbbfffff'.
			
	self assert: (rover isAt: 0@6 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:52:54'!
test35MarsRoverCommandsStringThreeBackwardsThenTwoForwardThenFiveBackwards
	
	| rover |
	rover := MarsRover at: 0@0 heading: #North.
	
	rover command: 'bbbffbbbbb'.
			
	self assert: (rover isAt: 0@-6 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 18:37:21'!
test36MarsRoverOneInvalidCommandDoesNothing
	
	| rover |
	rover := MarsRover at: 0@0 heading: #North.
	
	rover command: '@'.
			
	self assert: (rover isAt: 0@0 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 18:38:52'!
test37MarsRoverMultipleInvalidCommandsDoNothing
	
	| rover |
	rover := MarsRover at: 0@0 heading: #North.
	
	rover command: '@X#'.
			
	self assert: (rover isAt: 0@0 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 18:40:30'!
test38MarsRoverCommandsExecutedUntilInvalidCommand
	
	| rover |
	rover := MarsRover at: 0@0 heading: #North.
	
	rover command: 'ffbbfffb@fffffbbf'.
			
	self assert: (rover isAt: 0@2 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 18:41:23'!
test39MarsRoverCommandsExecutedUntilFirstInvalidCommand
	
	| rover |
	rover := MarsRover at: 0@0 heading: #North.
	
	rover command: 'ffb#fffb@ffffYbbf'.
			
	self assert: (rover isAt: 0@1 heading: #North).! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 18:43:33'!
test40MarsRoverMultipleCommands
	
	| rover |
	rover := MarsRover at: 0@0 heading: #North.
	
	rover command: 'frfffrb@ffffYbbf'.
	self assert: (rover isAt: 3@2 heading: #South).! !


!MarsRoverTest methodsFor: 'rover-commands' stamp: 'iniesz 5/16/2022 00:26:13'!
with: aMarsRover processCommand: aCommandString times: timesAmount
	timesAmount timesRepeat: [aMarsRover command: aCommandString].! !


!classDefinition: #MarsRover category: 'Ej04-MarsRover'!
Object subclass: #MarsRover
	instanceVariableNames: 'directionHandler position'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!MarsRover methodsFor: 'movement' stamp: 'iniesz 5/16/2022 17:57:13'!
backward	
	directionHandler moveBackwards: self.! !

!MarsRover methodsFor: 'movement' stamp: 'iniesz 5/16/2022 01:17:21'!
directionHandler: aMarsRoverDirectionHandler
	directionHandler := aMarsRoverDirectionHandler.! !

!MarsRover methodsFor: 'movement' stamp: 'iniesz 5/16/2022 17:57:19'!
forward
	directionHandler moveForward: self.! !

!MarsRover methodsFor: 'movement' stamp: 'iniesz 5/16/2022 01:28:21'!
isAt: aPoint heading: aHeadingSymbol 
	^ (aPoint = position) and: (aHeadingSymbol = directionHandler directionSymbol)! !

!MarsRover methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:48:27'!
moveEast
	position := (position x+1) @ position y! !

!MarsRover methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:48:14'!
moveNorth
	position := position x @ (position y +1)! !

!MarsRover methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:48:44'!
moveSouth
	position := position x @ (position y -1)! !

!MarsRover methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:48:35'!
moveWest
	position := (position x-1) @ position y! !

!MarsRover methodsFor: 'movement' stamp: 'iniesz 5/16/2022 17:57:32'!
rotateLeft
	directionHandler rotateLeft: self.

! !

!MarsRover methodsFor: 'movement' stamp: 'iniesz 5/16/2022 17:57:28'!
rotateRight
	directionHandler rotateRight: self.
	
	! !


!MarsRover methodsFor: 'initialization' stamp: 'iniesz 5/16/2022 17:57:41'!
initialize
	self initializeAt: 0@0 heading: #North.! !

!MarsRover methodsFor: 'initialization' stamp: 'iniesz 5/16/2022 17:57:49'!
initializeAt: aPoint heading: aDirectionSymbol
	position := aPoint.
	directionHandler := MarsRoverMovementHandler for: aDirectionSymbol.
	! !


!MarsRover methodsFor: 'commands' stamp: 'iniesz 5/16/2022 17:57:56'!
command: aStringOfCommands 
	
	| commandsHandler |
	
	commandsHandler := MarsRoverCommandsHandler for: aStringOfCommands.
	commandsHandler command: self.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'Ej04-MarsRover'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'instance creation' stamp: 'iniesz 5/16/2022 01:43:35'!
at: aPoint heading: aSymbol 
	^self new initializeAt: aPoint heading: aSymbol ! !


!classDefinition: #MarsRoverCommandsHandler category: 'Ej04-MarsRover'!
Object subclass: #MarsRoverCommandsHandler
	instanceVariableNames: 'stringOfCommands'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!MarsRoverCommandsHandler methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:50:04'!
command: aMarsRover
	
	| nextCommandHandler |
	self executeCommand: aMarsRover.
	
	nextCommandHandler := MarsRoverCommandsHandler for: stringOfCommands.
	nextCommandHandler command: aMarsRover.! !

!MarsRoverCommandsHandler methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:10:40'!
executeCommand: aMarsRover
	self subclassResponsibility.! !

!MarsRoverCommandsHandler methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:23:27'!
initializeWith: aStringOfCommands
	stringOfCommands  := aStringOfCommands.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverCommandsHandler class' category: 'Ej04-MarsRover'!
MarsRoverCommandsHandler class
	instanceVariableNames: ''!

!MarsRoverCommandsHandler class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:59:56'!
for: aStringsOfCommands
	
	|commandHandlerSubclass theRestOfTheStringOfCommands commandsHandler|
	
	commandHandlerSubclass := self implementationClasses 
			detect:[:aCommandHandlerSubclass  | aCommandHandlerSubclass isCommandHandlerFor: aStringsOfCommands] 
			ifNone:[NoCommand].
			
	commandsHandler := commandHandlerSubclass new.
	commandsHandler initializeWith: (self removeFirstCommand: aStringsOfCommands).
	
	^ commandsHandler.! !

!MarsRoverCommandsHandler class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:33:39'!
implementationClasses
	| implementationClasses |
	implementationClasses := CharCommand subclasses asOrderedCollection.
	implementationClasses add: NoCommand.
	
	^ implementationClasses ! !

!MarsRoverCommandsHandler class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:07:02'!
isCommandHandlerFor: aStringOfCommands
	(aStringOfCommands size > 1)
		ifTrue:[^ self commandString = (aStringOfCommands at: 1)].
		
	
		
! !

!MarsRoverCommandsHandler class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:59:25'!
removeFirstCommand: aStringOfCommands
	| theRestOfTheStringOfCommands |
	(aStringOfCommands size > 1)
		ifTrue:[theRestOfTheStringOfCommands := aStringOfCommands copyFrom: 2 to: aStringOfCommands size]
		ifFalse:[theRestOfTheStringOfCommands := ''].
		
	^ theRestOfTheStringOfCommands! !


!classDefinition: #CharCommand category: 'Ej04-MarsRover'!
MarsRoverCommandsHandler subclass: #CharCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CharCommand class' category: 'Ej04-MarsRover'!
CharCommand class
	instanceVariableNames: ''!

!CharCommand class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:20:46'!
commandString
	^ self subclassResponsibility.! !

!CharCommand class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:48:42'!
isCommandHandlerFor: aStringOfCommands
	(aStringOfCommands size > 0) 
	ifTrue: [^ self commandString = (aStringOfCommands at: 1)].
	
	^false.
		
	
		! !


!classDefinition: #BackwardsCommand category: 'Ej04-MarsRover'!
CharCommand subclass: #BackwardsCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!BackwardsCommand methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:40:10'!
executeCommand: aMarsRover
	aMarsRover backward.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'BackwardsCommand class' category: 'Ej04-MarsRover'!
BackwardsCommand class
	instanceVariableNames: ''!

!BackwardsCommand class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:21:35'!
commandString
	^ $b.! !


!classDefinition: #ForwardCommand category: 'Ej04-MarsRover'!
CharCommand subclass: #ForwardCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!ForwardCommand methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:40:30'!
executeCommand: aMarsRover
	aMarsRover forward.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ForwardCommand class' category: 'Ej04-MarsRover'!
ForwardCommand class
	instanceVariableNames: ''!

!ForwardCommand class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:21:52'!
commandString
	^ $f.! !


!classDefinition: #RotateLeftCommand category: 'Ej04-MarsRover'!
CharCommand subclass: #RotateLeftCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!RotateLeftCommand methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:40:37'!
executeCommand: aMarsRover
	aMarsRover rotateLeft.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RotateLeftCommand class' category: 'Ej04-MarsRover'!
RotateLeftCommand class
	instanceVariableNames: ''!

!RotateLeftCommand class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:21:41'!
commandString
	^ $l.! !


!classDefinition: #RotateRightCommand category: 'Ej04-MarsRover'!
CharCommand subclass: #RotateRightCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!RotateRightCommand methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:40:46'!
executeCommand: aMarsRover
	aMarsRover rotateRight.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RotateRightCommand class' category: 'Ej04-MarsRover'!
RotateRightCommand class
	instanceVariableNames: ''!

!RotateRightCommand class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:21:46'!
commandString
	^ $r.! !


!classDefinition: #NoCommand category: 'Ej04-MarsRover'!
MarsRoverCommandsHandler subclass: #NoCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!NoCommand methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:49:43'!
command: aMarsRover
	"DO NOTHING"! !

!NoCommand methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:41:07'!
executeCommand: aMarsRover
	"DO NOTHING"! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NoCommand class' category: 'Ej04-MarsRover'!
NoCommand class
	instanceVariableNames: ''!

!NoCommand class methodsFor: 'as yet unclassified' stamp: 'iniesz 5/16/2022 17:14:48'!
isCommandHandlerFor: aStringOfCommands
	^ aStringOfCommands size = 0.
		
	
		
! !


!classDefinition: #MarsRoverMovementHandler category: 'Ej04-MarsRover'!
Object subclass: #MarsRoverMovementHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!MarsRoverMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:45:53'!
moveBackwards: aMarsRover
	self subclassResponsibility.! !

!MarsRoverMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:45:47'!
moveForward: aMarsRover
	self subclassResponsibility.! !

!MarsRoverMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 01:17:44'!
rotateLeft: aMarsRover
	self subclassResponsibility.! !

!MarsRoverMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 01:17:51'!
rotateRight: aMarsRover
	self subclassResponsibility.! !


!MarsRoverMovementHandler methodsFor: 'testing' stamp: 'iniesz 5/16/2022 02:11:21'!
directionSymbol
	^ self class directionSymbol.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverMovementHandler class' category: 'Ej04-MarsRover'!
MarsRoverMovementHandler class
	instanceVariableNames: ''!

!MarsRoverMovementHandler class methodsFor: 'instance-creation' stamp: 'iniesz 5/16/2022 02:10:26'!
directionSymbol
	^ self subclassResponsibility.! !

!MarsRoverMovementHandler class methodsFor: 'instance-creation' stamp: 'iniesz 5/16/2022 02:19:14'!
for: aDirectionSymbol
	
	| directionHandlerSubClass |
	directionHandlerSubClass := self subclasses 
			detect:[:directionHandlerSubclass | directionHandlerSubclass isDirectionHandlerFor: aDirectionSymbol] 
			ifNone:[	self error: 'The aDirectionSymbol does not represent a valid direction.'.].
				
	^ directionHandlerSubClass new.! !

!MarsRoverMovementHandler class methodsFor: 'instance-creation' stamp: 'iniesz 5/16/2022 02:14:22'!
isDirectionHandlerFor: aDirectionSymbol
	^ (self directionSymbol = aDirectionSymbol). ! !


!classDefinition: #EastMovementHandler category: 'Ej04-MarsRover'!
MarsRoverMovementHandler subclass: #EastMovementHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!EastMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:49:56'!
moveBackwards: aMarsRover
	aMarsRover moveWest.! !

!EastMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:49:42'!
moveForward: aMarsRover
	aMarsRover moveEast.! !

!EastMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:56:52'!
rotateLeft: aMarsRover
	aMarsRover directionHandler: NorthMovementHandler new.! !

!EastMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:57:02'!
rotateRight: aMarsRover
	aMarsRover directionHandler: SouthMovementHandler new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EastMovementHandler class' category: 'Ej04-MarsRover'!
EastMovementHandler class
	instanceVariableNames: ''!

!EastMovementHandler class methodsFor: 'instance-creation' stamp: 'iniesz 5/16/2022 02:08:14'!
directionSymbol
	^ #East.! !


!classDefinition: #NorthMovementHandler category: 'Ej04-MarsRover'!
MarsRoverMovementHandler subclass: #NorthMovementHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!NorthMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:50:51'!
moveBackwards: aMarsRover
	aMarsRover moveSouth.! !

!NorthMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:50:15'!
moveForward: aMarsRover
	aMarsRover moveNorth.! !

!NorthMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:57:12'!
rotateLeft: aMarsRover
	aMarsRover directionHandler: WestMovementHandler new.! !

!NorthMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:56:31'!
rotateRight: aMarsRover
	aMarsRover directionHandler: EastMovementHandler new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NorthMovementHandler class' category: 'Ej04-MarsRover'!
NorthMovementHandler class
	instanceVariableNames: ''!

!NorthMovementHandler class methodsFor: 'instance-creation' stamp: 'iniesz 5/16/2022 02:09:16'!
directionSymbol
	^ #North.! !


!classDefinition: #SouthMovementHandler category: 'Ej04-MarsRover'!
MarsRoverMovementHandler subclass: #SouthMovementHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!SouthMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:55:27'!
moveBackwards: aMarsRover
	aMarsRover moveNorth.! !

!SouthMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:51:07'!
moveForward: aMarsRover
	aMarsRover moveSouth.! !

!SouthMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:56:31'!
rotateLeft: aMarsRover
	aMarsRover directionHandler: EastMovementHandler new.! !

!SouthMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:57:12'!
rotateRight: aMarsRover
	aMarsRover directionHandler: WestMovementHandler new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SouthMovementHandler class' category: 'Ej04-MarsRover'!
SouthMovementHandler class
	instanceVariableNames: ''!

!SouthMovementHandler class methodsFor: 'instance-creation' stamp: 'iniesz 5/16/2022 02:09:45'!
directionSymbol
	^ #South.! !


!classDefinition: #WestMovementHandler category: 'Ej04-MarsRover'!
MarsRoverMovementHandler subclass: #WestMovementHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Ej04-MarsRover'!

!WestMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:55:44'!
moveBackwards: aMarsRover
	aMarsRover moveEast.! !

!WestMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:51:48'!
moveForward: aMarsRover
	aMarsRover moveWest.! !

!WestMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:57:02'!
rotateLeft: aMarsRover
	aMarsRover directionHandler: SouthMovementHandler new.! !

!WestMovementHandler methodsFor: 'movement' stamp: 'iniesz 5/16/2022 02:56:52'!
rotateRight: aMarsRover
	aMarsRover directionHandler: NorthMovementHandler new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'WestMovementHandler class' category: 'Ej04-MarsRover'!
WestMovementHandler class
	instanceVariableNames: ''!

!WestMovementHandler class methodsFor: 'instance-creation' stamp: 'iniesz 5/16/2022 02:10:57'!
directionSymbol
	^ #West.! !
