!classDefinition: #FloorTest category: 'ISW1-2022-1C-2doParcial'!
TestCase subclass: #FloorTest
	instanceVariableNames: 'dice testFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 17:54:33'!
createTestFloor
	^ Floor width: 10 height: 15.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 19:14:07'!
createTestFloor2
	^ Floor width: 5 height: 10.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:01:41'!
createTestPlayerIn: aFloor
	| diceRoller floor |
	
	diceRoller := testFactory createTestDice: #(7 8).
	^ Player dice: diceRoller
		position: 3@3 
		floor: floor.
		! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 20:38:53'!
roll
	^ dice next.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 20:43:08'!
setUp
	testFactory := LaSTestFactory new.
	dice := CircularReadStream on: #(1) 
						moving: NullUnit new.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 17:54:26'!
test01CanCreateFloorWithValidWidthAndHeight

	| floor |
	floor := self createTestFloor.
	
	self assert: 10 equals: floor width.
	self assert: 15 equals: floor height.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:05:17'!
test02FloorCorrectlyCalculatesNoMovement

	| floor newPosition |
	floor := self createTestFloor.
	
	newPosition := floor newPositionFrom: 5@6 withRoll: 6@6.
	
	self assert: 5 equals: newPosition x.
	self assert: 6 equals: newPosition y.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:12:34'!
test03FloorCorrectlyCalculatesXMovement

	| floor newPosition |
	floor := self createTestFloor.
	
	newPosition := floor newPositionFrom: 5@6 withRoll: 10@6.
	
	self assert: 9 equals: newPosition x.
	self assert: 6 equals: newPosition y.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:12:44'!
test04FloorCorrectlyCalculatesXMovementUpperLimit

	| floor newPosition |
	floor := self createTestFloor.
	
	newPosition := floor newPositionFrom: 5@6 withRoll: 12@6.
	
	self assert: 10 equals: newPosition x.
	self assert: 6 equals: newPosition y.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:12:52'!
test05FloorCorrectlyCalculatesYMovement

	| floor newPosition |
	floor := self createTestFloor.
	
	newPosition := floor newPositionFrom: 5@6 withRoll: 6@8.
	
	self assert: 5 equals: newPosition x.
	self assert: 8 equals: newPosition y.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:33:45'!
test06FloorCorrectlyCalculatesYMovementUpperLimit

	| floor newPosition |
	floor := self createTestFloor.
	
	newPosition := floor newPositionFrom: 5@14 withRoll: 6@12.
	
	self assert: 5 equals: newPosition x.
	self assert: 15 equals: newPosition y.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:34:29'!
test07FloorCorrectlyCalculatesXMovementLowerLimit

	| floor newPosition |
	floor := self createTestFloor.
	
	newPosition := floor newPositionFrom: 5@10 withRoll: 1@6.
	
	self assert: 1 equals: newPosition x.
	self assert: 10 equals: newPosition y.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:37:16'!
test08FloorCorrectlyCalculatesYMovementLowerLimit

	| floor newPosition |
	floor := self createTestFloor.
	
	newPosition := floor newPositionFrom: 5@3 withRoll: 6@2.
	
	self assert: 5 equals: newPosition x.
	self assert: 1 equals: newPosition y.! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:01:51'!
test09FloorMovePlayer

	| floor player |
	floor := self createTestFloor.
	player := self createTestPlayerIn: floor.
	
	floor move: player withRoll: 7@8.
	
	self assert: (4@5) equals: player position.
	self assert: floor equals: player floor.
		
	! !

!FloorTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:03:43'!
test10FloorShortcutIntoAnotherFloor

	| floor floor2 player |
	floor := self createTestFloor.
	floor2 := self createTestFloor2.
	
	player := self createTestPlayerIn: floor.
	floor at: 5@4 exitTo: floor2 position: 1@1.
	
	floor move: player withRoll: 8@7.
	
	self assert: (1@1) equals: player position.
	self assert: floor2 equals: player floor.
	
	! !


!classDefinition: #LaSTestFactory category: 'ISW1-2022-1C-2doParcial'!
TestCase subclass: #LaSTestFactory
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!LaSTestFactory methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 20:42:44'!
createTestDice: aNumbersSequence
	^ TestDice with: (CircularReadStream on: aNumbersSequence moving: NullUnit new).! !

!LaSTestFactory methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 19:53:13'!
createTestFloor
	^ Floor width: 10 height: 15.! !

!LaSTestFactory methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 19:53:20'!
createTestFloor2
	^ Floor width: 5 height: 10.! !

!LaSTestFactory methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 19:54:05'!
createTestFloor3
	^ Floor width: 10 height: 10.! !

!LaSTestFactory methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 19:54:12'!
createTestFloor4
	^ Floor width: 5 height: 5.! !

!LaSTestFactory methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:19:01'!
createTestFloorsCollection
	| floor floor2 floor3 floor4 |
	floor := self createTestFloor.
	floor2 := self createTestFloor2.	
	floor3 := self createTestFloor3.
	floor4 := self createTestFloor4.
	
	floor at: 4@5 exitTo: floor2 position: 1@1.
	floor2 at: 3@3 exitTo: floor3 position: 5@5.
	floor3 at: 5@7 exitTo: floor position: 2@2.
	floor3 at: 2@2 exitTo: floor4 position: 5@5.	
	floor4 at: 4@4 exitTo: floor2 position: 4@1.
	
	
	^ OrderedCollection with: floor with: floor2 with: floor3 with: floor4.! !


!classDefinition: #LaddersAndSlidesTest category: 'ISW1-2022-1C-2doParcial'!
TestCase subclass: #LaddersAndSlidesTest
	instanceVariableNames: 'testFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!LaddersAndSlidesTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 20:03:21'!
setUp
	
	testFactory := LaSTestFactory new.! !

!LaddersAndSlidesTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:52:23'!
test01CreateNewGameWithPlayers

	| game diceRoller floors |
	diceRoller := testFactory createTestDice: #(7 7 6 6 4 4).
	floors := testFactory createTestFloorsCollection.
	game := LaddersAndSlides startGamePlayers: 3
					initialFloor: 1
					finalPosition: 3@3
					finalFloor: 4
					floors: floors
					dice:  diceRoller.
					
	
	self assert: 3 equals: game totalPlayers.
	! !

!LaddersAndSlidesTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 20:06:22'!
test02CreateNewGameRaisesErrorWhenNegativeInitialFloor! !

!LaddersAndSlidesTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 20:06:37'!
test03CreateNewGameRaisesErrorWhenDecimalInitialFloor! !

!LaddersAndSlidesTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 20:08:05'!
test04CreateNewGameRaisesErrorWhenNegativeFinalFloor! !

!LaddersAndSlidesTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 20:08:14'!
test05CreateNewGameRaisesErrorWhenDecimalFinalFloor! !

!LaddersAndSlidesTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:51:06'!
test06TwoPlayersPlayTheirTurns

	| game diceRoller floors |
	diceRoller := testFactory createTestDice: #(7 7 6 6 4 4).
	floors := testFactory createTestFloorsCollection.
	game := LaddersAndSlides startGamePlayers: 2
					initialFloor: 1
					finalPosition: 3@3
					finalFloor: 4
					floors: floors
					dice:  diceRoller.
					
	game playTurn.
	game playTurn.
						
	self assert: 2@2 equals: (game positionOfPlayer:1).
	self assert: (floors at: 1) equals: (game floorOfPlayer:1).
	
	self assert: 1@1 equals: (game positionOfPlayer:2).
	self assert: (floors at: 1) equals: (game floorOfPlayer:2).
	
	! !

!LaddersAndSlidesTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:55:28'!
test07TwoPlayersPlayTwoTurnsPlayerOneChangeFloor

	| game diceRoller floors |
	diceRoller := testFactory createTestDice: #(7 7 6 6 8 9 5 5).
	floors := testFactory createTestFloorsCollection.
	game := LaddersAndSlides startGamePlayers: 2
					initialFloor: 1
					finalPosition: 3@3
					finalFloor: 4
					floors: floors
					dice:  diceRoller.
					
	game playTurn.
	game playTurn.
	game playTurn.
	game playTurn.
						
	self assert: 1@1 equals: (game positionOfPlayer:1).
	self assert: (floors at: 2) equals: (game floorOfPlayer:1).
	
	self assert: 1@1 equals: (game positionOfPlayer:2).
	self assert: (floors at: 1) equals: (game floorOfPlayer:2).
	
	! !


!classDefinition: #PlayerTest category: 'ISW1-2022-1C-2doParcial'!
TestCase subclass: #PlayerTest
	instanceVariableNames: 'dice'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!PlayerTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:40:13'!
createTestFloor
	^ Floor width: 10 height: 15.! !

!PlayerTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:54:23'!
roll
	^ dice next.! !

!PlayerTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:58:33'!
setUp

	dice := CircularReadStream on: #(6 1 2 3 4 5) 
						moving: NullUnit new.! !

!PlayerTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:51:46'!
test01CreatePlayerInPosition

	| player |
	player := Player dice: self
				position: 3@3 
				floor: (self createTestFloor).

	self assert: 3@3 equals: player position.! !

!PlayerTest methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 19:02:02'!
test02PlayerRollDicesToChangePosition

	| player |
	
	dice := CircularReadStream on: #(6 7 9) moving: NullUnit new.
	
	player := Player dice: self
				position: 3@3 
				floor: (self createTestFloor).

	self assert: 3@3 equals: player position.
	player makeRolls.
	self assert: 4 @ 6 equals: player position.
	! !


!classDefinition: #Dice category: 'ISW1-2022-1C-2doParcial'!
Object subclass: #Dice
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!Dice methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 18:50:11'!
roll
	^ (1 to: 12) atRandom.! !


!classDefinition: #TestDice category: 'ISW1-2022-1C-2doParcial'!
Dice subclass: #TestDice
	instanceVariableNames: 'generator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!TestDice methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:49:45'!
initializeWith: aGenerator
	generator := aGenerator.! !

!TestDice methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 20:42:34'!
roll
	| number |
	number := generator current.
	generator next.
	^ number.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TestDice class' category: 'ISW1-2022-1C-2doParcial'!
TestDice class
	instanceVariableNames: 'generator'!

!TestDice class methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:49:22'!
with: aGenerator
	| dice |
	dice := TestDice new.
	dice initializeWith: aGenerator.
	^ dice.! !


!classDefinition: #Floor category: 'ISW1-2022-1C-2doParcial'!
Object subclass: #Floor
	instanceVariableNames: 'width height exitPointsTo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!Floor methodsFor: 'initialization' stamp: 'iniesz 7/4/2022 20:11:42'!
initializeWidth: aWidth height: aHeight 
	width := aWidth.
	height := aHeight.
	
	exitPointsTo := Dictionary new.! !


!Floor methodsFor: 'dimensions' stamp: 'iniesz 7/4/2022 17:52:22'!
height
	^height! !

!Floor methodsFor: 'dimensions' stamp: 'iniesz 7/4/2022 17:52:14'!
width
	^width! !


!Floor methodsFor: 'movement' stamp: 'iniesz 7/4/2022 21:07:57'!
move: aPlayer withRoll: aGameRoll 
	
	| position floor |
	position := self newPositionFrom: (aPlayer position) withRoll: aGameRoll.
	floor := self.
	(exitPointsTo includesKey: position)
		ifTrue:[ | exitPoint |
			exitPoint := exitPointsTo at: position.
			position := exitPoint at:#position.
			floor:=exitPoint at:#floor.
		].
	
	aPlayer setPosition: position inFloor: floor.
	
	
	
	! !

!Floor methodsFor: 'movement' stamp: 'iniesz 7/4/2022 18:37:02'!
newPositionFrom: aCurrentPosition withRoll: aGameRoll
	| movementX movementY newPosition |
	movementX := aGameRoll x - 6.
	movementY := aGameRoll y - 6.
	
	newPosition := aCurrentPosition + (movementX @ movementY).
	
	(newPosition x > width)
		ifTrue:[newPosition := width @ newPosition y].
	
	(newPosition x < 1)
		ifTrue:[newPosition := 1 @ newPosition y].
	
	(newPosition y > height)
		ifTrue:[newPosition := newPosition x @ height].	
	
	(newPosition y < 1)
		ifTrue:[newPosition := newPosition x @ 1].
	
	^ newPosition

		! !


!Floor methodsFor: 'shortcuts' stamp: 'iniesz 7/4/2022 20:59:52'!
addEntryPoint: aPosition fromFloor: anExitPoint
	
	| entriesFrom |
	(entriesFrom includesKey: aPosition)
		ifFalse:[entriesFrom at: aPosition put: OrderedCollection new].
		
	entriesFrom add: anExitPoint.! !

!Floor methodsFor: 'shortcuts' stamp: 'iniesz 7/4/2022 20:56:07'!
at: aPosition exitTo: aFloor position: anExitPosition
	
	| entryPoint |
	entryPoint := Dictionary new.
	
	entryPoint at: #position put: anExitPosition.
	entryPoint at: #floor put: aFloor.
	
	exitPointsTo at: aPosition put: entryPoint.! !

!Floor methodsFor: 'shortcuts' stamp: 'iniesz 7/4/2022 20:12:17'!
exitAt: aPosition

	^ exitPointsTo at: aPosition.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Floor class' category: 'ISW1-2022-1C-2doParcial'!
Floor class
	instanceVariableNames: ''!

!Floor class methodsFor: 'instance creation' stamp: 'iniesz 7/4/2022 17:51:45'!
width: aWidth height: aHeight 
	^self new initializeWidth: aWidth height: aHeight. ! !


!classDefinition: #LaddersAndSlides category: 'ISW1-2022-1C-2doParcial'!
Object subclass: #LaddersAndSlides
	instanceVariableNames: 'playersAmount initialFloor finalPosition finalFloor floorsCollection players gameEnded playersRotation'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!LaddersAndSlides methodsFor: 'initialization' stamp: 'iniesz 7/4/2022 21:45:38'!
floorOfPlayer: aPlayerIndex
	^ (players at:aPlayerIndex) floor. 
! !

!LaddersAndSlides methodsFor: 'initialization' stamp: 'iniesz 7/4/2022 21:44:45'!
initializePlayersWithDice: aDice
	players := OrderedCollection new.
	playersAmount timesRepeat: 
	[ 
		players add: (Player dice: aDice position: 1@1 floor: (	floorsCollection at:initialFloor)).
	].

	playersRotation :=  (CircularReadStream on: players moving: NullUnit new).! !

!LaddersAndSlides methodsFor: 'initialization' stamp: 'iniesz 7/4/2022 21:46:43'!
initializeStartGamePlayers: aPlayersAmount initialFloor: anInitialFloor finalPosition: aFinalPosition finalFloor: aFinalFloor floors: aFloorsCollection dice: aDice.
	playersAmount := aPlayersAmount.
	initialFloor := anInitialFloor.
	finalPosition := aFinalPosition.
	finalFloor := aFinalFloor.
	floorsCollection := aFloorsCollection.
	gameEnded := false.
	
	players := OrderedCollection new.
	self initializePlayersWithDice: aDice.! !

!LaddersAndSlides methodsFor: 'initialization' stamp: 'iniesz 7/4/2022 21:45:31'!
positionOfPlayer: aPlayerIndex
	^ (players at:aPlayerIndex) position. 
! !

!LaddersAndSlides methodsFor: 'initialization' stamp: 'iniesz 7/4/2022 19:52:31'!
totalPlayers
	^ playersAmount.! !


!LaddersAndSlides methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:42:36'!
assertGameHasNotEnded
	gameEnded
		ifTrue: [self error: self class theGameAlreadyEndedErrorMessage].! !

!LaddersAndSlides methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:39:39'!
checkIfFinalPosition: aPlayer
	^ (aPlayer position = finalPosition)
	
	and: [aPlayer floor = floorsCollection at: finalFloor].! !

!LaddersAndSlides methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:45:05'!
playTurn
	| player |
	self assertGameHasNotEnded.
	player := playersRotation current.
	player makeRolls.
	
	(self checkIfFinalPosition: player)
		ifTrue: [gameEnded := true].
	
	playersRotation next.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'LaddersAndSlides class' category: 'ISW1-2022-1C-2doParcial'!
LaddersAndSlides class
	instanceVariableNames: ''!

!LaddersAndSlides class methodsFor: 'instance creation' stamp: 'iniesz 7/4/2022 21:23:24'!
startGamePlayers: aPlayersAmount initialFloor: anInitialFloor finalPosition: aFinalPosition finalFloor: aFinalFloor floors: aFloorsCollection dice: aDice
	
	^self new initializeStartGamePlayers: aPlayersAmount initialFloor: anInitialFloor finalPosition: aFinalPosition finalFloor: aFinalFloor floors: aFloorsCollection dice: aDice.! !


!LaddersAndSlides class methodsFor: 'as yet unclassified' stamp: 'iniesz 7/4/2022 21:43:01'!
theGameAlreadyEndedErrorMessage
	^ 'The game has already ended'.! !


!classDefinition: #Player category: 'ISW1-2022-1C-2doParcial'!
Object subclass: #Player
	instanceVariableNames: 'position floor dice'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!Player methodsFor: 'initialization' stamp: 'iniesz 7/4/2022 18:59:37'!
initializeDice: aDice position: aPosition floor: aFloor 
	dice := aDice.
	position := aPosition.
	floor := aFloor.! !

!Player methodsFor: 'initialization' stamp: 'iniesz 7/4/2022 18:42:05'!
initializePosition: aPosition floor: aFloor 
	position := aPosition.
	floor := aFloor.! !


!Player methodsFor: 'movement-internal' stamp: 'iniesz 7/4/2022 20:36:08'!
setPosition: aCoordinate inFloor: aFloor

	position := aCoordinate.
	floor := aFloor.! !


!Player methodsFor: 'movement' stamp: 'iniesz 7/4/2022 20:36:19'!
floor
	^floor! !

!Player methodsFor: 'movement' stamp: 'iniesz 7/4/2022 21:36:14'!
makeRolls
	
	| rollForX rollForY |
	rollForX := dice roll.
	rollForY := dice roll.
	
				
	floor move: self withRoll: (rollForX @ rollForY).
				
				
	
	! !

!Player methodsFor: 'movement' stamp: 'iniesz 7/4/2022 18:42:26'!
position
	^position! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Player class' category: 'ISW1-2022-1C-2doParcial'!
Player class
	instanceVariableNames: ''!

!Player class methodsFor: 'instance creation' stamp: 'iniesz 7/4/2022 18:59:22'!
dice: aDice position: aPosition floor: aFloor 
	^self new initializeDice: aDice position: aPosition floor: aFloor ! !
